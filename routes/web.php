<?php


use App\User;

Auth::routes();
Route::get('/', 'HomeController@index')->name('dashboard')->middleware('auth');

// user
Route::get('/user','UserController@index')->middleware('auth');
Route::get('/user/add',['as'=>'user.add','uses'=>'UserController@create'])->middleware('auth');
Route::post('/user/create',['as'=>'user.create','uses'=>'UserController@store'])->middleware('auth');
Route::get('/user/delete/{id}',['as'=>'user.delete','uses'=>'UserController@destroy'])->middleware('auth');
Route::get('/user/edit/{id}',['as'=>'user.edit','uses'=>'UserController@edit'])->middleware('auth');
Route::put('/user/update/{id}',['as'=>'user.update','uses'=>'UserController@update'])->middleware('auth');
//end user

// Expense Type
Route::get('/expense-type/create' ,'ExpenseTypeController@create')->name('createExpenseType')->middleware('auth');
Route::get('/expense-type' ,'ExpenseTypeController@index')->middleware('auth');

// Resourceful Route
Route::resource('employee' ,'EmployeeController')->middleware('auth');
Route::get('salary' ,'EmployeeController@salary')->name('check-salary')->middleware('auth');
Route::post('check-salary' ,'EmployeeController@CheckSalary')->name('post-salary')->middleware('auth');
Route::get('tada' ,'EmployeeController@tada')->name('check-tada')->middleware('auth');
Route::post('tada' ,'EmployeeController@tada_post')->name('check-tada-post')->middleware('auth');
Route::post('pay-tada' ,'TadaController@PayTada')->name('pay-tada')->middleware('auth');
Route::resource('employee-expense' ,'EmployeeExpenseController')->middleware('auth');
Route::resource('attendance' ,'AttendanceController')->middleware('auth');
Route::resource('off-day' ,'OffDayController')->middleware('auth');
Route::resource('company-expense' ,'CompanyExpenseController')->middleware('auth');
Route::post('com_ex' ,'CompanyExpenseController@list')->name('com_ex')->middleware('auth');
Route::post('office_ex' ,'OfficeExpenseController@list')->name('office_ex')->middleware('auth');

Route::resource('office-expense' ,'OfficeExpenseController')->middleware('auth');
Route::get('attendance-edit' ,'AttendanceController@editAttendance')->middleware('auth');
Route::post('attendance-update' ,'AttendanceController@updateAttendance')->middleware('auth');

// Routes for Ajax / Axios Call
Route::get('all_employee','EmployeeController@AllEmployee')->middleware('auth');
Route::get('all_employee_expense','EmployeeExpenseController@AllEmployeeExpense')->middleware('auth');
Route::get('all_attendance','AttendanceController@AllAttendance')->middleware('auth');
Route::get('all_off','AttendanceController@AllOff')->middleware('auth');
Route::get('all_off_employee','EmployeeController@all_off_employee')->middleware('auth');
Route::post('all_employee/{id}/{m_id}','EmployeeExpenseController@FindEmployee')->middleware('auth');
Route::post('save_expense_e/{id}','EmployeeExpenseController@SaveEx')->middleware('auth');
Route::delete('delete_expense/{id}','EmployeeExpenseController@DeleteEx')->middleware('auth');
Route::post('add_leave','AttendanceController@addLeave')->middleware('auth');
Route::get('get_leave','AttendanceController@getLeave')->middleware('auth');
Route::post('create-expense-type','ExpenseTypeController@store')->middleware('auth');
Route::post('generate-expense-type','ExpenseTypeController@show')->middleware('auth');

// Export to excel
Route::get('export','ExpenseTypeController@export')->middleware('auth');
Route::get('salary-export','EmployeeController@exportSalary')->middleware('auth');
Route::get('attendance-export','AttendanceController@exportAttendance')->middleware('auth');
Route::get('employee-export','EmployeeController@exportEmployee')->middleware('auth');


//Products Route
Route::resource('product', 'ProductController')->middleware('auth');

//Sales Route
Route::resource('sale', 'SalesController')->middleware('auth');
Route::post('/import', 'SalesController@import')->middleware('auth');
Route::get('/sales-report', 'SalesController@sales_report')->middleware('auth');
Route::post('/generate-sales-report', 'SalesController@sales_report')->middleware('auth');
Route::get('/sales_details/{id}', 'SalesController@sales_details')->name('sales_details')->middleware('auth');

//Stocks Route
Route::get('/stocks-list', 'StockController@index')->middleware('auth');
Route::get('/add-stocks', 'StockController@create')->middleware('auth');
Route::post('/import-stocks', 'StockController@import')->middleware('auth');

//Retail Market Route
Route::get('/retail-market', 'RetailMarketController@index')->middleware('auth');
Route::post('/retail-market-details', 'RetailMarketController@retail_market')->middleware('auth');

// Michallencious
Route::get('add-designation', 'EmployeeController@addDesignation')->name('add-designation')->middleware('auth');
Route::post('create-designation', 'EmployeeController@createDesignation')->name('create-designation')->middleware('auth');
Route::get('tada-export' ,'TadaController@export')->name('tada-export')->middleware('auth');


//ROI
Route::get('/roi', 'RoiController@index')->middleware('auth');
Route::post('/roi-details', 'RoiController@details')->middleware('auth');
Route::get('/pending_claim', 'RoiController@pending_claim')->middleware('auth');
Route::get('/investments', 'InvestmentController@index')->middleware('auth');
Route::get('/add-investment', 'InvestmentController@create')->middleware('auth');
Route::post('/store-investment', 'InvestmentController@store')->middleware('auth');
Route::get('/add-investment-details', 'InvestmentController@create_details')->middleware('auth');
Route::post('/store-investment-details', 'InvestmentController@store_details')->middleware('auth');

//other info
Route::get('/other-info', 'OtherInfoController@index')->middleware('auth');
Route::post('/store-other-info', 'OtherInfoController@store')->middleware('auth');

//shop
Route::resource('shop', 'ShopController')->middleware('auth');

//cheque
Route::get('cheque', 'ChequeController@index')->middleware('auth');
Route::get('cheque_pass/{id}', 'ChequeController@cheque_pass')->middleware('auth');
Route::get('cheque_bounce/{id}', 'ChequeController@cheque_bounce')->middleware('auth');

//register
Route::get('register', 'RegisterController@index');

//Provident Fund 
Route::get('/provident_fund_list' ,'EmployeeController@ProvidentList')->name('provident-fund-list');
Route::get('/provident_fund_pay/{employee}' ,'EmployeeController@ProvidentPay')->name('provident-fund-pay');
Route::post('/provident_fund_post' ,'EmployeeController@ProvidentPost')->name('provident-fund-post');

//Sales Values
Route::get('/sales-edit/{id}', 'Sale_valuesController@edit');
Route::put('/sales-update/{id}', 'Sale_valuesController@update');

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:cache');
    return "Cache is cleared";
});