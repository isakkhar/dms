@extends('layouts.master')
@section('page_main_content')

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
           

           <div class="col-md-4 col-sm-8 col-xs-12">
           
               <a href="{{ url('stocks-list') }}" class="info-box info_box_index" style="color: #000; text-decoration: none; background: #ffefef; border: 1px solid #ddd8d8;">
                   <span class="info-box-icon bg-red"><i class="fa fa-gift"></i></span>
                   <div class="info-box-content">
                       <span class="info-box-text">Total Available Products</span>
                       <span class="info-box-number" style="margin-top: 10px;">{{ $stocks }}</span>
                   </div><!-- /.info-box-content -->
               </a><!-- /.info-box -->
            </div>

			
			<div class="col-md-4 col-sm-8 col-xs-12">

			   <a href="" class="info-box info_box_index" style="color: #000; text-decoration: none; background: #ffefef; border: 1px solid #ddd8d8;">
			       <span class="info-box-icon bg-aqua"><i class="fa fa-try"></i></span>
			       <div class="info-box-content">
			           <span class="info-box-text">Total Expeses for Employee</span>
			           <span class="info-box-number" style="margin-top: 10px;"> {{ $employee_expenses }} </span>
			       </div><!-- /.info-box-content -->
			   </a><!-- /.info-box -->

			   </div>

			   <div class="col-md-4 col-sm-8 col-xs-12">
			   
			   <a href="{{ url('employee') }}" class="info-box info_box_index" style="color: #000; text-decoration: none; background: #ffefef; border: 1px solid #ddd8d8;">
			       <span class="info-box-icon bg-olive"><i class="fa fa-users"></i></span>
			       <div class="info-box-content">
			           <span class="info-box-text">Total Employees</span>
			           <span class="info-box-number" style="margin-top: 10px;">{{ $employees }}</span>
			       </div><!-- /.info-box-content -->
			   </a><!-- /.info-box -->

			   </div>

			   <div class="col-md-4 col-sm-8 col-xs-12">
			       
			       <a href="{{ url('user') }}" class="info-box info_box_index" style="color: #000; text-decoration: none; background: #ffefef; border: 1px solid #ddd8d8;">
			
			           <span class="info-box-icon bg-orange"><i class="fa fa-user"></i></span>
			           <div class="info-box-content">
			               <span class="info-box-text">Total Users</span>
			               <span class="info-box-number" style="margin-top: 10px;">{{ $users }}</span>
			           </div><!-- /.info-box-content -->

			       </a><!-- /.info-box -->

			   </div>

			   <div class="col-md-4 col-sm-8 col-xs-12">

			       <a href="{{ url('retail-market') }}" class="info-box info_box_index" style="color: #000; text-decoration: none; background: #ffefef; border: 1px solid #ddd8d8;">
			
			           <span class="info-box-icon bg-green"><i class="fa  fa-dollar"></i></span>
			           <div class="info-box-content">
			               <span class="info-box-text">Cash Short : {{ $cash_short }}</span>
			               <span class="info-box-text" style="margin-top: 10px;">Cash Excess : {{ $cash_excess }}</span>
			           </div><!-- /.info-box-content -->
			       </a><!-- /.info-box -->

			   </div>


			   <div class="col-md-4 col-sm-8 col-xs-12">

			       <a href="{{ url('sale') }}" class="info-box info_box_index" style="color: #000; text-decoration: none; background: #ffefef; border: 1px solid #ddd8d8;">
			
			           <span class="info-box-icon bg-yellow"><i class="fa fa-check-square"></i></span>
			           <div class="info-box-content">
			               <span class="info-box-text">All Time Sales</span>
			               <span class="info-box-number" style="margin-top: 10px;">{{ $sales }}</span>
			           </div><!-- /.info-box-content -->
			       </a><!-- /.info-box -->
			   </div>


			    <div class="col-md-4 col-sm-8 col-xs-12">
			       <a href="{{ url('/company-expense') }}" class="info-box info_box_index" style="color: #000; text-decoration: none; background: #ffefef; border: 1px solid #ddd8d8;">			
			           <span class="info-box-icon bg-maroon"><i class="fa fa-money"></i></span>
			           <div class="info-box-content">
			               <span class="info-box-text">Total Company Expenses</span>
			               <span class="info-box-number" style="margin-top: 10px;">{{ $company_expenses }}</span>
			           </div><!-- /.info-box-content -->

			       </a><!-- /.info-box -->
				</div>

			   <div class="col-md-4 col-sm-8 col-xs-12"> 
			       <a href="{{ url('office-expense') }}" class="info-box info_box_index" style="color: #000; text-decoration: none; background: #ffefef; border: 1px solid #ddd8d8;">
			
			           <span class="info-box-icon bg-purple"><i class="fa fa-rupee"></i></span>
			           <div class="info-box-content">
			               <span class="info-box-text">Total Office Expenses</span>
			               <span class="info-box-number" style="margin-top: 10px;">{{ $office_expenses }}</span>
			           </div><!-- /.info-box-content -->
			       </a><!-- /.info-box -->

			   </div>

			   <div class="col-md-4 col-sm-8 col-xs-12">
			       
			       <a href="" class="info-box info_box_index" style="color: #000; text-decoration: none; background: #ffefef; border: 1px solid #ddd8d8;">
			
			           <span class="info-box-icon bg-blue-gradient"><i class="fa fa-book"></i></span>
			           <div class="info-box-content">
			               <span class="info-box-text">Total Absent by All Employeer</span>
			               <span class="info-box-number" style="margin-top: 10px;">{{ $absent_days }}</span>
			           </div><!-- /.info-box-content -->
			       </a><!-- /.info-box -->
			   </div>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>

@endsection