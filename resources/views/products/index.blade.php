@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">All Products</h3>
        <a href="{{ route('product.create') }}" class="text-right btn btn-success"><i class="fa fa-users fa-fw"></i> Add Product</a>
    </div>

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>SKU Code</th>
                            <th>Product Name</th>
                            <th>Details</th>
                            <th>Trade Price</th>
                            <th>List Price</th>
                            <th>MRP</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                            
                    <tbody>
                        @foreach($products as $product)
	                        <tr>

	                            <td>{{ $product->id }}</td>
	                            <td>{{ $product->product_sku_code }}</td>
	                            <td>{{ $product->product_name }}</td>
	                            <td>{{ $product->product_details }}</td>
                                <td>{{ $product->trade_price }}</td>
                                <td>{{ $product->list_price }}</td>
	                            <td>{{ $product->mrp }}</td>
	                            <td>
	                                <a href="{{ route('product.edit',['product' => $product->id]) }}" class="btn btn-warning">
	                                    <i class="fa fa-edit"></i> 
	                                </a>
	                                
	                                <a href="javascript;" class="btn btn-danger" @click.prevent="deleteProduct({{ $product->id }})">
	                                    <i class="fa fa-trash"></i> 
	                                </a>
	                            </td>
	            
	                        </tr>
                        @endforeach
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>

@endsection