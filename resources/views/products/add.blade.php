@extends('layouts.master')
@section('page_main_content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box-header with-border">
						<legend class="box-title" style="text-align: center; font-weight: bold;">Add New Product</legend>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" enctype="multipart/form-data" action="{{ route('product.store') }}">
				@csrf
				<div class="box-body">

					<!-- sku code -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('product_sku_code') ? ' has-error' : '' }}">
								<label for="product_sku_code">Product SKU Code :</label>
								<input type="text" name="product_sku_code" value="{{ old('product_sku_code') }}" class="form-control" placeholder="Enter Details" required="">
								@if ($errors->has('product_sku_code'))
								<span class="help-block">
									<strong>{{ $errors->first('product_sku_code') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					<!-- name -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('product_name') ? ' has-error' : '' }}">
								<label for="product_name">Product Name :</label>
								<input type="text" name="product_name" value="{{ old('product_name') }}" class="form-control" placeholder="Enter Name" required="">
								@if ($errors->has('product_name'))
								<span class="help-block">
									<strong>{{ $errors->first('product_name') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>
					
					
					<!-- Trade price -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('trade_price') ? ' has-error' : '' }}">
								<label for="trade_price">Trade Price :</label>
								<input type="text" name="trade_price" value="{{ old('trade_price') }}" class="form-control" placeholder="Trade Price" required="">
								@if ($errors->has('trade_price'))
								<span class="help-block">
									<strong>{{ $errors->first('trade_price') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>

					<!-- List price -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('list_price') ? ' has-error' : '' }}">
								<label for="list_price">List Price :</label>
								<input type="text" name="list_price" value="{{ old('list_price') }}" class="form-control" placeholder="List Price" required="">
								@if ($errors->has('list_price'))
								<span class="help-block">
									<strong>{{ $errors->first('list_price') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>

					<!-- Trade price -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('mrp') ? ' has-error' : '' }}">
								<label for="mrp">MRP :</label>
								<input type="text" name="mrp" value="{{ old('mrp') }}" class="form-control" placeholder="MRP" required="">
								@if ($errors->has('mrp'))
								<span class="help-block">
									<strong>{{ $errors->first('mrp') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>

					<!-- details -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('product_details') ? ' has-error' : '' }}">
								<label for="product_details">Product Details :</label>
								<input type="text" name="product_details" value="{{ old('product_details') }}" class="form-control" placeholder="Enter Details" required="">
								@if ($errors->has('product_details'))
								<span class="help-block">
									<strong>{{ $errors->first('product_details') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary col-sm-12">Submit</button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
@endsection