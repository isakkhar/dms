@extends('layouts.master')
@section('page_main_content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box-header with-border">
						<legend class="box-title" style="text-align: center; font-weight: bold;">Add New Office Expenses</legend>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" enctype="multipart/form-data" action="{{ route('office-expense.store') }}">
				@csrf
				<div class="box-body">

					<!-- Name -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name">Expense Name :</label>
								{{-- <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Enter Details" required=""> --}}
								<select name="Name" class="form-control" required>
									<option value="">Select Expenses</option>
									<option value='Salary & Wages'>Salary & Wages</option>
									<option value='Redistribution'>Redistribution</option>
									<option value='Loading & Unloading'>Loading & Unloading</option>
									<option value='Entertainment'>Entertainment</option>
									<option value='Conveyance'>Conveyance</option>
									<option value='Stationers'>Stationers</option>
									<option value='Vehicle Maintenance Costs'>Vehicle Maintenance Costs</option>
									<option value='Fuel Costs'>Fuel Costs</option>
									<option value='Internet Bills office'>Internet Bills office</option>
									<option value='Mobile Phone Bills'>Mobile Phone Bills</option>
									<option value='Electricity Bills'>Electricity Bills</option>
									<option value='Office Expences'>Office Expences</option>
									<option value='Rickshaw Van Expenses'>Rickshaw Van Expenses</option>
									<option value='Printing'>Printing</option>
									<option value='WASA Bills'>WASA Bills</option>
									<option value='Electric Goods / Repair'>Electric Goods / Repair</option>
									<option value='Donation'>Donation</option>
									<option value='Trafiq Donation'>Trafiq Donation</option>
									<option value='Vat exp.(Others)'>Vat exp.(Others)</option>
									<option value='other'>Others Office Expenses(Entertainment/PF/Bad Debt/Incidental Expenses etc.)</option>
								</select>
								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					<!-- Amount -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('amount') ? ' has-error' : '' }}">
								<label for="amount">Expense Amount :</label>
								<input type="text" name="amount" value="{{ old('amount') }}" class="form-control" placeholder="Enter Name" required="">
								@if ($errors->has('amount'))
								<span class="help-block">
									<strong>{{ $errors->first('amount') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>
					
					
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary col-sm-12">Submit</button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
@endsection