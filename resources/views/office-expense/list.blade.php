@extends('layouts.master')
@section('page_main_content')
<div class="box">
    <div class="box-header">
        <h1 class="box-title">Office Expenses List from {{$from}} to {{$to}}</h1>
    </div>

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif

<tada></tada>


<section class="content">
	<div class="row">
			<div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr>
                            <th>Serial</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Amount</th>
                            <th>Action</th>

                        </tr>
                    </thead>
                            
                    <tbody>
                        @php $i = 1;  @endphp 
                        @php $sum = 0;  @endphp 
                        @foreach($officeExpenses as $expense)
                        @php $sum += $expense->amount  @endphp 

                            <tr>

                                <td>{{ $i++ }}</td>
                                <td>{{ $expense->created_at->format('m/d/Y') }}</td>
                                <td>{{ $expense->name }}</td>
                                <td>{{ $expense->amount }}</td>
                                <td>
                                    <a href="{{ route('office-expense.edit',['expense' => $expense->id]) }}" class="btn btn-warning">
                                        <i class="fa fa-edit"></i> 
                                    </a>
                                    
                                    <a href="javascript;" class="btn btn-danger" @click.prevent="deleteExpenseOffice({{ $expense->id }})">
                                        <i class="fa fa-trash"></i> 
                                    </a>
                                </td>
                
                            </tr>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Total: {{ $sum ?? '0' }}</td>
                            {{-- <td></td> --}}

                        </tr>
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
		</div>
</section>

</div>
@endsection