@extends('layouts.master')
@section('page_main_content')

@if(session('success'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('success') }}
        </div>
@endif

<div class="box">
    <div class="box-header">
        <h3 class="box-title">All Sales</h3>
        <a href="{{ url('add-stocks') }}" class="text-right btn btn-success"><i class="fa fa-users fa-fw"></i> Add Stocks</a>
    </div>
</div>


<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr>
                            <th>SKU CODE</th>
                            <th>SKU Name</th>
                            <th>Pack Size</th>
                            <th>Available CTN</th>
                            <th>Available PCS</th>
                            <th>Last Update</th>
                        </tr>
                    </thead>
                            
                    <tbody>
                    	@foreach($stocks as $stock)
                    		<tr>
                    			<td>{{ $stock->sku_code }}</td>
                    			<td>{{ $stock->sku_name }}</td>
                    			<td>{{ $stock->pack_size }}</td>
                    			<td>{{ $stock->available_ctn }}</td>
                    			<td>{{ $stock->available_pcs }}</td>
                    			<td>{{ Carbon\Carbon::parse($stock->updated_at)->format('d-m-Y') }}</td>
                    		</tr>
                    	@endforeach
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>


@endsection