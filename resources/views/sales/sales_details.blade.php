@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Sales Details</h3>
    </div>
</div>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-header">
        	<p>Salesman : <strong>{{ $employee->name }}</strong></p>
        	<p>Sales Date : <strong>{{ Carbon\Carbon::parse($sales_value_details->created_at->toDateString())->format('d-m-Y') }}</strong></p>
        </div>
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr>
                            <th>SKU Code</th>
                            <th>SKU</th>
                            <th>Pack Size</th>
                            <th>Ctn Sale</th>
                            <th>Unit Sale</th>
                            <th>Ctn Free</th>
                            <th>Unit Free</th>
                            <th>Ctn Return</th>
                            <th>Unit Return</th>
                            <th>Sales Value</th>
                        </tr>
                    </thead>
                            
                    <tbody>
                    	@foreach($sales_details as $sale)
	                    	<tr>
	                    		<td>{{ $sale->sku_code }}</td>
	                    		<td>{{ $sale->sku }}</td>
	                    		<td>{{ $sale->pack_size }}</td>
	                    		<td>{{ $sale->ctn_sale }}</td>
	                    		<td>{{ $sale->unit_sale }}</td>
	                    		<td>{{ $sale->ctn_free }}</td>
	                    		<td>{{ $sale->unit_free }}</td>
	                    		<td>{{ $sale->ctn_return }}</td>
	                    		<td>{{ $sale->unit_return }}</td>
	                    		<td>{{ $sale->sales_value }}</td>
	                    	</tr>            
	                    @endforeach
	                        
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="row">
  	<div class="col-md-12">
      <div class="table-responsive">
        <table id="example1" class="table table-bordered">
          <thead>
              <tr>
                  <th>Cash Issue</th>
                  <th>Free</th>
                  <th>Discount</th>
                  <th>Promo Discount</th>
                  <th>Credit Given</th>
                  <th>Prev Credit Collected</th>
                  <th>Expenses</th>
                  <th>Market Return</th>
                  <th>Store Discount</th>
                  <th>Cash To Be Deposite</th>
                  <th>Cash Given</th>
                  <th>Cash Short</th>
              </tr>
          </thead>               
                                  
          <tbody>
               <tr>
                  <td>{{ $sales_value_details->cash_issue }}</td>
                  <td>{{ $sales_value_details->free }}</td>
                  <td>{{ $sales_value_details->discount }}</td>
                  <td>{{ $sales_value_details->promo_discount }}</td>
                  <td>{{ $sales_value_details->credit_given }}</td>
                  <td>{{ $sales_value_details->prev_credit_collected }}</td>
                  <td>{{ $sales_value_details->expenses }}</td>
                  <td>{{ $sales_value_details->market_return }}</td>
                  <td>{{ $sales_value_details->store_discount }}</td>
                  <td>{{ $sales_value_details->cash_to_be_deposite }}</td>
                  <td>{{ $sales_value_details->cash_deposited }}</td>
                  <td>{{ $sales_value_details->cash_short }}</td>
               </tr> 
          </tbody>                
        </table>
      </div>
  		                
  	</div>
  </div>		
</section>


@endsection