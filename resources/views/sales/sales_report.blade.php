@extends('layouts.master')
@section('page_main_content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box-header with-border">
						<legend class="box-title" style="text-align: center; font-weight: bold;">Genarate Sales Report</legend>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" enctype="multipart/form-data" action="{{ url('/generate-sales-report') }}">
				@csrf

				<div class="box-body">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-2">
							<label for="from">From :</label>
						</div>
						<div class="col-md-4">
							<input type="date" name="from" value="{{ request('from', date('y-m-d')) }}" class="form-control" required="">
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>
				
				<div class="box-body">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-2">
							<label for="to">To :</label>
						</div>
						<div class="col-md-4">
							<input type="date" name="to" value="{{ request('from', date('y-m-d')) }}" class="form-control" required="">
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>

				<!-- /.box-body -->
				<div class="box-footer">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary col-sm-12">Generate</button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>

@if(isset($sales_details))

<button onclick="detailsToCSV('sales_report.csv')" class="btn btn-primary">Save as CSV</button><br><br>
<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<!-- /.box-header -->
			            <div class="table-responsive">
			                <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info">
			                    <thead>
			                        <tr>
			                            <th>Cash Issue</th>
			                            <th>Free</th>
			                            <th>Discount</th>
			                            <th>Promo Discount</th>
			                            <th>Credit Given</th>
			                            <th>Prev. Credit Given</th>
			                            <th>Expenses</th>
			                            <th>Market Return</th>
			                            <th>Store Discount</th>
			                            <th>Cash to be Deposite</th>
			                            <th>Cash Deposite</th>
			                            <th>Cash Short</th>
			                        </tr>
			                    </thead>
			                            
			                    <tbody>
			                    	@foreach($sales_details as $each)
			                    		<tr>
			                    			<td>{{ round($each->cash_issue) }}</td>
			                    			<td>{{ round($each->free) }}</td>
			                    			<td>{{ round($each->discount) }}</td>
			                    			<td>{{ round($each->promo_discount) }}</td>
			                    			<td>{{ round($each->credit_given) }}</td>
			                    			<td>{{ round($each->prev_credit_collected) }}</td>
			                    			<td>{{ round($each->expenses) }}</td>
			                    			<td>{{ round($each->market_return) }}</td>
			                    			<td>{{ round($each->store_discount) }}</td>
			                    			<td>{{ round($each->cash_to_be_deposite) }}</td>
			                    			<td>{{ round($each->cash_deposited) }}</td>
			                    			<td>{{ round($each->cash_short) }}</td>
			                    		</tr>
			                    	@endforeach     
			                    </tbody>
			                            
			                </table>
			            </div>
		</div>
		<!-- /.box -->
	</div>
</div>

<script type="text/javascript">

    function downloadCSV(csv, filename) {
        var csvFile;
        var downloadLink;

        // CSV file
        csvFile = new Blob([csv], {type: "text/csv"});

        // Download link
        downloadLink = document.createElement("a");

        // File name
        downloadLink.download = filename;

        // Create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);

        // Hide download link
        downloadLink.style.display = "none";

        // Add the link to DOM
        document.body.appendChild(downloadLink);

        // Click download link
        downloadLink.click();
    }

    function detailsToCSV(filename) {
        var csv = [];
        var rows = document.querySelectorAll("table tr");

        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");

            for (var j = 0; j < cols.length; j++)
                if (j == 6 || j == 7) {

                }
                else {
                    row.push(cols[j].innerText);
                }

            csv.push(row.join(","));
        }

        // Download CSV file
        downloadCSV(csv.join("\n"), filename);
    }

</script>
@endif
@endsection