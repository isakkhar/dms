@extends('layouts.master')
@section('page_main_content')

@if(session('success'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('success') }}
        </div>
@endif

<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box-header with-border">
						<legend class="box-title" style="text-align: center; font-weight: bold;">Add New Sales</legend>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" enctype="multipart/form-data" action="{{ url('/import') }}">
				@csrf

				<div class="row">
					<div class="col-md-3"></div>
					<div class="form-group">
						<div class="col-md-2">
							<label for="employee">Select Employee :</label>
						</div>
						<div class="col-md-4">	
							
							<select name="employee" class="form-control" required="">
								<option value="">Select</option>
								@foreach($employee_data as $employee)
									<option value="{{$employee->id}}">{{ $employee->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-3"></div>
				</div>

				<div class="box-body">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-2">
							<label for="cash_deposite">Cash Deposite/ Given :</label>
						</div>
						<div class="col-md-4">
							<input type="number" name="cash_deposite" value="{{ old('cash_deposite') }}" class="form-control" placeholder="Enter Amount" required="">
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>

				<div class="box-body">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-2">
							<label for="date">Date :</label>
						</div>
						<div class="col-md-4">
							<input type="date" name="date" class="form-control" required="" value="{{ old('date', date('Y-m-d')) }}">
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>

				<div class="box-body">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-2">
							<label for="excel_file">Select Sales Excel File :</label>
						</div>
						<div class="col-md-4">
							<input type="file" name="excel_file" class="form-control" required="">
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>
				
				<div class="box-body">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-2">
							<label for="due">Add Due :</label>
						</div>
						<div class="col-md-4">
							<button type="button" id="due" class="btn btn-info btn-sm">Add</button>
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>

				<div class="box-body">
					<div class="row">
						<div class="col-md-5"></div>
						<div class="col-md-4 shop_due">
							
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>

				<div class="box-body">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-2">
							<label for="due">Cheque Payment :</label>
						</div>
						<div class="col-md-4">
							<button type="button" id="cheque" class="btn btn-info btn-sm">Add</button>
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>

				<div class="box-body">
					<div class="row">
						<div class="col-md-5"></div>
						<div class="col-md-4 cheque_payment">
							
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>

				<div class="box-body">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-2">
							<label for="due">Due Realize :</label>
						</div>
						<div class="col-md-4">
							<button type="button" id="due_realize" class="btn btn-info btn-sm">Add</button>
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>

				<div class="box-body">
					<div class="row">
						<div class="col-md-5"></div>
						<div class="col-md-4 due_realize">
							
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>

				<!-- /.box-body -->
				<div class="box-footer">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary col-sm-12">Submit <i class="fa fa-check"></i></button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>

<script src="https://code.jquery.com/jquery-3.4.1.js"></script>









<script type="text/javascript">
	  
	  //due
	  $(document).ready(function(){
	      $("#due").click(function(){
	          $(".shop_due").append(`
	    <div><select name="shop_due[]" class="form-control">
	    	<option value="">--- select ---</option>
	    	<?php
	    	foreach($shops as $shop)
	    	{ ?>
	    		<option value="<?= $shop->id; ?>"><?= $shop->shop_name; ?></option>
	    	<?php } ?>
	    	
	    </select>
		<input type="number" name="due_amount[]" class="form-control" style="margin-top:5px;" placeholder="Enter Due Amount" required>
		<a href="#" class="remove">Remove</a></<div>
	    	
	    </div>
	    <br>
	  `); 
	      });
	      $(document).on("click", "a.remove" , function() {
	          $(this).parent('div').remove();
	      });
	  });	

	  	//cheque payment
	    $(document).ready(function(){
	        $("#cheque").click(function(){
	            $(".cheque_payment").append(`
	      <div><select name="shop_cheque_pay[]" class="form-control">
	      	<option value="">--- select ---</option>
	      	<?php
	      	foreach($shops as $shop)
	      	{ ?>
	      		<option value="<?= $shop->id; ?>"><?= $shop->shop_name; ?></option>
	      	<?php } ?>
	      	
	      </select>
	  	<input type="number" name="cheque_amount[]" class="form-control" style="margin-top:5px;" placeholder="Enter Amount" required>
	  	<input type="number" name="cheque_number[]" class="form-control" style="margin-top:5px;" placeholder="Enter Cheque Number" required>
	  	<input type="date" name="deposite_date[]" class="form-control" value="<?php echo date('Y-m-d');?>" style="margin-top:5px;">

	  	<a href="#" class="remove2">Remove</a></<div>
	      	
	      </div>
	      <br>
	    `); 
	        });
	        $(document).on("click", "a.remove2" , function() {
	            $(this).parent('div').remove();
	        });
	    });

	      //due realize  
	      $(document).ready(function(){
	          $("#due_realize").click(function(){
	              $(".due_realize").append(`
	        <div><select name="shop_due_realize[]" class="form-control">
	        	<option value="">--- select ---</option>
	        	<?php
	        	foreach($shops as $shop)
	        	{ ?>
	        		<option value="<?= $shop->id; ?>"><?= $shop->shop_name; ?></option>
	        	<?php } ?>
	        	
	        </select>
	    	<input type="number" name="amount[]" class="form-control" style="margin-top:5px;" placeholder="Enter Amount" required>
	    	<select name="payment_type[]" class="form-control" style="margin-top:5px;">
	    		        	<option value="">--- select ---</option>
	    		    		<option value="cash">Cash</option>
	    		    		<option value="cheque">Cheque</option>
	    		        </select>
	    	<input type="number" name="due_realize_cheque_number[]" class="form-control" style="margin-top:5px;" placeholder="Cheque Number(if payment method is cheque)">
	    	<input type="date" name="deposite_date_due_realize[]" class="form-control" value="<?php echo date('Y-m-d');?>" style="margin-top:5px;">
	    	<a href="#" class="remove3">Remove</a></<div>
	        </div><br>
	      `); 
	          });
	          $(document).on("click", "a.remove3" , function() {
	              $(this).parent('div').remove();
	          });
	      });
</script>

@endsection