@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">All Sales</h3>
        <a href="{{ route('sale.create') }}" class="text-right btn btn-success"><i class="fa fa-users fa-fw"></i> Add Sale</a>
    </div>
</div>


<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info" style="text-align: center;">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Sale ID</th>
                            <th style="text-align: center;">Salesman</th>
                            <th style="text-align: center;">Sale Date</th>
                            <th style="text-align: center;">View Sales Details</th>
                            <th style="text-align: center;">Edit</th>
                        </tr>
                    </thead>
                            
                    <tbody>
                    	@foreach($sales as $sale)
	                    	<tr>
	                    		<td>{{ $sale->id }}</td>
	                    		<td>{{ $sale->name }}</td>
	                    		<td>{{ Carbon\Carbon::parse($sale->created_at->toDateString())->format('d-M-Y') }}</td>
	                    		<td>
                                    <form action="{{ url('/sales_details/'.$sale->id) }}" method="get" target="new">
                                        <input type="hidden" name="date" value="{{ Carbon\Carbon::parse($sale->created_at->toDateString())->format('Y-m-d') }}">
                                        <input type="hidden" name="employee" value="{{ $sale->employee_id }}">
                                        <button type="submit" class="btn btn-info">Details</button>
                                    </form>
	                    		</td>
                                <td>
                                    <a href="{{ url('/sales-edit/'.$sale->id) }}" title="Edit" class="btn btn-primary" target="_blank"><i class="fa fa-edit"></i></a>
                                </td>
	                    	</tr>            
	                    @endforeach
	                        
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>




@endsection