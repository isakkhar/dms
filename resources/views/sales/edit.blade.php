@extends('layouts.master')
@section('page_main_content')

@if(session('success'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('success') }}
        </div>
@endif

<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box-header with-border">
						<legend class="box-title" style="text-align: center; font-weight: bold;">Edit Sales</legend>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" action="{{ url('/sales-update/'.$sales_info->id) }}">
				@csrf
				@method('PUT')
				<div class="box-body">
					<div class="row">
						<div class="col-md-1"></div>
						<div class="col-md-4">
							<label for="cash_deposite" style="float: right;">Edit Cash Deposite/ Given :</label>
						</div>
						<div class="col-md-4">
							<input type="number" name="cash_deposite" value="{{ $sales_info->cash_deposited }}" class="form-control" required="">
						</div>
						<div class="col-md-3"></div>
					</div>		
				</div>

				<!-- /.box-body -->
				<div class="box-footer">
					<div class="row">
						<div class="col-md-5"></div>
						<div class="col-md-4">
							<button type="submit" class="btn btn-primary col-sm-12">Submit <i class="fa fa-check"></i></button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
@endsection