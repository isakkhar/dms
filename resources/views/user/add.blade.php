@extends('layouts.master')

@section('page_main_content')


 
   <div class="row">
	  <div class="col-sm-12">  
	  	@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
        @endif
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Add New User</h3>				
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" action="{{ route('user.create') }}">
				@csrf
				<div class="box-body">

					
						<div class="form-group col-sm-3 {{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name">Name</label>
						<input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Enter Name" required="">
						@if ($errors->has('name'))
						<span class="help-block">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
						@endif
					</div>
					
					<div class="form-group col-sm-3 {{ $errors->has('email') ? ' has-error' : '' }}">
						<label for="email">Email address</label>
						<input type="email" name="email" class="form-control" placeholder="Enter email" value="{{ old('email') }}" required="">
						@if ($errors->has('email'))
						<span class="help-block">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group col-sm-3 {{ $errors->has('password') ? ' has-error' : '' }}">
						<label for="password">Password</label>
						<input type="password" name="password" class="form-control" placeholder="Enter password" required="">
						@if ($errors->has('password'))
						<span class="help-block">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group col-sm-3 {{ $errors->has('mobile') ? ' has-error' : '' }}">
						<label for="mobile">Phone Number</label>
						<input type="text" name="mobile" class="form-control" placeholder="Enter Phone Number" value="{{ old('mobile') }}" required="">
						@if ($errors->has('mobile'))
						<span class="help-block">
							<strong>{{ $errors->first('mobile') }}</strong>
						</span>
						@endif
					</div>
					

					
						
					

					<div class="form-group col-sm-3 {{ $errors->has('role') ? ' has-error' : '' }} clearfix">
						<label for="role">Role</label>
						<select name="role" class="form-control" required="">
							<option value="">Select</option>
							<option value="Super Admin" {{ old('role') == 'Super Admin' ? 'selected': '' }}>Super Admin</option>
							<option value="User" {{ old('role') == 'User' ? 'selected': '' }}>User</option>
							
						</select>
						@if ($errors->has('role'))
						<span class="help-block">
							<strong>{{ $errors->first('role') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group col-sm-3 {{ $errors->has('designation') ? ' has-error' : '' }}">
						<label for="designation">Designation</label>
						<select name="designation" class="form-control" required="">
							<option value="">Select</option>
							<option value="Super Admin" {{ old('role') == 'Super Admin' ? 'selected': '' }}>Super Admin</option>
							<option value="IT" {{ old('role') == 'IT' ? 'selected': '' }}>IT</option>
							<option value="Manager" {{ old('role') == 'Manager' ? 'selected': '' }}>Manager</option>
							<option value="Accounts" {{ old('role') == 'Accounts' ? 'selected': '' }}>Accounts</option>
						</select>
						@if ($errors->has('designation'))
						<span class="help-block">
							<strong>{{ $errors->first('designation') }}</strong>
						</span>
						@endif
					</div>
                    <div class="form-group col-sm-3 {{ $errors->has('address') ? ' has-error' : '' }}">
                        <label>Address</label>
                        <textarea name="address" class="form-control input-sm" rows="3" required="">
                        	{{ trim(old('address')) }}
                        </textarea>

                        @if ($errors->has('address'))
						<span class="help-block">
							<strong>{{ $errors->first('address') }}</strong>
						</span>
						@endif
                    </div>
                    <div class="form-group col-sm-3 {{ $errors->has('gender') ? ' has-error' : '' }}">
						<label>Gender: </label><br>
						<label>
							<input type="radio" value="m" name="gender" {{ old('gender') == 'm' ? 'checked': '' }} required=""> Male
						</label>
						<label>
							<input type="radio" value="f" name="gender" {{ old('gender') == 'f' ? 'checked': '' }} required=""> Female
						</label>
						@if ($errors->has('gender'))
						<span class="help-block">
							<strong>{{ $errors->first('gender') }}</strong>
						</span>
						@endif
					</div>
				</div>
					
					

					
				<!-- /.box-body -->
				<div class="box-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>

 	
 	


@endsection