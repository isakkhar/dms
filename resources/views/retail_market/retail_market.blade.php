@extends('layouts.master')
@section('page_main_content')


@if(count($employee_expenses) > 0)
<div class="box">
    <div class="box-header">
        <h2 class="box-title">Unilever Retail Market Credit Information</h2>
        <h3>Period: 
		@php
			switch ($month) 
			{
			    case 1:
			        echo "January";
			        break;
			    case 2:
			        echo "February";
			        break;
			    case 3:
			        echo "March";
			        break;
			    case 4:
			        echo "April";
			        break;
			    case 5:
			        echo "May";
			        break;
			    case 6:
			        echo "June";
			        break;
			    case 7:
			        echo "July";
			        break;
			    case 8:
			        echo "August";
			        break;
			    case 9:
			        echo "September";
			        break;
				case 10:
				    echo "October";
				    break;
				case 11:
				    echo "November";
				    break;
				case 12:
				    echo "December";
				    break;
			}

		@endphp
		{{ $year }}
        </h3>
    </div>
</div>

<div class="table-responsive">
	<table class="table table-bordered table-hover dataTable" style="font-weight: bold;">
    	<thead>
    		<tr style="background-color: #3C8DBC; color: #fff;">
    		    <th>ID</th>
    		    <th>NAME</th>
    		    <th>DIVISION</th>
    		    <th>b/f</th>
    		    <th>1</th>
    		    <th>2</th>
    		    <th>3</th>
    		    <th>4</th>
    		    <th>5</th>
    		    <th>6</th>
    		    <th>7</th>
    		    <th>8</th>
    		    <th>9</th>
    		    <th>10</th>
    		    <th>11</th>
    		    <th>12</th>
    		    <th>13</th>
    		    <th>14</th>
    		    <th>15</th>
    		    <th>16</th>
    		    <th>17</th>
    		    <th>18</th>
    		    <th>19</th>
    		    <th>20</th>
    		    <th>21</th>
    		    <th>22</th>
    		    <th>23</th>
    		    <th>24</th>
    		    <th>25</th>
    		    <th>26</th>
    		    <th>27</th>
    		    <th>28</th>
    		    <th>29</th>
    		    <th>30</th>
    		    <th>31</th>
    		    <th>Total Due</th>
    		    <th>Due Left</th>
    		    <th>Total Paid</th>
    		    <th>1</th>
    		    <th>2</th>
    		    <th>3</th>
    		    <th>4</th>
    		    <th>5</th>
    		    <th>6</th>
    		    <th>7</th>
    		    <th>8</th>
    		    <th>9</th>
    		    <th>10</th>
    		    <th>11</th>
    		    <th>12</th>
    		    <th>13</th>
    		    <th>14</th>
    		    <th>15</th>
    		    <th>16</th>
    		    <th>17</th>
    		    <th>18</th>
    		    <th>19</th>
    		    <th>20</th>
    		    <th>21</th>
    		    <th>22</th>
    		    <th>23</th>
    		    <th>24</th>
    		    <th>25</th>
    		    <th>26</th>
    		    <th>27</th>
    		    <th>28</th>
    		    <th>29</th>
    		    <th>30</th>
    		    <th>31</th>
    		</tr>
    	</thead>

    	<tbody>
    		@php $total_due_this_month = 0; @endphp
    	    		@foreach($employees as $key => $employee)
    		    		<tr>
    		    			<td style="background-color: #C0C0C0;">{{ $employee->id }}</td>
    		    			<td style="background-color: #C0C0C0;">{{ $employee->name }}</td>
    		    			<td style="background-color: #C0C0C0;">{{ $employee->department }}</td>
    		    			<td style="background-color: #99CCFF;">
    		    				{{ ($cash_flows[$key]->cash_short - $cash_flows[$key]->cash_excess) - ($cash_short[$key] - $cash_excess[$key]) }}
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($one) > 0)
    		    				@for($i = 0; $i < count($one); $i++)
    		    					@if((count($one[$i]) > 0) && ($one[$i][0]['expenses_type'] == 0) && ($one[$i][0]['employee_id'] == $employee->id))
    		    						{{ $one[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $one[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($two) > 0)
    		    				@for($i = 0; $i < count($two); $i++)
    		    					@if((count($two[$i]) > 0) && ($two[$i][0]['expenses_type'] == 0) && ($two[$i][0]['employee_id'] == $employee->id))
    		    						{{ $two[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $two[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($three) > 0)
    		    				@for($i = 0; $i < count($three); $i++)
    		    					@if((count($three[$i]) > 0) && ($three[$i][0]['expenses_type'] == 0) && ($three[$i][0]['employee_id'] == $employee->id))
    		    						{{ $three[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $three[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($four) > 0)
    		    				@for($i = 0; $i < count($four); $i++)
    		    					@if((count($four[$i]) > 0) && ($four[$i][0]['expenses_type'] == 0) && ($four[$i][0]['employee_id'] == $employee->id))
    		    						{{ $four[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $four[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($five) > 0)
    		    				@for($i = 0; $i < count($five); $i++)
    		    					@if((count($five[$i]) > 0) && ($five[$i][0]['expenses_type'] == 0) && ($five[$i][0]['employee_id'] == $employee->id))
    		    						{{ $five[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $five[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($six) > 0)
    		    				@for($i = 0; $i < count($six); $i++)
    		    					@if((count($six[$i]) > 0) && ($six[$i][0]['expenses_type'] == 0) && ($six[$i][0]['employee_id'] == $employee->id))
    		    						{{ $six[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $six[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($seven) > 0)
    		    				@for($i = 0; $i < count($seven); $i++)
    		    					@if((count($seven[$i]) > 0) && ($seven[$i][0]['expenses_type'] == 0) && ($seven[$i][0]['employee_id'] == $employee->id))
    		    						{{ $seven[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $seven[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($eigth) > 0)
    		    				@for($i = 0; $i < count($eigth); $i++)
    		    					@if((count($eigth[$i]) > 0) && ($eigth[$i][0]['expenses_type'] == 0) && ($eigth[$i][0]['employee_id'] == $employee->id))
    		    						{{ $eigth[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $eigth[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($nine) > 0)
    		    				@for($i = 0; $i < count($nine); $i++)
    		    					@if((count($nine[$i]) > 0) && ($nine[$i][0]['expenses_type'] == 0) && ($nine[$i][0]['employee_id'] == $employee->id))
    		    						{{ $nine[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $nine[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($ten) > 0)
    		    				@for($i = 0; $i < count($ten); $i++)
    		    					@if((count($ten[$i]) > 0) && ($ten[$i][0]['expenses_type'] == 0) && ($ten[$i][0]['employee_id'] == $employee->id))
    		    						{{ $ten[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $ten[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($eleven) > 0)
    		    				@for($i = 0; $i < count($eleven); $i++)
    		    					@if((count($eleven[$i]) > 0) && ($eleven[$i][0]['expenses_type'] == 0) && ($eleven[$i][0]['employee_id'] == $employee->id))
    		    						{{ $eleven[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $eleven[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($twelve) > 0)
    		    				@for($i = 0; $i < count($twelve); $i++)
    		    					@if((count($twelve[$i]) > 0) && ($twelve[$i][0]['expenses_type'] == 0) && ($twelve[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twelve[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $twelve[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($thirteen) > 0)
    		    				@for($i = 0; $i < count($thirteen); $i++)
    		    					@if((count($thirteen[$i]) > 0) && ($thirteen[$i][0]['expenses_type'] == 0) && ($thirteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $thirteen[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $thirteen[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($fourteen) > 0)
    		    				@for($i = 0; $i < count($fourteen); $i++)
    		    					@if((count($fourteen[$i]) > 0) && ($fourteen[$i][0]['expenses_type'] == 0) && ($fourteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $fourteen[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $fourteen[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($fifteen) > 0)
    		    				@for($i = 0; $i < count($fifteen); $i++)
    		    					@if((count($fifteen[$i]) > 0) && ($fifteen[$i][0]['expenses_type'] == 0) && ($fifteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $fifteen[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $fifteen[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($sixteen) > 0)
    		    				@for($i = 0; $i < count($sixteen); $i++)
    		    					@if((count($sixteen[$i]) > 0) && ($sixteen[$i][0]['expenses_type'] == 0) && ($sixteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $sixteen[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $sixteen[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($seventeen) > 0)
    		    				@for($i = 0; $i < count($seventeen); $i++)
    		    					@if((count($seventeen[$i]) > 0) && ($seventeen[$i][0]['expenses_type'] == 0) && ($seventeen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $seventeen[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $seventeen[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($eigthteen) > 0)
    		    				@for($i = 0; $i < count($eigthteen); $i++)
    		    					@if((count($eigthteen[$i]) > 0) && ($eigthteen[$i][0]['expenses_type'] == 0) && ($eigthteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $eigthteen[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $eigthteen[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($ninteen) > 0)
    		    				@for($i = 0; $i < count($ninteen); $i++)
    		    					@if((count($ninteen[$i]) > 0) && ($ninteen[$i][0]['expenses_type'] == 0) && ($ninteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $ninteen[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $ninteen[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($twenty) > 0)
    		    				@for($i = 0; $i < count($twenty); $i++)
    		    					@if((count($twenty[$i]) > 0) && ($twenty[$i][0]['expenses_type'] == 0) && ($twenty[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twenty[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $twenty[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($twentyone) > 0)
    		    				@for($i = 0; $i < count($twentyone); $i++)
    		    					@if((count($twentyone[$i]) > 0) && ($twentyone[$i][0]['expenses_type'] == 0) && ($twentyone[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentyone[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $twentyone[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($twentytwo) > 0)
    		    				@for($i = 0; $i < count($twentytwo); $i++)
    		    					@if((count($twentytwo[$i]) > 0) && ($twentytwo[$i][0]['expenses_type'] == 0) && ($twentytwo[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentytwo[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $twentytwo[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($twentythree) > 0)
    		    				@for($i = 0; $i < count($twentythree); $i++)
    		    					@if((count($twentythree[$i]) > 0) && ($twentythree[$i][0]['expenses_type'] == 0) && ($twentythree[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentythree[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $twentythree[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($twentyfour) > 0)
    		    				@for($i = 0; $i < count($twentyfour); $i++)
    		    					@if((count($twentyfour[$i]) > 0) && (twentyfourw[$i][0]['expenses_type'] == 0) && ($tw[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentyfour[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $twentyfour[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($twentyfive) > 0)
    		    				@for($i = 0; $i < count($twentyfive); $i++)
    		    					@if((count($twentyfive[$i]) > 0) && ($twentyfive[$i][0]['expenses_type'] == 0) && ($twentyfive[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentyfive[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $twentyfive[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($twentysix) > 0)
    		    				@for($i = 0; $i < count($twentysix); $i++)
    		    					@if((count($twentysix[$i]) > 0) && ($twentysix[$i][0]['expenses_type'] == 0) && ($twentysix[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentysix[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $twentysix[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($twentyseven) > 0)
    		    				@for($i = 0; $i < count($twentyseven); $i++)
    		    					@if((count($twentyseven[$i]) > 0) && ($twentyseven[$i][0]['expenses_type'] == 0) && ($twentyseven[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentyseven[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $twentyseven[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($twentyeight) > 0)
    		    				@for($i = 0; $i < count($twentyeight); $i++)
    		    					@if((count($twentyeight[$i]) > 0) && ($twentyeight[$i][0]['expenses_type'] == 0) && ($twentyeight[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentyeight[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $twentyeight[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($twentynine) > 0)
    		    				@for($i = 0; $i < count($twentynine); $i++)
    		    					@if((count($twentynine[$i]) > 0) && ($twentynine[$i][0]['expenses_type'] == 0) && ($twentynine[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentynine[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $twentynine[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($thirty) > 0)
    		    				@for($i = 0; $i < count($thirty); $i++)
    		    					@if((count($thirty[$i]) > 0) && ($thirty[$i][0]['expenses_type'] == 0) && ($thirty[$i][0]['employee_id'] == $employee->id))
    		    						{{ $thirty[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $thirty[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #99CCFF;">
    		    				@if(count($thirtyone) > 0)
    		    				@for($i = 0; $i < count($thirtyone); $i++)
    		    					@if((count($thirtyone[$i]) > 0) && ($thirtyone[$i][0]['expenses_type'] == 0) && ($thirtyone[$i][0]['employee_id'] == $employee->id))
    		    						{{ $thirtyone[$i][0]['expenses_amount'] }}
    		    						@php $total_due_this_month += $thirtyone[$i][0]['expenses_amount']; @endphp
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>


    		    			<td style="background-color: #FFFF99;">{{ (($cash_flows[$key]->cash_short - $cash_flows[$key]->cash_excess) - ($cash_short[$key] - $cash_excess[$key])) + ($cash_short[$key]) }}</td>
    		    			<td style="background-color: #FFCC99;">
    		    				{{ $cash_flows[$key]->cash_short - $cash_flows[$key]->cash_excess }} 
    		    			</td>
    		    			<td style="background-color: #FFFF99;">{{ $cash_excess[$key] }}</td>


    		    			<td style="background-color: #008000;">
    		    				@if(count($one) > 0)
    		    				@for($i = 0; $i < count($one); $i++)
    		    					@if((count($one[$i]) > 0) && ($one[$i][0]['expenses_type'] == 1) && ($one[$i][0]['employee_id'] == $employee->id))
    		    						{{ $one[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($two) > 0)
    		    				@for($i = 0; $i < count($two); $i++)
    		    					@if((count($two[$i]) > 0) && ($two[$i][0]['expenses_type'] == 1) && ($two[$i][0]['employee_id'] == $employee->id))
    		    						{{ $two[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($three) > 0)
    		    				@for($i = 0; $i < count($three); $i++)
    		    					@if((count($three[$i]) > 0) && ($three[$i][0]['expenses_type'] == 1) && ($three[$i][0]['employee_id'] == $employee->id))
    		    						{{ $three[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($four) > 0)
    		    				@for($i = 0; $i < count($four); $i++)
    		    					@if((count($four[$i]) > 0) && ($four[$i][0]['expenses_type'] == 1) && ($four[$i][0]['employee_id'] == $employee->id))
    		    						{{ $four[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($five) > 0)
    		    				@for($i = 0; $i < count($five); $i++)
    		    					@if((count($five[$i]) > 0) && ($five[$i][0]['expenses_type'] == 1) && ($five[$i][0]['employee_id'] == $employee->id))
    		    						{{ $five[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($six) > 0)
    		    				@for($i = 0; $i < count($six); $i++)
    		    					@if((count($six[$i]) > 0) && ($six[$i][0]['expenses_type'] == 1) && ($six[$i][0]['employee_id'] == $employee->id))
    		    						{{ $six[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($seven) > 0)
    		    				@for($i = 0; $i < count($seven); $i++)
    		    					@if((count($seven[$i]) > 0) && ($seven[$i][0]['expenses_type'] == 1) && ($seven[$i][0]['employee_id'] == $employee->id))
    		    						{{ $seven[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($eigth) > 0)
    		    				@for($i = 0; $i < count($eigth); $i++)
    		    					@if((count($eigth[$i]) > 0) && ($eigth[$i][0]['expenses_type'] == 1) && ($eigth[$i][0]['employee_id'] == $employee->id))
    		    						{{ $eigth[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($nine) > 0)
    		    				@for($i = 0; $i < count($nine); $i++)
    		    					@if((count($nine[$i]) > 0) && ($nine[$i][0]['expenses_type'] == 1) && ($nine[$i][0]['employee_id'] == $employee->id))
    		    						{{ $nine[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($ten) > 0)
    		    				@for($i = 0; $i < count($ten); $i++)
    		    					@if((count($ten[$i]) > 0) && ($ten[$i][0]['expenses_type'] == 1) && ($ten[$i][0]['employee_id'] == $employee->id))
    		    						{{ $ten[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($eleven) > 0)
    		    				@for($i = 0; $i < count($eleven); $i++)
    		    					@if((count($eleven[$i]) > 0) && ($eleven[$i][0]['expenses_type'] == 1) && ($eleven[$i][0]['employee_id'] == $employee->id))
    		    						{{ $eleven[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($twelve) > 0)
    		    				@for($i = 0; $i < count($twelve); $i++)
    		    					@if((count($twelve[$i]) > 0) && ($twelve[$i][0]['expenses_type'] == 1) && ($twelve[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twelve[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($thirteen) > 0)
    		    				@for($i = 0; $i < count($thirteen); $i++)
    		    					@if((count($thirteen[$i]) > 0) && ($thirteen[$i][0]['expenses_type'] == 1) && ($thirteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $thirteen[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($fourteen) > 0)
    		    				@for($i = 0; $i < count($fourteen); $i++)
    		    					@if((count($fourteen[$i]) > 0) && ($fourteen[$i][0]['expenses_type'] == 1) && ($fourteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $fourteen[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($fifteen) > 0)
    		    				@for($i = 0; $i < count($fifteen); $i++)
    		    					@if((count($fifteen[$i]) > 0) && ($fifteen[$i][0]['expenses_type'] == 1) && ($fifteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $fifteen[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($sixteen) > 0)
    		    				@for($i = 0; $i < count($sixteen); $i++)
    		    					@if((count($sixteen[$i]) > 0) && ($sixteen[$i][0]['expenses_type'] == 1) && ($sixteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $sixteen[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($seventeen) > 0)
    		    				@for($i = 0; $i < count($seventeen); $i++)
    		    					@if((count($seventeen[$i]) > 0) && ($seventeen[$i][0]['expenses_type'] == 1) && ($seventeen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $seventeen[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($eigthteen) > 0)
    		    				@for($i = 0; $i < count($eigthteen); $i++)
    		    					@if((count($eigthteen[$i]) > 0) && ($eigthteen[$i][0]['expenses_type'] == 1) && ($eigthteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $eigthteen[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($ninteen) > 0)
    		    				@for($i = 0; $i < count($ninteen); $i++)
    		    					@if((count($ninteen[$i]) > 0) && ($ninteen[$i][0]['expenses_type'] == 1) && ($ninteen[$i][0]['employee_id'] == $employee->id))
    		    						{{ $ninteen[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($twenty) > 0)
    		    				@for($i = 0; $i < count($twenty); $i++)
    		    					@if((count($twenty[$i]) > 0) && ($twenty[$i][0]['expenses_type'] == 1) && ($twenty[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twenty[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($twentyone) > 0)
    		    				@for($i = 0; $i < count($twentyone); $i++)
    		    					@if((count($twentyone[$i]) > 0) && ($twentyone[$i][0]['expenses_type'] == 1) && ($twentyone[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentyone[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($twentytwo) > 0)
    		    				@for($i = 0; $i < count($twentytwo); $i++)
    		    					@if((count($twentytwo[$i]) > 0) && ($twentytwo[$i][0]['expenses_type'] == 1) && ($twentytwo[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentytwo[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($twentythree) > 0)
    		    				@for($i = 0; $i < count($twentythree); $i++)
    		    					@if((count($twentythree[$i]) > 0) && ($twentythree[$i][0]['expenses_type'] == 1) && ($twentythree[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentythree[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($twentyfour) > 0)
    		    				@for($i = 0; $i < count($twentyfour); $i++)
    		    					@if((count($twentyfour[$i]) > 0) && (twentyfourw[$i][0]['expenses_type'] == 1) && ($tw[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentyfour[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($twentyfive) > 0)
    		    				@for($i = 0; $i < count($twentyfive); $i++)
    		    					@if((count($twentyfive[$i]) > 0) && ($twentyfive[$i][0]['expenses_type'] == 1) && ($twentyfive[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentyfive[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($twentysix) > 0)
    		    				@for($i = 0; $i < count($twentysix); $i++)
    		    					@if((count($twentysix[$i]) > 0) && ($twentysix[$i][0]['expenses_type'] == 1) && ($twentysix[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentysix[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($twentyseven) > 0)
    		    				@for($i = 0; $i < count($twentyseven); $i++)
    		    					@if((count($twentyseven[$i]) > 0) && ($twentyseven[$i][0]['expenses_type'] == 1) && ($twentyseven[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentyseven[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($twentyeight) > 0)
    		    				@for($i = 0; $i < count($twentyeight); $i++)
    		    					@if((count($twentyeight[$i]) > 0) && ($twentyeight[$i][0]['expenses_type'] == 1) && ($twentyeight[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentyeight[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($twentynine) > 0)
    		    				@for($i = 0; $i < count($twentynine); $i++)
    		    					@if((count($twentynine[$i]) > 0) && ($twentynine[$i][0]['expenses_type'] == 1) && ($twentynine[$i][0]['employee_id'] == $employee->id))
    		    						{{ $twentynine[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($thirty) > 0)
    		    				@for($i = 0; $i < count($thirty); $i++)
    		    					@if((count($thirty[$i]) > 0) && ($thirty[$i][0]['expenses_type'] == 1) && ($thirty[$i][0]['employee_id'] == $employee->id))
    		    						{{ $thirty[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    			<td style="background-color: #008000;">
    		    				@if(count($thirtyone) > 0)
    		    				@for($i = 0; $i < count($thirtyone); $i++)
    		    					@if((count($thirtyone[$i]) > 0) && ($thirtyone[$i][0]['expenses_type'] == 1) && ($thirtyone[$i][0]['employee_id'] == $employee->id))
    		    						{{ $thirtyone[$i][0]['expenses_amount'] }}
    		    					@endif
    		    				@endfor
    		    				@endif
    		    			</td>
    		    		</tr>
    	    		@endforeach

                    @foreach($company_expenses_prev_month as $offset => $each_val)
                    <tr>
                        <td style="background-color: #C0C0C0;">{{ $each_val->id }}</td>
                        <td style="background-color: #C0C0C0;">{{ $each_val->name }}</td>
                        <td style="background-color: #C0C0C0;">Company Expenses</td>
                        <td style="background-color: #99CCFF;">{{ $each_val->amount + $claim[$offset] }}</td>
                        @foreach($data as $key => $val)
                        <td style="background-color: #99CCFF;"></td>
                        @endforeach

                        <td style="background-color: #FFFF99;">{{ $each_val->amount + $claim[$offset] }}</td>
                        <td style="background-color: #FFCC99;">{{ $each_val->amount }}</td>
                        <td style="background-color: #FFFF99;">{{ $claim[$offset] }}</td>
                        
                        @foreach($data as $key => $val)
                        <td style="background-color: #008000;">
                            @if(count($data[$key]) > 0)
                                @for($i = 0; $i < count($data[$key]); $i++)
                                    @if(count($data[$key][$i]) > 0 && $data[$key][$i][0]['company_expenses_id'] == $each_val->id)
                                        {{ $data[$key][$i][0]['amount'] }}
                                    @endif
                                @endfor
                            @endif
                        </td>
                        @endforeach    
                        
                    </tr>
                    @endforeach

                    @foreach($company_expenses_current_month as $offset => $each_val)
                    <tr>
                        <td style="background-color: #C0C0C0;">{{ $each_val->id }}</td>
                        <td style="background-color: #C0C0C0;">{{ $each_val->name }}</td>
                        <td style="background-color: #C0C0C0;">Company Expenses</td>
                        <td style="background-color: #99CCFF;">0</td>

                        @foreach($current_claim_data as $key => $val)
                        <td style="background-color: #99CCFF;">
                            @if(count($current_claim_data[$key]) > 0)
                                @for($i = 0; $i < count($current_claim_data[$key]); $i++)
                                    @if(count($current_claim_data[$key][$i]) > 0 && $current_claim_data[$key][$i][0]['id'] == $each_val->id)
                                        {{ $current_claim_data[$key][$i][0]['amount'] + $current_claim[$offset] }}
                                    @endif
                                @endfor
                            @endif
                        </td>
                        @endforeach

                        <td style="background-color: #FFFF99;">{{ $each_val->amount + $current_claim[$offset] }}</td>
                        <td style="background-color: #FFCC99;">{{ $each_val->amount }}</td>
                        <td style="background-color: #FFFF99;">{{ $current_claim[$offset] }}</td>
                        
                        @foreach($current_data as $key => $val)
                        <td style="background-color: #008000;">
                            @if(count($current_data[$key]) > 0)
                                @for($i = 0; $i < count($current_data[$key]); $i++)
                                    @if(count($current_data[$key][$i]) > 0 && $current_data[$key][$i][0]['company_expenses_id'] == $each_val->id)
                                        {{ $current_data[$key][$i][0]['amount'] }}
                                    @endif
                                @endfor
                            @endif
                        </td>
                        @endforeach    
                        
                    </tr>
                    @endforeach



    		
    	</tbody>
	</table>
</div>



@else
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-5">
			<h2 style="color: red;">No Data Found!</h2>
			<h3 style="color: red;">Please Select Month and Year Properly!</h3>
			<a class="btn btn-info" href="{{ url('retail-market') }}" style="text-align: center;">Search Again</a>
		</div>
		<div class="col-md-3"></div>
	</div>
@endif



                

@endsection