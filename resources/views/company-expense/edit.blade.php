@extends('layouts.master')
@section('page_main_content')

<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box-header with-border">
						<legend class="box-title" style="text-align: center; font-weight: bold;">Edit Expense</legend>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" enctype="multipart/form-data" action="{{ route('company-expense.update',['expense' => $expense->id]) }}">
				@method('PUT')
				@csrf
				<div class="box-body">

					<!-- Name -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name">Expense Name :</label>
								<input type="text" name="name" value="{{ $expense->name }}" class="form-control" placeholder="Enter Details" required="">
								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					<!-- Amount -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('amount') ? ' has-error' : '' }}">
								<label for="amount">Expense Amount :</label>
								<input type="text" name="amount" value="{{ $expense->amount }}" class="form-control" placeholder="Enter Name" required="">
								@if ($errors->has('amount'))
								<span class="help-block">
									<strong>{{ $errors->first('amount') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>
					
					
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary col-sm-12">Update</button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
@endsection