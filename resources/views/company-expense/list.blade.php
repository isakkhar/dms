@extends('layouts.master')
@section('page_main_content')
<div class="box">
    <div class="box-header">
        <h1 class="box-title">Company Expenses List from {{$from}} to {{$to}}</h1>
    </div>

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif

<tada></tada>


<section class="content">
    <div class="row">
            <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Claim Date</th>
                            <th>Name</th>
                            <th>Claim Amount</th>
                            <th>Claim Pass</th>
                            <th>Net Claim Amount</th>
                            <th>Action</th>

                        </tr>
                    </thead>
                            
                    <tbody>
                        @php 
                            $claim_pass = 0;  
                            $claim_amount = 0;  
                            $net_claim = 0;  
                        @endphp 
                        @foreach($companyExpenses as $expense)
                            @php 
                                $claim_pass += $expense->claim_pass; 
                                $claim_amount += $expense->amount + $expense->claim_pass; 
                                $net_claim += $expense->amount; 
                            @endphp 

                            <tr>

                                <td>{{ $expense->id }}</td>
                                <td>{{ $expense->created_at->format('m/d/Y') }}</td>
                                <td>{{ $expense->name }}</td>
                                <td>{{ $expense->amount + $expense->claim_pass }}</td>
                                <td>{{ $expense->claim_pass }}</td>
                                <td>{{ $expense->amount }}</td>
                                <td>
                                    <a href="{{ route('company-expense.edit',['expense' => $expense->id]) }}" class="btn btn-warning">
                                        <i class="fa fa-edit"></i> 
                                    </a>
                                    
                                    <a href="javascript;" class="btn btn-danger" @click.prevent="deleteExpenseOffice({{ $expense->id }})">
                                        <i class="fa fa-trash"></i> 
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                            <tr style="background: #222d32; color: #ddd; font-weight: bold;">
                               <td>Total</td>
                               <td></td>
                               <td></td>
                               <td>{{ $claim_amount }}</td>
                               <td>{{ $claim_pass }}</td>
                               <td>{{ $net_claim }}</td>
                               <td></td>
                            </tr>
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
        </div>
</section>

</div>
@endsection