@extends('layouts.master')
@section('page_main_content')
<script src="https://code.jquery.com/jquery-1.12.4.js" 
integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU=" crossorigin="anonymous"></script>

<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box-header with-border">
						<legend class="box-title" style="text-align: center; font-weight: bold;">Add New Company Expenses</legend>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" enctype="multipart/form-data" action="{{ route('company-expense.store') }}">
				@csrf
				<div class="box-body">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12">
								<label for="name">Expense Type :</label>
								<select id="expenses_type" name="expenses_type" class="form-control">
									<option value="">Select</option>
									<option value="claim">Claim</option>
									<option value="claim_pass">Claim Pass</option>
								</select>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>

					<div class="row" id="claim" style="display: none;">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name">Expense Name :</label>
								<input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Enter Details">
								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>

					<div class="row" id="claim_pass" style="display: none;">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12">
								<label for="name">Expenses Name :</label>
								<select name="claim_pass" class="form-control">
									<option value="">Select</option>
									@foreach($company_expenses as $each)
									<option value="{{ $each->id }}">{{ $each->id }}. {{ $each->name }}</option>}
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>

					<!-- Amount -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('amount') ? ' has-error' : '' }}">
								<label for="amount">Amount :</label>
								<input type="text" name="amount" value="{{ old('amount') }}" class="form-control" placeholder="Enter Name" required="">
								@if ($errors->has('amount'))
								<span class="help-block">
									<strong>{{ $errors->first('amount') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>
					
					
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary col-sm-12">Submit</button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function(){
	    $('#expenses_type').on('change', function() {
	      if ( this.value == 'claim')
	      {
	        $("#claim").show();
	        $("#claim_pass").hide();
	      }
	      else if( this.value == 'claim_pass')
	      {
	        $("#claim_pass").show();
	        $("#claim").hide();
	      }
	      else {
	      	$("#claim").hide();
	      	$("#claim_pass").hide();
	      }
	    });
	});
</script>
@endsection

