@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">All Company Expenses</h3>
        <a href="{{ route('company-expense.create') }}" class="text-right btn btn-success"><i class="fa fa-users fa-fw"></i> Add Company Expenses</a>
    </div>

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif

<section class="content">
  <div class="row">
            <form method="POST" action="{{ route('com_ex') }}">
                @csrf
                
                <div class="row">
                    <div class="col-sm-2"> From </div>
                    <div class="col-sm-4">
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" class="form-control" name="from_date">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-2"> To </div>
                    <div class="col-sm-4">
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" class="form-control" name="to_date">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                    </div>
                </div>
                <br><br>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <button class="btn btn-info " type="submit">Generate Company Expense</button>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </form>
        </div>
  <!-- /.row -->
</section>

@endsection