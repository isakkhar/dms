@extends('layouts.master')
@section('page_main_content')
<div class="box">
    <div class="box-header">
        <h1 class="box-title pull-left">Salary List of the month </h1>
        <a href="{{ url('salary-export') }}" class="btn btn-success pull-right">Download to Excel</a>
    </div>

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif

<tada></tada>


<section class="content">
	<div class="row">
			<div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example5" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Employee Name</th>
                            <th>Designation</th>
                            <th>Off Day Duty</th>
                            <th>Off Day</th>
                            <th>Present</th>
                            <th>Total Present</th>
                            <th>Leave</th>
                            <th>Absent</th>
                            <th>Basic Salary</th>
                            <th>Ho</th>
                            <th>Total Absent(TK)</th>
                            <th>Provident Fund</th>
                            
                            <th>Cash Short </th>
                            <th>Net Payable </th>
                        </tr>
                    </thead>
                            
                    <tbody>
                        @foreach($employees as $employee)
                        <tr>
                            <td>{{ $employee->id }}</td>
                            <td>{{ $employee->name }}</td>
                            <td>{{ $employee->designation_name->name }}</td>
                            <td>{{ $employee->off_day_work['count'] }}</td>
                            <td>{{ $employee->off_day['count'] }}</td>
                            <td>{{ date('d') - $employee->off_day_work['count'] -$employee->off_day['count'] - $employee->absent['count'] }}</td>
                            <td>{{ date('d') + $employee->off_day_work['count'] -$employee->off_day['count'] - $employee->absent['count'] }}</td>
                            <td>{{ $employee->leave['count'] }}</td>
                            <td>{{ $employee->absent['count'] }}</td>
                            <td>{{ $employee->salary }}</td>
                            <td>{{ round($employee->salary / 30) * $employee->off_day['count'] }}</td>
                            <td>{{ round($employee->salary / 30) * $employee->absent['count'] }}</td>
                            <td>{{ $employee->provident_fund }}</td>
                            <td>{{ $employee->cash_flow->cash_short }}</td>
                            
                            <td> {{ round((date('d') - $employee->off_day_work['count'] -$employee->off_day['count']) * ($employee->salary / 30)
                            + 
                            (round($employee->salary / 30) * $employee->off_day['count'] )
                            - 
                            (round($employee->salary / 30) * $employee->absent['count'])
                            -
                            $employee->provident_fund - $employee->cash_flow->cash_short) }}</td>
                        </tr>
                        @endforeach
                        
                    </tbody>
                            
                </table>
            </div>
		</div>
</section>

</div>
@endsection