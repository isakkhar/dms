@extends('layouts.master')
@section('page_main_content')
@if(session('err'))
<div class="alert alert-danger alert-dismissible notify">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	<h4><i class="icon fa fa-ban"></i>Error Alert!</h4>
	{{ session('err') }}
</div>
@endif
<div class="row">
	<div class="col-md-3">
		<div class="box box-primary">
			<div class="box-body box-profile">
				@if ($employee->photo == NULL)
				<img class="profile-user-img img-responsive img-circle" src="{{ asset('img/profile.png') }}" alt="User profile picture">
				@else
				<img class="profile-user-img img-responsive img-circle" src="{{ asset('storage/'.str_replace('public/', '', $employee->photo)) }}" alt="User profile picture">

				@endif
				<h3 class="profile-username text-center">{{ $employee->name }}</h3>
				<p class="text-muted text-center">{{ $employee->designation }} at {{ $employee->department }}</p>
				<ul class="list-group list-group-unbordered">
					<li class="list-group-item">
						<b>Phone</b> <a class="pull-right">{{ $employee->phone }}</a>
					</li>
					<li class="list-group-item">
						<b>Email</b> <a class="pull-right">{{ $employee->email  }}</a>
					</li>
					<li class="list-group-item">
						<b>Salary</b> <a class="pull-right">{{ number_format($employee->salary) . ' BDT' }}</a>
					</li>
					<li class="list-group-item">
						<b>Joining Salary</b> <a class="pull-right">{{ number_format($employee->joining_salary) . ' BDT'}}</a>
					</li>
					<li class="list-group-item">
						<b>Joining Date</b> <a class="pull-right">{{ date('F j, Y', strtotime($employee->join_date)) }}</a>
					</li>
				</ul>
				<a href="#" class="btn btn-primary btn-block"><b>Contact </b></a>
			</div>
		</div>
	</div>
	<div class="col-md-9">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#activity" data-toggle="tab" aria-expanded="true">Basic Information</a></li>
				<li class=""><a href="#timeline" data-toggle="tab" aria-expanded="false">Timeline</a></li>
				<li class=""><a href="#settings" data-toggle="tab" aria-expanded="false">Settings</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="activity">
					<div class="box">
						
						<!-- /.box-header -->
						<div class="box-body">
							<table class="table table-striped" style="white-space: nowrap;">
								<tbody>
								<tr>
									<th>Gender:</th>
									<td>{{ $employee->gender == 'm' ? 'Male' : 'Female' }}</td>
								</tr>
								<tr>
									<th>Date of Birth:</th>
									<td>{{date('F j, Y', strtotime($employee->DOB)) }}</td>
								</tr>
								<tr>
									<th>Present Adrress:</th>
									<td>{{ $employee->present_addr }}</td>
								</tr>
								<tr>
									<th>Permanent Adrress:</th>
									<td>{{ $employee->permanent_addr }}</td>
								</tr>
								<tr>
									<th>Father Name:</th>
									<td>{{ $employee->father_name ?? 'Not available'  }}</td>
								</tr>

							</tbody>
						</table>
						</div>
						<!-- /.box-body -->
					</div>
				</div>
				<!-- /.tab-pane -->
				<div class="tab-pane" id="timeline">
					<!-- The timeline -->
					<ul class="timeline timeline-inverse">
						<!-- timeline time label -->
						<li class="time-label">
							<span class="bg-red">
								10 Feb. 2014
							</span>
						</li>
						<!-- /.timeline-label -->
						<!-- timeline item -->
						<li>
							<i class="fa fa-envelope bg-blue"></i>
							<div class="timeline-item">
								<span class="time"><i class="fa fa-clock-o"></i> 12:05</span>
								<h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>
								<div class="timeline-body">
									Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
									weebly ning heekya handango imeem plugg dopplr jibjab, movity
									jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
									quora plaxo ideeli hulu weebly balihoo...
								</div>
								<div class="timeline-footer">
									<a class="btn btn-primary btn-xs">Read more</a>
									<a class="btn btn-danger btn-xs">Delete</a>
								</div>
							</div>
						</li>
						<!-- END timeline item -->
						
					</ul>
				</div>
				<!-- /.tab-pane -->
				<div class="tab-pane" id="settings">
					Extra tab for more
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- /.nav-tabs-custom -->
	</div>
</div>
@endsection