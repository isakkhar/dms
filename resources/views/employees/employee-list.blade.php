@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Employees</h3>
        <a href="{{ route('employee.create') }}" class="text-right btn btn-success"><i class="fa fa-users fa-fw"></i> Add Employee</a>
        <a href="{{ url('employee-export') }}" class="btn btn-success pull-right" style="margin-right: 15px">Download to Excel</a>
    </div>

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Department</th>
                            <th>Designation</th>
                            <th>Joining Date</th>
                            <th>TADA</th>
                            <th>Provident Fund</th>
                            <th>Current Salary</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                            
                    <tbody>
                        @foreach($employees as $employee)
                        <tr>
                            <td>{{ $employee->id }}</td>
                            <td>{{ $employee->name }}</td>
                            <td>{{ $employee->email }}</td>
                            <td>{{ $employee->phone }}</td>
                            <td>{{ $employee->department }}</td>
                            <td>{{ $employee->designation_name->name }}</td>
                            <td>{{ $employee->join_date }}</td>
                            <td>{{ $employee->tada }}</td>
                            <td>{{ $employee->provident_fund }}</td>
                            <td>{{ $employee->salary }}</td>
                            <td>
                                <a href="{{ route('employee.show', ['employee' => $employee->id ]) }}" class="btn btn-info">
                                    <i class="fa fa-user"></i> 
                                </a>
                                <a href="{{ route('employee.edit',['employee' => $employee->id]) }}" class="btn btn-warning">
                                    <i class="fa fa-edit"></i> 
                                </a>
                                
                                <a href="javascript;" class="btn btn-danger" @click.prevent="deleteEmployee({{ $employee->id }})">
                                    <i class="fa fa-trash"></i> 
                                </a>
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
</div>
@endsection