@extends('layouts.master')
@section('page_main_content')

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon fa fa-check"></i>{{ session('msg') }}
            
        </div>
@endif
<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Edit a attendance list</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" action="{{ url('attendance-update') }}">
				@csrf
				<div class="box-body">
					<div class="col-sm-4">
                        <div class="input-group date" data-provide="datepicker">
                            <input type="text" class="form-control" name="date">
                            <div class="input-group-addon">
                                <span class="glyphicon glyphicon-th"></span>
                            </div>
                        </div>
                    </div>

					<div class="form-group col-sm-6">
						<div class="box-footer">
					<button type="submit" class="btn btn-primary" style="margin-top: 14px;">Submit</button>
				</div>
					</div>
				</div>
				<!-- /.box-body -->
				
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
@endsection