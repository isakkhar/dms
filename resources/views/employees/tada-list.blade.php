@extends('layouts.master')
@section('page_main_content')
<div class="box">
    <div class="box-header">
        <h1 class="box-title">TA-DA List ({{ date("d-m-Y", strtotime($from)) . " to " . date("d-m-Y", strtotime($to)) }})</h1>
    </div>

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif



<section class="content">
    <a href="{{ route('tada-export') }}" class="btn btn-success pull-right" style="margin-top: -39px;display: block;padding: 15px;">Download to Excel</a>
	<div class="row">
			<div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example5" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Employee Name</th>
                            <th>Designation</th>
                            <th>Total Present</th>
                            <th>Ta/Da </th>
                            <th>Gross Ta/Da </th>
                            <th>Cash Short </th>
                            <th>Net TADA </th>
                            <th>Pay TADA </th>
                        </tr>
                    </thead>
                            
                    <tbody>
                        @foreach($employees as $employee)
                        @php
                        $cut =0;
                        if(strtotime($employee->join_date) >= strtotime($from) && strtotime($employee->join_date) <= strtotime($to)){
                            if (date('j', strtotime($from)) ==16) {
                                $cut= (int) date('j', strtotime($employee->join_date)) -15;
                            } else{
                                $cut= (int) date('j', strtotime($employee->join_date));
                            }
                        }else {
                            $cut =0;
                        }
                        @endphp
                        <tr>
                            <td>{{ $employee->id }}</td>
                            <td>{{ $employee->name }}</td>
                            <td>{{ $employee->designation_name->name }}</td>
                            <td>{{ $work = $working_period - $employee->off_day['count'] + $employee->off_day_work['count'] - $employee->leave_day['count'] - $employee->absent['count'] -$cut }}</td>
                            <td>
                                {{ $employee->tada }}
                            </td>
                            <td>
                                {{ $gross = $work * $employee->tada }}
                            </td>
                            <td>{{ $employee->cash_flow->cash_short }}</td>
                            @if($employee->paid_tada)
                            <td>{{ $employee->paid_tada->tada}}</td>
                            @else
                            <td>
                                @php 
                                $net = $gross - $employee->cash_flow->cash_short  
                                @endphp
                                {{ $net < 0  ?  $net." (Cash Short)" : $net}}
                                </td>
                            @endif
                            <td>
                                @if(!$employee->paid_tada)
                                    <button type="submit" data-id="{{ $employee->id }}" data-start="{{ $from }}" data-amount="{{ $net }}" class="btn btn-info subTada {{ $employee->id }}">Pay Tada</button>
                                @else
                                <div class="btn btn-success btn-xs">Paid</div>
                                @endif
                                </td>
                        </tr>
                        @endforeach
                        {{-- @foreach($all_employees as $employee)
                        @if($employee->count_absent == null)
							<tr>
                            <td>{{ $employee->id }}</td>
                            <td>{{ $employee->name }}</td>
                            <td>{{ $employee->designation }}</td>
                            <td>{{ $work = $working_period - $employee->all_off_day['count'] + $employee->off_day_duty['count'] - $employee->leave_day['count'] }}</td>
                            <td>
                                {{ $employee->tada }}
                            </td>
                            <td>
                                {{ $gross = $work * $employee->tada }}
                            </td>
                            <td>{{ $employee->cash_flow->cash_short }}</td>
                            <td>{{ $employee->cash_flow->cash_excess }}</td>
                            <td>{{ $gross - $employee->cash_flow->cash_short + $employee->cash_flow->cash_excess  }}</td>
                            <td><a href="" class="btn btn-info ">Pay Tada</a></td>

                        </tr>
                        @endif
                        @endforeach --}}
                    </tbody>
                            
                </table>
            </div>
		</div>
</section>

</div>
@endsection
@section('scripts')
<script type="text/javascript" src="js/axios.min.js"></script>
<script type="text/javascript">
    $('.subTada').click(function(e){
        e.preventDefault();
        var id = $(this).data('id');
        var start = $(this).data('start');
        var amount = $(this).data('amount');
        axios.post('/pay-tada',{employee_id: id,start_date:start,tada:amount})
        .then(({data}) => {
            $('.'+id).removeClass('btn-info').addClass('btn-xs').addClass('btn-success').text('Paid').attr('disabled', 'disabled');;
        });
    });
</script>
@endsection