@extends('layouts.master')
@section('style')
<style type="text/css">

</style>
@endsection
@section('page_main_content')
<div class="box">
    {{-- <div class="box-header">
        <h1 class="box-title">TA-DA</h1>
    </div> --}}

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif

<tada></tada>


<section class="content">
		<div class="row">
			<h3>Generate Tada</h3>

			@php 
			ksort($months)
			@endphp
			<form method="POST" action="{{ route('check-tada-post') }}">
				@csrf
				
				<div class="col-md-12">
					<div class="form-group">
						<div class="form-group col-sm-4 {{ $errors->has('from') ? ' has-error' : '' }}">
							<label class="control-label">Month: </label>
							<div class="input-group col-md-12">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
									<select name="month">
										<option value=""> Pick a Month</option>
										@foreach($months as $key => $month)
										<option value="{{ $key }}">{{ $month }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="form-group col-sm-4 {{ $errors->has('from') ? ' has-error' : '' }}">
							<label class="control-label">Date: </label>
							<div class="input-group col-md-12">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
									<select name="date">
										<option value="">Pick a date</option>
										<option value="first">First 15 days</option>
										<option value="second">Second 15 days</option>
									</select>
								</div>
							</div>
					</div>
					<div class="form-group col-sm-4 {{ $errors->has('from') ? ' has-error' : '' }}">
							<label class="control-label">Year: </label>
							<div class="input-group col-md-12">
								<div class="input-group-addon">
									<i class="fa fa-calendar"></i>
									<input type="text" name="year" placeholder="Enter a year">
								</div>
							</div>
					</div>


					</div>
				</div>
				
				<div class="col-md-4 clearfix">
					<button class="btn btn-info " type="submit">Generate Tada</button>
				</div>
			</form>
			
		</div>
		
		</section>

</div>
@endsection
@section('style')
<style type="text/css">
	.hide{display: none;}
</style>
@endsection