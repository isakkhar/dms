@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h1 class="box-title pull-left">Attendance List of this month</h1>
        <a href="{{ url('attendance-export') }}" class="btn btn-success pull-right">Download to Excel</a>
    </div>

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Designation</th>
                            <th>Off Day duty</th>
                            <th>Off Day </th>
                            <th>Present</th>
                            <th>Total Present</th>
                            <th>Leave</th>
                            <th>Absent Days</th>
                        </tr>
                    </thead>
                            
                    <tbody>
                        @foreach($employees as $employee)
                        @php
                        $p = (date('d') - $employee->off_day['count'] - $employee->leave['count'] - $employee->absent['count'] + $employee->off_day_work['count'] > date('d')) ? date('d') :date('d') - $employee->off_day['count'] - $employee->leave['count'] - $employee->absent['count'] + $employee->off_day_work['count'];
                        $t_p = (date('d') + $employee->off_day['count'] + $employee->leave['count'] - $employee->absent['count'] + $employee->off_day_work['count'] > date('d')) ? date('d') :date('d') + $employee->off_day['count'] + $employee->leave['count'] - $employee->absent['count'] + $employee->off_day_work['count'];
                        @endphp
                        <tr>
                            <td>{{ $employee->id }}</td>
                            <td>{{ $employee->name }}</td>
                            <td>{{ $employee->designation_name->name }}</td>
                            <td>{{ $employee->off_day_work['count'] }}</td>
                            <td>{{ $employee->off_day['count'] }}</td>
                            <td>{{ $p }}</td>
                            <td>{{ $t_p }}</td>
                            <td>{{ $employee->leave_day['count'] }}</td>
                            <td>{{ $employee->absent['count'] }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
</div>
@endsection