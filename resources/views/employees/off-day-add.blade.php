@extends('layouts.master')
@section('page_main_content')
<div class="box">
	<div class="box-header">
		<h1 class="box-title">Off List</h1>
	</div>
	@if(session('msg'))
	<div class="alert alert-success alert-dismissible notify">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-check"></i>Success Alert!</h4>
		{{ session('msg') }}
	</div>
	@endif
	@if(session('err'))
	<div class="alert alert-danger alert-dismissible notify">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<h4><i class="icon fa fa-ban"></i>Error Alert!</h4>
		{{ session('err') }}
		<ul>
			@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
	@endif
	<section class="content">
		<div class="row">
			<form method="POST" action="{{ route('off-day.store') }}">
				@csrf


				<div class="col-md-6">
					<div class="form-group">
						<select name="employee_id" class="form-control" required="">
							<option value="">Select an Employee</option>
							@foreach($employees as $employee)
							<option value="{{ $employee->id }}">{{ $employee->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-group col-md-12">
						<select name="off_day" class="form-control" required>
							<option value="">Select a day</option>
							<option value="Saturday">Saturday</option>
							<option value="Sunday">Sunday</option>
							<option value="Monday">Monday</option>
							<option value="Tuesday">Tuesday</option>
							<option value="Wednessday">Wednessday</option>
							<option value="Thursday">Thursday</option>
							<option value="Friday">Friday</option>
						</select>
					</div>
				</div>
				<div class="col-md-4 clearfix">
					<button class="btn btn-info " type="submit">Add Off Day</button>
				</div>
			</form>
		</div>
		<div class="row o-list" style="padding: 30px 0">
    <div class="col-xs-12">
      <div class="box">
      	<div class="box-header">
		    <h3 class="box-title">Off day list</h3> 
		</div>
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example3" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Employee Name</th>
                            <th>Off days</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                            
                    <tbody>
                    	{{-- {{ dd($off_day) }} --}}
                        @foreach($off_day as $day)
                        <tr>
                            <td>{{ $day->employee->id }}</td>
                            <td>{{ $day->employee->name }}</td>
                            <td>{{ $day->off_date }}</td>
                            <td>
                                <a href="javascript;" class="btn btn-danger" @click.prevent="deleteOff({{ $day->id }})"><i class="fa fa-trash"></i>
                                </a>
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
	</div>
	@endsection