@extends('layouts.master')
@section('page_main_content')

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon fa fa-check"></i>{{ session('msg') }}
            
        </div>
@endif
<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Add New Designation</h3>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" action="{{ route('create-designation') }}">
				@csrf
				<div class="box-body">
					<div class="form-group col-sm-6 {{ $errors->has('name') ? ' has-error' : '' }}">
						<label for="name">Name</label>
						<input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Enter Name" required>
						@if ($errors->has('name'))
						<span class="help-block">
							<strong>{{ $errors->first('name') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group col-sm-6">
						<div class="box-footer">
					<button type="submit" class="btn btn-primary" style="margin-top: 14px;">Submit</button>
				</div>
					</div>
				</div>
				<!-- /.box-body -->
				
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
@endsection