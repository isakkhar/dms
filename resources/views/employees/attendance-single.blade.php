@extends('layouts.master')
@section('page_main_content')

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <i class="icon fa fa-check"></i>{{ session('msg') }}
            
        </div>
@endif
<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">Update Attendance list {{ $date }}</h3>
			</div>
			<updateattendance :date="{{ json_encode($date) }}" :em="{{ json_encode($employees) }}"></updateattendance>

		</div>
		<!-- /.box -->
	</div>
</div>
@endsection