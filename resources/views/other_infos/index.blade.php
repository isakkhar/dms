@extends('layouts.master')
@section('page_main_content')

@if(session('success'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('success') }}
        </div>
@endif

<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box-header with-border">
						<legend class="box-title" style="text-align: center; font-weight: bold;">Add Other Infos</legend>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" enctype="multipart/form-data" action="{{ url('store-other-info') }}">
				@csrf
				<div class="box-body">
					<!-- Name -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name">Info Name :</label>
								<select name="name" class="form-control">
									<option value="">Select</option>
									<option value='["investment_in_stock", 1]'>1. Investment in Stock</option>
									<option value='["credit_to_trade", 2]'>2. Credit to Trade</option>
									<option value='["loading_unloading", 3]'>3. Loading & Unloading</option>
									<option value='["head_load_delivery", 4]'>4. Head Load Delivery</option>
									<option value='["landline_telephone_bills", 5]'>5. Landline telephone Bills</option>
									<option value='["mobile_phone_bills", 6]'>6. Mobile Phone Bills</option>
									<option value='["wasa_bills", 7]'>7. WASA Bills</option>
									<option value='["electricity_bills", 8]'>8. Electricity Bills</option>
									<option value='["gas_bills", 9]'>9. Gas Bills</option>
									<option value='["internet_bills_for_office", 10]'>10. Internet Bills for office usage</option>
									<option value='["internet_bills_for_hht", 11]'>11. Internet Bills for HHT</option>
								</select>
								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					<!-- Amount -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('amount') ? ' has-error' : '' }}">
								<label for="amount">Amount :</label>
								<input type="number" name="amount" value="{{ old('amount') }}" class="form-control" placeholder="Enter Life in amount" required="">
								@if ($errors->has('amount'))
								<span class="help-block">
									<strong>{{ $errors->first('amount') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary col-sm-12">Submit</button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
@endsection