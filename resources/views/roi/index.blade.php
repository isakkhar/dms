@extends('layouts.master')
@section('page_main_content')

<div class="row">
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="box-header with-border">
                        <legend class="box-title" style="text-align: center; font-weight: bold;">Generate ROI</legend>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" enctype="multipart/form-data" action="{{ url('roi-details') }}">
                @csrf
                <div class="box-body">

                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="form-group col-sm-12 {{ $errors->has('month') ? ' has-error' : '' }}">
                            <label for="month">Select Month :</label>
                            <select name="month" class="form-control" required="">
                                <option value="">Select</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>

                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="form-group col-sm-12 {{ $errors->has('year') ? ' has-error' : '' }}">
                            <label for="year">Year :</label>
                            <input type="number" name="year" class="form-control" required="">
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                    
                    
                </div>
                <!-- /.box-body -->
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <button class="btn btn-success" style="width: 100%;">SEARCH</button>
                    </div>
                    <div class="col-md-3"></div>
                </div>
            </form>
        </div>
        <!-- /.box -->
    </div>
</div>
@endsection