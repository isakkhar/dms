@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Investments</h3>
    </div>
</div>

<div class="row">
    <div class="col-md-8"></div>
    <div class="col-md-2">
        <a href="{{ url('/add-investment-details') }}" role="button" class="btn btn-info">Add Investment Details</a>
    </div>

    <div class="col-md-2">
        <a href="{{ url('/add-investment') }}" role="button" class="btn btn-info">Add Investment</a>
    </div>
</div>

<section class="content">
    <!-- MV -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-header">
                    <legend>Book value of Mechanized Vehicle</legend>
                </div>
                
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover" style="text-align: center;">
                        <thead>
                            <tr>
                                <th style="text-align: center; background: #222d32; color: #fff;">SL.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Types of MV</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Pur. Date</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">No. of Unit</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Unit Price</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Total</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Life(Years)</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Life(Months)</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Months in Use</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Monthly Dep.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Acc. Dep.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Balance Remain</th>
                            </tr>
                        </thead>  
                        <tbody>
                            @php 
                                $count = 1; $depreciation = $monthly_depcreciation = $total_cost = $remain = 0; 
                            @endphp
                            @foreach($MV as $data)
                                <tr>
                                    <td>{{ $count }}</td>
                                    <td>{{ $data->particuler }}</td>
                                    <td>{{ $data->puchase_date }}</td>
                                    <td>{{ $data->no_of_unit }}</td>
                                    <td>{{ $data->unit_price }}</td>
                                    <td>{{ $data->total_cost }}</td>
                                    <td>{{ $data->life_in_year }}</td>
                                    <td>{{ $data->life_in_month }}</td>
                                    <td>{{ $data->month_in_use }}</td>
                                    <td>{{ $data->monthly_depcreciation }}</td>
                                    <td>{{ $data->depreciation }}</td>
                                    <td>{{ $data->balance_remain }}</td>
                                </tr>
                                @php 
                                    $count++; 
                                    $depreciation += $data->depreciation;
                                    $monthly_depcreciation += $data->monthly_depcreciation;
                                    $total_cost += $data->total_cost; 
                                    $remain += $data->balance_remain;
                                @endphp
                            @endforeach
                        </tbody>     
                    </table>
                </div>
                <!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    
    <!-- NMV -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-header">
                    <legend>Book value of NonMechanized Vehicle</legend>
                </div>
                
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover" style="text-align: center;">
                        <thead>
                            <tr>
                                <th style="text-align: center; background: #222d32; color: #fff;">SL.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Types of NMV</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Pur. Date</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">No. of Unit</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Unit Price</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Total</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Life(Years)</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Life(Months)</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Months in Use</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Monthly Dep.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Acc. Dep.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Balance Remain</th>
                            </tr>
                        </thead>  
                        <tbody>
                            @php 
                                $count2 = 1; @endphp
                            @foreach($NMV as $data)
                                <tr>
                                    <td>{{ $count2 }}</td>
                                    <td>{{ $data->particuler }}</td>
                                    <td>{{ $data->puchase_date }}</td>
                                    <td>{{ $data->no_of_unit }}</td>
                                    <td>{{ $data->unit_price }}</td>
                                    <td>{{ $data->total_cost }}</td>
                                    <td>{{ $data->life_in_year }}</td>
                                    <td>{{ $data->life_in_month }}</td>
                                    <td>{{ $data->month_in_use }}</td>
                                    <td>{{ $data->monthly_depcreciation }}</td>
                                    <td>{{ $data->depreciation }}</td>
                                    <td>{{ $data->balance_remain }}</td>
                                </tr>
                                @php 
                                    $count2++; 
                                    $depreciation += $data->depreciation;
                                    $monthly_depcreciation += $data->monthly_depcreciation;
                                    $total_cost += $data->total_cost;
                                    $remain += $data->balance_remain;
                                @endphp
                            @endforeach
                        </tbody> 
                        <tfoot style="font-weight: bold; color: #fff; background: #0098bd;">
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Total(MV+NMV)</td>
                                <td></td>
                                <td></td>
                                <td>{{ $total_cost }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $monthly_depcreciation }}</td>
                                <td>{{ $depreciation }}</td>
                                <td>{{ $remain }}</td>
                            </tr>
                        </tfoot>    
                    </table>
                </div>
                <!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    
    <!-- Computer Equipment -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-header">
                    <legend>Book value of Computer Equipment</legend>
                </div>
                
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover" style="text-align: center;">
                        <thead>
                            <tr>
                                <th style="text-align: center; background: #222d32; color: #fff;">SL.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Particuler</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Pur. Date</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">No. of Unit</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Unit Price</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Total</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Life(Years)</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Life(Months)</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Months in Use</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Monthly Dep.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Acc. Dep.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Balance Remain</th>
                            </tr>
                        </thead>  
                        <tbody>
                            @php 
                                $count3 = 1; $depreciation3 = $monthly_depcreciation3 = $total_cost3 = $remain3 = 0; 
                            @endphp
                            @foreach($CE as $data)
                                <tr>
                                    <td>{{ $count3 }}</td>
                                    <td>{{ $data->particuler }}</td>
                                    <td>{{ $data->puchase_date }}</td>
                                    <td>{{ $data->no_of_unit }}</td>
                                    <td>{{ $data->unit_price }}</td>
                                    <td>{{ $data->total_cost }}</td>
                                    <td>{{ $data->life_in_year }}</td>
                                    <td>{{ $data->life_in_month }}</td>
                                    <td>{{ $data->month_in_use }}</td>
                                    <td>{{ $data->monthly_depcreciation }}</td>
                                    <td>{{ $data->depreciation }}</td>
                                    <td>{{ $data->balance_remain }}</td>
                                </tr>
                                @php 
                                    $count3++; 
                                    $depreciation3 += $data->depreciation;
                                    $monthly_depcreciation3 += $data->monthly_depcreciation;
                                    $total_cost3 += $data->total_cost;
                                    $remain3 += $data->balance_remain;
                                @endphp
                            @endforeach
                        </tbody>   
                        <tfoot style="font-weight: bold; color: #fff; background: #0098bd;">
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Total :</td>
                                <td></td>
                                <td></td>
                                <td>{{ $total_cost3 }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $monthly_depcreciation3 }}</td>
                                <td>{{ $depreciation3 }}</td>
                                <td>{{ $remain3 }}</td>
                            </tr>
                        </tfoot>  
                    </table>
                </div>
                <!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    <!-- Office Furniture -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-header">
                    <legend>Book value of Office Furniture and Other Equipment</legend>
                </div>
                
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover" style="text-align: center;">
                        <thead>
                            <tr>
                                <th style="text-align: center; background: #222d32; color: #fff;">SL.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Particuler</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Pur. Date</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">No. of Unit</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Unit Price</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Total</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Life(Years)</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Life(Months)</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Months in Use</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Monthly Dep.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Acc. Dep.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Balance Remain</th>
                            </tr>
                        </thead>  
                        <tbody>
                            @php 
                                $count4 = 1; $depreciation4 = $monthly_depcreciation4 = $total_cost4 = $remain4 = 0; 
                            @endphp
                            @foreach($OFE as $data)
                                <tr>
                                    <td>{{ $count4 }}</td>
                                    <td>{{ $data->particuler }}</td>
                                    <td>{{ $data->puchase_date }}</td>
                                    <td>{{ $data->no_of_unit }}</td>
                                    <td>{{ $data->unit_price }}</td>
                                    <td>{{ $data->total_cost }}</td>
                                    <td>{{ $data->life_in_year }}</td>
                                    <td>{{ $data->life_in_month }}</td>
                                    <td>{{ $data->month_in_use }}</td>
                                    <td>{{ $data->monthly_depcreciation }}</td>
                                    <td>{{ $data->depreciation }}</td>
                                    <td>{{ $data->balance_remain }}</td>
                                </tr>
                                @php 
                                    $count4++; 
                                    $depreciation4 += $data->depreciation;
                                    $monthly_depcreciation4 += $data->monthly_depcreciation;
                                    $total_cost4 += $data->total_cost;
                                    $remain4 += $data->balance_remain;
                                @endphp
                            @endforeach
                        </tbody>
                        <tfoot style="font-weight: bold; color: #fff; background: #0098bd;">
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Total :</td>
                                <td></td>
                                <td></td>
                                <td>{{ $total_cost4 }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $monthly_depcreciation4 }}</td>
                                <td>{{ $depreciation4 }}</td>
                                <td>{{ $remain4 }}</td>
                            </tr>
                        </tfoot>      
                    </table>
                </div>
                <!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

    <!-- TABS -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-header">
                    <legend>Book value of TABS</legend>
                </div>
                
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover" style="text-align: center;">
                        <thead>
                            <tr>
                                <th style="text-align: center; background: #222d32; color: #fff;">SL.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Particuler</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Pur. Date</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">No. of Tabs</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Unit Price</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Total</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Life(Years)</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Life(Months)</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Months in Use</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Monthly Dep.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Acc. Dep.</th>
                                <th style="text-align: center; background: #222d32; color: #fff;">Balance Remain</th>
                            </tr>
                        </thead>  
                        <tbody>
                            @php 
                                $count5 = 1; $depreciation5 = $monthly_depcreciation5 = $total_cost5 = $remain5 = 0; 
                            @endphp
                            @foreach($tabs as $data)
                                <tr>
                                    <td>{{ $count5 }}</td>
                                    <td>{{ $data->particuler }}</td>
                                    <td>{{ $data->puchase_date }}</td>
                                    <td>{{ $data->no_of_unit }}</td>
                                    <td>{{ $data->unit_price }}</td>
                                    <td>{{ $data->total_cost }}</td>
                                    <td>{{ $data->life_in_year }}</td>
                                    <td>{{ $data->life_in_month }}</td>
                                    <td>{{ $data->month_in_use }}</td>
                                    <td>{{ $data->monthly_depcreciation }}</td>
                                    <td>{{ $data->depreciation }}</td>
                                    <td>{{ $data->balance_remain }}</td>
                                </tr>
                                @php 
                                    $count5++; 
                                    $depreciation5 += $data->depreciation;
                                    $monthly_depcreciation5 += $data->monthly_depcreciation;
                                    $total_cost5 += $data->total_cost;
                                    $remain5 += $data->balance_remain;
                                @endphp
                            @endforeach
                        </tbody>
                        <tfoot style="font-weight: bold; color: #fff; background: #0098bd;">
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Total :</td>
                                <td></td>
                                <td></td>
                                <td>{{ $total_cost5 }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $monthly_depcreciation5 }}</td>
                                <td>{{ $depreciation5 }}</td>
                                <td>{{ $remain5 }}</td>
                            </tr>
                        </tfoot>      
                    </table>
                </div>
                <!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->

</section>




@endsection