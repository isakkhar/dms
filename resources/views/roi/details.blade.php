@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">
        	ROI For @if($month == 1) {{ 'January' }},
    				@elseif($month == 2) {{ 'February' }},
    				@elseif($month == 3) {{ 'March' }},
    				@elseif($month == 4) {{ 'April' }},
    				@elseif($month == 5) {{ 'May' }},
    				@elseif($month == 6) {{ 'June' }},
    				@elseif($month == 7) {{ 'July' }},
    				@elseif($month == 8) {{ 'August' }},
    				@elseif($month == 9) {{ 'September' }},
    				@elseif($month == 10) {{ 'October' }},
    				@elseif($month == 11) {{ 'November' }},
    				@else {{ 'December' }},
    				@endif
    			{{ $year }}
        </h3>
    </div>
</div>

<section class="content">

	<!-- Sales -->
	<div class="row">
	        <div class="col-xs-12">
	            <div class="box">
	                <!-- /.box-header -->
	                <div class="box-header">
	                    <legend>UBL Business</legend>
	                </div>
	                
	                <div class="box-body table-responsive">
	                    <table class="table table-bordered table-hover">
	                        <thead>
	                            <tr>
									<th style="background: #16365C; color: #fff;">Particulars</th>
									<th style="background: #16365C; color: #fff;">Amount</th>
	                            </tr>
	                        </thead>  
	                        <tbody>
	                        	<tr>
	                        		<td>Total Secondary Sales - List Price</td>
	                        		<td>{{ round($sales['total_sales']) }}</td>
	                        	</tr>
	                        	<tr>
	                        		<td>Distribution Drive (DD)- Free Goods</td>
	                        		<td>{{ round($sales['total_free']) }}</td>
	                        	</tr>
	                        	<tr>
	                        		<td>Regular Sales in List Price</td>
	                        		<td>{{ round($sales['total_earn']) }}</td>
	                        	</tr>
	                        	<tr>
	                        		<td>Regular Sales -List Price @ 4.67%</td>
	                        		<td>{{ $sales['regular_list_price'] }}</td>
	                        	</tr>
	                        </tbody>    
	                        <tfoot style="font-weight: bold; color: #fff; background: #16365C;">
	                            <tr>
	                                <td>Total Earnings from UBL Business</td>
	                                <td>{{ $sales['regular_list_price'] }}</td>
	                            </tr>
	                        </tfoot>  
	                    </table>
	                </div>
	                <!-- /.box-body -->
	            </div><!-- /.box -->
	        </div><!-- /.col -->
	</div><!-- /.row -->

	<!-- Investment -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-header">
				    <legend>Investment</legend>
				</div>

				<div class="box-body table-responsive">
					<table class="table table-bordered table-hover">
				    	<thead>
				    		<tr>
				    			<th style="background: #16365C; color: #fff;">Particulars</th>
				    			<th style="background: #16365C; color: #fff;">Amount</th>
				    		</tr>
				    	</thead>  
				        <tbody>
				            <tr>
				            	<td>Investment in Stock</td>
				            	<td>{{ $investment['investment'] }}</td>
				            </tr>
				            <tr>
				            	<td>AIT on Dist. Margin (Advance Income Tax)</td>
				            	<td>{{ $investment['income_tax'] }}</td>
				            </tr>
				            <tr>
				            	<td>Credit to Trade</td>
				            	<td>{{ $investment['credit_to_trade'] }}</td>
				            </tr>
				            <tr>
				            	<td>Cash in hand/Bank</td>
				            	<td>{{ $investment['cash'] }}</td>
				            </tr>
				            <tr>
				            	<td>Pending Claims with UBL</td>
				            	<td>{{ $investment['pending_claims'] }}</td>
				            </tr>
				            <tr>
				            	<td>Book Value of PDA/TAB</td>
				            	<td>{{ $investment['tab'] }}</td>
				            </tr>
				            <tr>
				            	<td>Book Value of Computer and Printer</td>
				            	<td>{{ $investment['computer'] }}</td>
				            </tr>
				            <tr>
				            	<td>Book Value of Office Furnitures and other equipments</td>
				            	<td>{{ $investment['furniture'] }}</td>
				            </tr>
				            <tr>
				            	<td>Book Value of Vehicles</td>
				            	<td>{{ $investment['vehicle'] }}</td>
				            </tr>
				       	</tbody>    
				        <tfoot style="font-weight: bold; color: #fff; background: #16365C;">
				        	<tr>
				                <td>Total Investments</td>
				                <td>{{ $investment['total'] }}</td>
				            </tr>
				        </tfoot>  
				    </table>
				</div>
				<!-- /.box-body -->	                
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->

	<!-- Depreciation -->
	<div class="row">
	        <div class="col-xs-12">
	            <div class="box">
	                <!-- /.box-header -->
	                <div class="box-header">
	                    <legend>Deprecation</legend>
	                </div>
	                
	                <div class="box-body table-responsive">
	                    <table class="table table-bordered table-hover">
	                        <thead>
	                            <tr>
									<th style="background: #16365C; color: #fff;">Particulars</th>
									<th style="background: #16365C; color: #fff;">Amount</th>
	                            </tr>
	                        </thead>  
	                        <tbody>
	                        	<tr>
	                        		<td>Depreciation on Vehicle</td>
	                        		<td>{{ round($dep['vehicle_dep']) }}</td>
	                        	</tr>
	                        	<tr>
	                        		<td>Depreciation on Computer and Printer</td>
	                        		<td>{{ round($dep['computer_dep']) }}</td>
	                        	</tr>
	                        	<tr>
	                        		<td>Depreciation on Office Furniture and Other Equipment</td>
	                        		<td>{{ round($dep['furniture_dep']) }}</td>
	                        	</tr>
	                        	<tr>
	                        		<td>Depreciation on PDA/TAB</td>
	                        		<td>{{ round(($dep['tabs_dep'] * 4.67)/100) }}</td>
	                        	</tr>
	                        </tbody>    
	                        <tfoot style="font-weight: bold; color: #fff; background: #16365C;">
	                            <tr>
	                                <td>Total Depreciation</td>
	                                <td>{{ $dep['total'] }}</td>
	                            </tr>
	                        </tfoot>  
	                    </table>
	                </div>
	                <!-- /.box-body -->
	            </div><!-- /.box -->
	        </div><!-- /.col -->
	</div><!-- /.row -->
		
	<!-- Office Staff Salary -->
	<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<!-- /.box-header -->
					<div class="box-header">
					    <legend>Office Staff Salary</legend>
					</div>

					<div class="box-body table-responsive">
						<table class="table table-bordered table-hover">
					    	<thead>
					    		<tr>
					    			<th style="background: #16365C; color: #fff;">Designation</th>
					    			<th style="background: #16365C; color: #fff;">Amount</th>
					    		</tr>
					    	</thead>  
					        <tbody>
					            @php $total_office = 0; @endphp
					            @foreach($salary['office'] as $value)
					            <tr>
					                <td>{{ $value->designation }}</td>
					                <td>{{ $value->salary }}</td>
					            </tr>
					            @php $total_office += $value->salary; @endphp
					            @endforeach
					       	</tbody>    
					        <tfoot style="font-weight: bold; color: #fff; background: #16365C;">
					        	<tr>
					                <td>Total Office Staff Salaries</td>
					                <td>{{ $total_office }}</td>
					            </tr>
					        </tfoot>  
					    </table>
					</div>
					<!-- /.box-body -->	                
				</div><!-- /.box -->
			</div><!-- /.col -->
	</div><!-- /.row -->

	<!-- Field Team Salary -->
	<div class="row">
	        <div class="col-xs-12">
	            <div class="box">
	                <!-- /.box-header -->
	                <div class="box-header">
	                    <legend>Field Team Salary</legend>
	                </div>
	                
	                <div class="box-body table-responsive">
	                    <table class="table table-bordered table-hover">
	                        <thead>
	                            <tr>
									<th style="background: #16365C; color: #fff;">Designation</th>
									<th style="background: #16365C; color: #fff;">Amount</th>
	                            </tr>
	                        </thead>  
	                        <tbody>
	                        	@php $total_field = 0; @endphp
	                        	@foreach($salary['field'] as $value)
	                        		<tr>
	                        			<td>{{ $value->designation }}</td>
	                        			<td>{{ $value->salary }}</td>
	                        		</tr>
	                        	@php $total_field += $value->salary; @endphp
	                        	@endforeach
	                        </tbody>    
	                        <tfoot style="font-weight: bold; color: #fff; background: #16365C;">
	                            <tr>
	                                <td>Total Field Team Salaries</td>
	                                <td>{{ $total_field }}</td>
	                            </tr>
	                            <tr style="height: 5px;"></tr>
	                            <tr>
	                                <td>Total Salaries(Office Staff + Field Team)</td>
	                                <td>{{ $total_field + $total_office }}</td>
	                            </tr>
	                        </tfoot>  
	                    </table>
	                </div>
	                <!-- /.box-body -->
	            </div><!-- /.box -->
	        </div><!-- /.col -->
	</div><!-- /.row -->

	<!-- Redistribution -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-header">
				    <legend>Redistribution Costs</legend>
				</div>

				<div class="box-body table-responsive">
					<table class="table table-bordered table-hover">
				    	<thead>
				    		<tr>
				    			<th style="background: #16365C; color: #fff;">Particulars</th>
				    			<th style="background: #16365C; color: #fff;">Amount</th>
				    		</tr>
				    	</thead>  
				        <tbody>
				        	<tr>
				        		<td>Loading & Unloading</td>
				        		<td>{{ $redistribution['load_unload'] }}</td>
				        	</tr>
				        	<tr>
				        		<td>Head Load Delivery</td>
				        		<td>{{ $redistribution['head_load'] }}</td>
				        	</tr>
				        	<tr>
				        		<td>TA DA</td>
				        		<td>{{ $redistribution['ta_da'] }}</td>
				        	</tr>
				       	</tbody>    
				        <tfoot style="font-weight: bold; color: #fff; background: #16365C;">
				        	<tr>
				                <td>Redistribution Costs</td>
				                <td>{{ $redistribution['total'] }}</td>
				            </tr>
				        </tfoot>  
				    </table>
				</div>
				<!-- /.box-body -->	                
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->
	
	<!-- Vehicle Cost -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-header">
				    <legend>Vehicle Maintenance & Running Costs</legend>
				</div>

				<div class="box-body table-responsive">
					<table class="table table-bordered table-hover">
				    	<thead>
				    		<tr>
				    			<th style="background: #16365C; color: #fff;">Particulars</th>
				    			<th style="background: #16365C; color: #fff;">Amount</th>
				    		</tr>
				    	</thead>  
				        <tbody>
				            <tr>
				                <td>Fuel Costs for vehicle only</td>
				                <td>{{ $vehicle['fuel'] }}</td>
				            </tr>
				            <tr>
				                <td>Vehicle Maintenance Costs</td>
				                <td>{{ $vehicle['maintenance'] }}</td>
				            </tr>
				       	</tbody>    
				        <tfoot style="font-weight: bold; color: #fff; background: #16365C;">
				        	<tr>
				                <td>Vehicle Maintenance & Running Costs</td>
				                <td>{{ $vehicle['total'] }}</td>
				            </tr>

				            <tr>
				            	<td>Total Redistribution Costs</td>
				            	<td>{{ $redistribution['total'] + $vehicle['total'] }}</td>
				            </tr>
				        </tfoot>  
				    </table>
				</div>
				<!-- /.box-body -->	                
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->

	<!-- Rent -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-header">
				    <legend>Rent</legend>
				</div>

				<div class="box-body table-responsive">
					<table class="table table-bordered table-hover">
				    	<thead>
				    		<tr>
				    			<th style="background: #16365C; color: #fff;">Particulars</th>
				    			<th style="background: #16365C; color: #fff;">Amount</th>
				    		</tr>
				    	</thead>  
				        <tbody>
				            <tr>
				                <td>Rent Payable</td>
				                <td>{{ $rent }}</td>
				            </tr>
				       	</tbody>    
				        <tfoot style="font-weight: bold; color: #fff; background: #16365C;">
				        	<tr>
				                <td>Total Rent Paid</td>
				                <td>{{ $rent }}</td>
				            </tr>
				        </tfoot>  
				    </table>
				</div>
				<!-- /.box-body -->	                
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->

	<!-- Office Expenses -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-header">
				    <legend>Office Expenses</legend>
				</div>

				<div class="box-body table-responsive">
					<table class="table table-bordered table-hover">
				    	<thead>
				    		<tr>
				    			<th style="background: #16365C; color: #fff;">Particulars</th>
				    			<th style="background: #16365C; color: #fff;">Amount</th>
				    		</tr>
				    	</thead>  
				        <tbody>
				            <tr>
				                <td>Computer Maintenance</td>
				                <td>{{ $offfice_expenses['computer'] }}</td>
				            </tr>
				            <tr>
				                <td>Furniture Maintenance</td>
				                <td>{{ $offfice_expenses['furniture'] }}</td>
				            </tr>
				            <tr>
				                <td>Generator Fuel</td>
				                <td>{{ $offfice_expenses['generator'] }}</td>
				            </tr>
				            <tr>
				                <td>Other Office Expenses</td>
				                <td>{{ $offfice_expenses['other'] }}</td>
				            </tr>
				       	</tbody>    
				        <tfoot style="font-weight: bold; color: #fff; background: #16365C;">
				        	<tr>
				                <td>Total Office Expenses</td>
				                <td>{{ $offfice_expenses['total'] }}</td>
				            </tr>
				        </tfoot>  
				    </table>
				</div>
				<!-- /.box-body -->	                
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->

	<!-- Utility -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-header">
				    <legend>Utilities</legend>
				</div>

				<div class="box-body table-responsive">
					<table class="table table-bordered table-hover">
				    	<thead>
				    		<tr>
				    			<th style="background: #16365C; color: #fff;">Partuculars</th>
				    			<th style="background: #16365C; color: #fff;">Amount</th>
				    		</tr>
				    	</thead>  
				        <tbody>
				        	<tr>
				        		<td>Landline telephone Bills</td>
				        		<td>{{ $utility['landline'] }}</td>
				        	</tr>
				        	<tr>
				        		<td>Mobile Phone Bills</td>
				        		<td>{{ $utility['mobile'] }}</td>
				        	</tr>
				        	<tr>
				        		<td>WASA Bills</td>
				        		<td>{{ $utility['wasa'] }}</td>
				        	</tr>
				        	<tr>
				        		<td>Electricity Bills</td>
				        		<td>{{ $utility['electricity'] }}</td>
				        	</tr>
				        	<tr>
				        		<td>Gas Bills</td>
				        		<td>{{ $utility['gas'] }}</td>
				        	</tr>
				        	<tr>
				        		<td>Internet Bills for office usage</td>
				        		<td>{{ $utility['office_internet'] }}</td>
				        	</tr>
				        	<tr>
				        		<td>Internet Bills for HHT</td>
				        		<td>{{ $utility['hht_internet'] }}</td>
				        	</tr>
				       	</tbody>    
				        <tfoot style="font-weight: bold; color: #fff; background: #16365C;">
				        	<tr>
				                <td>Total Utilities</td>
				                <td>{{ $utility['total'] }}</td>
				            </tr>
				        </tfoot>  
				    </table>
				</div>
				<!-- /.box-body -->	                
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->

	<!-- Miscellaneous -->
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<!-- /.box-header -->
				<div class="box-header">
				    <legend>Miscellaneous</legend>
				</div>

				<div class="box-body table-responsive">
					<table class="table table-bordered table-hover">
				    	<thead>
				    		<tr>
				    			<th style="background: #16365C; color: #fff;">Particulars</th>
				    			<th style="background: #16365C; color: #fff;">Amount</th>
				    		</tr>
				    	</thead>  
				        <tbody>
				            <tr>
				            	<td>Others(Entertainment/PF/Bad Debt/Incidental Expenses/Legal Consultancies/Insurance premium on Cash and Godown etc.)</td>
				            	<td>{{ $miscellaneous }}</td>
				            </tr>
				       	</tbody>    
				        <tfoot style="font-weight: bold; color: #fff; background: #16365C;">
				        	<tr>
				                <td>Total Miscellaneous</td>
				                <td>{{ $miscellaneous }}</td>
				            </tr>
				        </tfoot>  
				    </table>
				</div>
				<!-- /.box-body -->	                
			</div><!-- /.box -->
		</div><!-- /.col -->
	</div><!-- /.row -->

	<!-- Last Infos -->
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-body table-responsive">
					<table class="table table-bordered table-hover">
				    	<thead>
				    		<tr>
				    			@php
				    				$total_expenses = $miscellaneous + $utility['total'] + $offfice_expenses['total'] + $rent + $redistribution['total'] + $vehicle['total'] + $total_field + $total_office + $dep['total']; 
				    			@endphp
				    			<th style="background: #16365C; color: #fff;">Total Expenses</th>
				    			<th style="background: #16365C; color: #fff;">{{ $total_expenses }}</th>
				    		</tr>
				    		<tr>
				    			<th style="background: #16365C; color: #fff;">Trading Profit</th>
				    			<th style="background: #16365C; color: #fff;">{{ $sales['regular_list_price'] - $total_expenses }}</th>
				    		</tr>
				    		<tr>
				    			<th style="background: #16365C; color: #fff;">Annualized Return On Investment</th>
				    			<th style="background: #16365C; color: #fff;">{{ round(((($sales['regular_list_price'] - $total_expenses) * 12) / $investment['total']) * 100) }}%</th>
				    		</tr>
				    	</thead>  
				    </table>
				</div>
			</div>
		</div>
	</div>

</section>




@endsection