@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Pending Claims</h3>
    </div>
</div>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
                <table class="table table-bordered table-hover" style="text-align: center;">
                    <thead>
                        <tr>
                        	<th style="text-align: center;">Claim Code</th>
                        	<th style="text-align: center;">Details</th>
                        	<th style="text-align: center;">Claim Amount</th>
                        	<th style="text-align: center;">Pass Amount</th>
                        	<th style="text-align: center;">Due Amount</th>
                        	<th style="text-align: center;">Claim Date</th>
                        	<th style="text-align: center;">Last Claim Pass</th>
                        </tr>
                    </thead>
                            
                    <tbody>
                    	@php $claim = $pass = $due = 0; @endphp
                    	@foreach($pending_claims as $each)
                    		<tr>
                    			<td>{{ $each->id }}</td>
                    			<td>{{ $each->name }}</td>
                    			<td>{{ $each->amount + $each->pass }}</td>
                    			<td>{{ $each->pass != null ? $each->pass : '0' }}</td>
                    			<td>{{ $each->amount }}</td>
                    			<td>{{ Carbon\Carbon::parse($each->created_at)->toDateString() }}</td>
                    			<td>{{ Carbon\Carbon::parse($each->updated_at)->toDateString() }}</td>
                    		</tr>
                    		@php
                    			$claim += $each->amount + $each->pass;
                    			$pass += $each->pass;
                    			$due += $each->amount;
                    		@endphp
                    	@endforeach
                    	<tr style="background: #2a5e75; color: #fff; font-weight: bold;">
                    		<td></td>
                    		<td>Total</td>
                    		<td>{{ $claim }}</td>
                    		<td>{{ $pass }}</td>
                    		<td>{{ $due }}</td>
                    		<td></td>
                    		<td></td>
                    	</tr>
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>




@endsection