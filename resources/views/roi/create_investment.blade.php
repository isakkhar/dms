@extends('layouts.master')
@section('page_main_content')

@if(session('success'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('success') }}
        </div>
@endif

<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box-header with-border">
						<legend class="box-title" style="text-align: center; font-weight: bold;">Add an Investment</legend>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form role="form" method="post" enctype="multipart/form-data" action="{{ url('store-investment') }}">
				@csrf
				<div class="box-body">

					<!-- Name -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name">Investment Name :</label>
								<select name="name" class="form-control">
									<option value="">Select</option>
									<option value="1">1.Mechanized Vehicle</option>
									<option value="2">2.Non Mechanized Vehicle</option>
									<option value="3">3.Computer Equipment</option>
									<option value="4">4.Office Furniture and Other Equipment</option>
									<option value="5">5.TABs</option>
								</select>
								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					<!-- Life -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('year') ? ' has-error' : '' }}">
								<label for="year">Life in Year :</label>
								<input type="number" name="year" value="{{ old('year') }}" class="form-control" placeholder="Enter Life in year" required="">
								@if ($errors->has('year'))
								<span class="help-block">
									<strong>{{ $errors->first('year') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>
					
					
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary col-sm-12">Submit</button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
@endsection