@extends('layouts.master')
@section('page_main_content')

@if(session('success'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('success') }}
        </div>
@endif

<div class="row">
	<div class="col-md-12">
		<!-- general form elements -->
		<div class="box box-primary">
			<div class="row">
				<div class="col-md-3"></div>
				<div class="col-md-6">
					<div class="box-header with-border">
						<legend class="box-title" style="text-align: center; font-weight: bold;">Add an Investment</legend>
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>
			<!-- /.box-header -->
			<!-- form start -->
			<form method="post" enctype="multipart/form-data" action="{{ url('store-investment-details') }}">
				@csrf
				<div class="box-body">

					<!-- id -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name">Investment :</label>
								<select name="investment" class="form-control">
									<option value="">Select</option>
									@foreach($investments as $investment)
									<option value="{{$investment->name}}">
										@if($investment->name == 1) Mechanized Vehicle
										@elseif($investment->name == 2) Non Mechanized Vehicle
										@elseif($investment->name == 3) Computer Equipment
										@elseif($investment->name == 4) Office Furniture and Other Equipment
										@elseif($investment->name == 5) TAB
										@endif
									</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-3"></div>
					</div>
					<!-- name -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('name') ? ' has-error' : '' }}">
								<label for="name">Name :</label>
								<input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Enter Value" required="">
								@if ($errors->has('name'))
								<span class="help-block">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>

					<!-- purchase date -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('date') ? ' has-error' : '' }}">
								<label for="date">Purchase Date :</label>
								<input type="date" name="date" value="{{ old('date') }}" class="form-control" placeholder="Enter Value" required="">
								@if ($errors->has('date'))
								<span class="help-block">
									<strong>{{ $errors->first('date') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>


					<!-- unit price -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('price') ? ' has-error' : '' }}">
								<label for="price">Unit Price :</label>
								<input type="number" name="price" value="{{ old('price') }}" class="form-control" placeholder="Enter Value" required="">
								@if ($errors->has('price'))
								<span class="help-block">
									<strong>{{ $errors->first('price') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>


					<!-- no of unit -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('no_of_unit') ? ' has-error' : '' }}">
								<label for="no_of_unit">No of Unit :</label>
								<input type="number" name="no_of_unit" value="{{ old('no_of_unit') }}" class="form-control" placeholder="Enter Value" required="">
								@if ($errors->has('no_of_unit'))
								<span class="help-block">
									<strong>{{ $errors->first('no_of_unit') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>

					<!-- used month -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('used_month') ? ' has-error' : '' }}">
								<label for="used_month">Month Used :</label>
								<input type="number" name="used_month" value="{{ old('used_month') }}" class="form-control" placeholder="Enter Value" required="">
								@if ($errors->has('used_month'))
								<span class="help-block">
									<strong>{{ $errors->first('used_month') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>
					
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary col-sm-12">Submit</button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</form>
		</div>
		<!-- /.box -->
	</div>
</div>
@endsection