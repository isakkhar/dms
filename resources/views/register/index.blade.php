@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h4 style="float: left;">Daily Accounts</h4>
 		<h4 style="float: right;">Date : {{ Carbon\Carbon::today()->format('d-M-Y') }}</h4>
    </div>
</div>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-resposive">
                <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info" style="text-align: center;">
                    <thead>
                        <tr>
                            <th style="text-align: center;">SL.</th>
                            <th style="text-align: center;">Sales</th>
                            <th style="text-align: center;">Expenses</th>
                            <th style="text-align: center;">Cash</th>
                            <th style="text-align: center;">Cheque & Due</th>
                            <th style="text-align: center;">Diff</th>
                            <th style="text-align: center;">D/R Cheque</th>
                            <th style="text-align: center;">D/R Cash</th>
                        </tr>
                    </thead>
                    <tbody>
                    	@php 
                    		$i = 1;
                    		$total_sales = $total_expenses = $total_cash = $total_cheque_due = $total_diff = $total_due_realize_cheque = $total_due_realize_cash = 0;
                    	@endphp
                        @foreach($data as $each)
                        <tr>
                        	<td> {{ $i }} </td>
                        	<td> {{ $each['sales'] }} </td>
                        	<td> {{ $each['expenses'] }} </td>
                        	<td> {{ $each['cash'] }} </td>
                        	<td> {{ $each['cheque_due'] }} </td>
                        	<td> {{ $each['diff'] }} </td>
                        	<td> {{ $each['due_realizes_cheque'] }} </td>
                        	<td> {{ $each['due_realizes_cash'] }} </td>
                        </tr>
                        @php 
                        	$i++; 
                        	$total_sales += $each['sales'];
                        	$total_expenses += $each['expenses'];
                        	$total_cash += $each['cash'];
                        	$total_cheque_due += $each['cheque_due'];
                        	$total_diff += $each['diff'];
                        	$total_due_realize_cheque += $each['due_realizes_cheque'];
                        	$total_due_realize_cash += $each['due_realizes_cash'];
                        @endphp
                        @endforeach
                    </tbody>
                    <tfoot>
                    	<tr style="background: #ccc;">
                    		<td>Total :</td>
                    		<td> {{ $total_sales }} </td>
                    		<td> {{ $total_expenses }} </td>
                    		<td> {{ $total_cash }} </td>
                    		<td> {{ $total_cheque_due }} </td>
                    		<td> {{ $total_diff }} </td>
                    		<td> {{ $total_due_realize_cheque }} </td>
                    		<td> {{ $total_due_realize_cash }} </td>
                    	</tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="box">
      <div class="box-header">
          <h4 style="float: left;">Final Daily Accounts</h4>
      </div>
  </div>


  <div class="row">
    <div class="col-xs-6">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-resposive">
                <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info" style="text-align: center;">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Particular's</th>
                            <th style="text-align: center;">Taka</th>
                        </tr>
                    </thead>
                    <tbody>  
                    @php $total_due_details = $total_cheque_details = $total_due_realize_details = 0; @endphp           
                        <tr>
                        	<td>Company Expenses</td>
                        	<td>{{ $total_expenses }}</td>
                        </tr>
                        <tr>
                        	<td colspan="2" style="font-weight: bold;">Due List :</td>
                        </tr>
                        @for($i=0; $i < count($due_details); $i++) 
                        	@for($j=0; $j < count($due_details[$i]); $j++) 
                        		<tr>
                        			<td> {{ $due_details[$i][$j]['shop_name'] }} </td>
                        			<td> {{ $due_details[$i][$j]['amount'] }} </td>
                        		</tr>
                        		@php $total_due_details += $due_details[$i][$j]['amount']; @endphp	
                        	@endfor
                        @endfor
						<tr>
							<td colspan="2"></td>
						</tr>
                        <tr>
                        	<td>Cash in Hand </td>
                        	<td> {{ $total_cash + $total_due_realize_cash }} </td>
                        </tr>

                        <tr>
                        	<td colspan="2" style="font-weight: bold;">Cheque :</td>
                        </tr>
                        @for($i=0; $i < count($cheque_details); $i++) 
                        	@for($j=0; $j < count($cheque_details[$i]); $j++) 
                        		<tr>
                        			<td> {{ $cheque_details[$i][$j]['shop_name'] }} </td>
                        			<td> {{ $cheque_details[$i][$j]['amount'] }} </td>
                        		</tr>
                        		@php $total_cheque_details += $cheque_details[$i][$j]['amount']; @endphp	
                        	@endfor
                        @endfor
                    </tbody>

                    <tfoot>
                    	<tr style="background: #ccc;">
                    		<td>Total :</td>
                    		<td> {{ $total_cheque_details + $total_cash + $total_due_realize_cash + $total_due_details + $total_expenses }} </td>
                    	</tr>
                    </tfoot>

                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->

    <div class="col-xs-6">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-resposive">
                <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info" style="text-align: center;">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Particular's</th>
                            <th style="text-align: center;">Taka</th>
                        </tr>
                    </thead>
                    <tbody>             
                        <tr>
                        	<td>Daily Sales</td>
                        	<td> {{ $total_sales }} </td>
                        </tr>
                        <tr>
                        	<td colspan="2" style="font-weight: bold;">Due Realize List :</td>
                        </tr>
                        @for($i=0; $i < count($due_realize); $i++) 
                        	@for($j=0; $j < count($due_realize[$i]); $j++) 
                        		<tr>
                        			<td> {{ $due_realize[$i][$j]['shop_name'] }} </td>
                        			<td> {{ $due_realize[$i][$j]['amount'] }} </td>
                        		</tr>
                        		@php $total_due_realize_details += $due_realize[$i][$j]['amount']; @endphp
                        	@endfor
                        @endfor
                    </tbody>
                    <tfoot>
                    	<tr style="background: #ccc;">
                    		<td>Total :</td>
                    		<td> {{ $total_due_realize_details + $total_sales }} </td>
                    	</tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
<hr style="background: #ccc; height: 1px;">
	<div class="row">
		<div class="col-md-6">
			<p style="font-weight: bold;">Total Cash : {{ $total_due_realize_cash + $total_cash }}</p>
		</div>

		<div class="col-md-6">
			<p style="font-weight: bold;">Total Due : {{ $total_due_details }}</p>
			<p style="font-weight: bold;">Total Due Realize : {{ $total_due_realize_details }}</p>
		</div>
	</div>

</section>




@endsection