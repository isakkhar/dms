@extends('layouts.master')
@section('page_main_content')

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Provident Fund List of Employees</h3>
    </div>

@if(session('msg'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('msg') }}
        </div>
@endif

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Provident Fund</th>
                            <th>Payable Provident Fund</th>
                            <th>Paid Provident Fund</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                            
                    <tbody>
                        @foreach($employees as $employee)
                        <?php 
                            $total_amount =0;
                            $date1 = strtotime($employee->join_date);  
                            $date2 = strtotime(date('Y/m/d', time()));  
                              
                            // Formulate the Difference between two dates 
                            $diff = abs($date2 - $date1);  
                            
                            $months = floor($diff / (30*60*60*24)); 
                        ?>
                        @foreach($employee->provident as $provident)
                         <?php $total_amount += $provident->paid_amount; ?>
                        @endforeach
                        <tr>
                            <td>{{ $employee->id }}</td>
                            <td>{{ $employee->name }}</td>
                            <td>{{ $employee->email }}</td>
                            <td>{{ $employee->provident_fund }}</td>
                            <td>{{ $employee->provident_fund * $months - $total_amount}}</td>
                            <td>{{$total_amount}}</td>
                            <td>
                                <a href="{{ route('provident-fund-pay',['employee' => $employee->id]) }}" class="btn btn-info">
                                    Pay
                                </a>
                            </td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
</div>
@endsection