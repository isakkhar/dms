@extends('layouts.master')
@section('page_main_content')

<?php 
$total_amount =0;
    $date1 = strtotime($employee->join_date);  
    $date2 = strtotime(date('Y/m/d', time()));  
      
    // Formulate the Difference between two dates 
    $diff = abs($date2 - $date1);  
    
    $months = floor($diff / (30*60*60*24)); 
?>
@foreach($employee->provident as $provident)
    <?php $total_amount += $provident->paid_amount; ?>
@endforeach
<div class="box">
    <div class="box-header">
        <h2 class="box-title">Pay Provident fund to <strong>{{$employee->name}}</strong></h2><br>
        <h3 class="box-title">Net payable <strong>{{$employee->provident_fund * $months - $total_amount."TK"}}</strong></h3>
    </div>
    
    <section>
        <form role="form" method="post" action="{{ route('provident-fund-post') }}">
				@csrf
				<div class="box-body">

					<!-- Amount -->
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<div class="form-group col-sm-12 {{ $errors->has('amount') ? ' has-error' : '' }}">
								<label for="amount">Amount :</label>
								<input type="number" name="amount" value="{{ old('amount') }}" class="form-control" placeholder="Enter Amount" required="" max="{{  $employee->provident_fund * $months }}">
								@if ($errors->has('amount'))
								<span class="help-block">
									<strong>{{ $errors->first('amount') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="col-md-3"></div>				
					</div>
					<input type="hidden" value="{{ $employee->id }}" name="id" />
					
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
					<div class="row">
						<div class="col-md-3"></div>
						<div class="col-md-6">
							<button type="submit" class="btn btn-primary col-sm-12">Submit</button>
						</div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</form>
    </section>
    
    
</div>

@endsection