@extends('layouts.master')
@section('page_main_content')

@if(session('success'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('success') }}
        </div>
@endif

<div class="box">
    <div class="box-header">
        <h3 class="box-title">All Cheque List</h3>
    </div>
</div>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div class="table-resposive">
                <table class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info" style="text-align: center;">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Cheque ID</th>
                            <th style="text-align: center;">Shop Name</th>
                            <th style="text-align: center;">Cheque Number</th>
                            <th style="text-align: center;">Amount</th>
                            <th style="text-align: center;">Received Date</th>
                            <th style="text-align: center;">Deposite Date</th>
                            <th style="text-align: center;">Status</th>
                            <th style="text-align: center;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($cheque_list as $each)
                        <tr @if(Carbon\Carbon::parse($each->deposite_date)->toDateString() == Carbon\Carbon::today()->toDateString())
                            style="background: #fd5353;" 
                            @endif>
                            <td>{{ $each->id }}</td>
                            <td>{{ $each->shop_name }}</td>
                            <td>{{ $each->number }}</td>
                            <td>{{ $each->amount }}</td>
                            <td>{{ Carbon\Carbon::parse($each->received_date)->format('d-M-Y') }}</td>
                            <td>{{ Carbon\Carbon::parse($each->deposite_date)->format('d-M-Y') }}</td>
                            <td>
                                @if($each->status === 0)
                                    <button type="button" class="btn btn-danger btn-xs">Bounce</button>
                                @elseif($each->status == 1)
                                    <button type="button" class="btn btn-success btn-xs">Passed</button>
                                @else
                                    <button type="button" class="btn btn-default btn-xs">None</button>
                                @endif
                            </td>
                            <td>
                                @if($each->status === NULL)
                                    <a href="{{ url('/cheque_pass/'.$each->id) }}" class="btn btn-success btn-sm">Pass</a>
                                    <a href="{{ url('/cheque_bounce/'.$each->id) }}" class="btn btn-danger btn-sm">Bounce</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>




@endsection