@extends('layouts.master')
@section('page_main_content')
<div class="row" style="margin-bottom: 20px;">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <legend style="border-color: #000; text-align: center;">Add New Shop</legend>
        <form action="{{ url('/shop') }}" method="post" accept-charset="utf-8">
            @csrf
            <input type="text" name="shop" class="form-control" style="border: 1px solid #00a65a;" placeholder="Enter Shop Name here"><br>
            <button type="submit" class="btn btn-success btn-block">ADD</button>
        </form>
    </div>
    <div class="col-md-4"></div>
</div>

@if(session('success'))
        <div class="alert alert-success alert-dismissible notify">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i>Success Alert!</h4>
            {{ session('success') }}
        </div>
@endif

<div class="box">
    <div class="box-header">
        <h3 class="box-title">All Shop List</h3>
    </div>
</div>

<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <table id="example1" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example1_info" style="text-align: center;">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Shop ID</th>
                            <th style="text-align: center;">Shop Name</th>
                            <th style="text-align: center;">Due</th>
                            <th style="text-align: center;">Create Date</th>
                            <th style="text-align: center;">Edit</th>
                        </tr>
                    </thead>
                    	@foreach($shops as $shop)
                            @php $final_cheque = $final_due = $final_due_realize = 0; @endphp
	                    	<tr>
	                    		<td>{{ $shop->id }}</td>
	                    		<td>{{ $shop->shop_name }}</td>
                                <td>
                                    <!-- due -->
                                    @foreach($due as $each)
                                        @if($each->id == $shop->id)
                                            @php $final_due = $each->due; @endphp
                                        @endif
                                    @endforeach
                                    <!-- due realize -->
                                    @foreach($due_realize as $each)
                                        @if($each->id == $shop->id)
                                            @php $final_due_realize = $each->due_realize; @endphp
                                        @endif
                                    @endforeach
                                    <!-- cheque -->
                                    @foreach($cheque as $each)
                                        @if($each->id == $shop->id)
                                            @php $final_cheque = $each->cheque; @endphp
                                        @endif
                                    @endforeach
                                    {{ intval($final_due) - intval($final_due_realize) + intval($final_cheque) }}

                                </td>
	                    		<td>{{ Carbon\Carbon::parse($shop->created_at->toDateString())->format('d-M-Y') }}</td>
                                <td>
                                    <form action="{{ url('/shop/'.$shop->id) }}" method="post" accept-charset="utf-8">
                                        @csrf
                                        @method('PUT')
                                        <input type="text" name="shop_edit" class="form-control input-sm" style="border: 1px solid #00a65a;" value="{{ $shop->shop_name }}">
                                        <button type="submit" class="btn btn-success btn-sm">Update</button>
                                    </form>
                                </td>
	                    	</tr>            
	                    @endforeach
	                        
                    </tbody>
                            
                </table>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>




@endsection