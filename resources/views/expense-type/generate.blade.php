@extends('layouts.master')
@section('page_main_content')
<section class="content">
    <a href="{{ url('export') }}" class="btn btn-success">Download as Excel</a>
    {{-- Office type employee --}}
    <div class="row">
        <h1 class="text-center">Office Staff Employer</h1>
        <div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Designation</th>
                        <th>Total No. of employees</th>
                        <th>Salary</th>
                        <th>Total Salary</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i= 0;@endphp
                    @foreach($office_employee as $employee)
                    @php $i++;@endphp
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ App\Designation::find($employee->d)->name}}</td>
                        <td>{{ $employee->ind }}</td>
                        <td>{{ round($employee->total / $employee->ind) }}</td>
                        <td>{{ $employee->total }}</td>
                    </tr>
                    @endforeach
                    
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td><strong>{{ $office_employee_salary->total }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    {{-- Field Type employer --}}
    <div class="row">
        <h1 class="text-center">Field Team Salaries</h1>
        <div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Designation</th>
                        <th>Total No. of employees</th>
                        <th>Salary</th>
                        <th>Total Salary</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i= 0;@endphp
                    @foreach($field_employee as $employee)
                    @php $i++;@endphp
                    <tr>
                        <td>{{ $i }}</td>
                        <td>{{ App\Designation::find($employee->d)->name }}</td>
                        <td>{{ $employee->ind }}</td>
                        <td>{{ round($employee->total / $employee->ind) }}</td>
                        <td>{{ $employee->total }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td><strong>{{ $field_employee_salary->total }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    {{-- Convinience --}}
    <div class="row">
        <h1 class="text-center">Conveyance</h1>
        <div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Designation</th>
                        <th>No. of Days</th>
                        <th>Rate Per day </th>
                        <th>Total Cost </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($employees as $employee)
                    <tr>
                        <td>{{ $employee->id }}</td>
                        <td>{{ $employee->designation_name->name }}</td>
                        <td>{{ $work = $working_period - $employee->off_day['count'] + $employee->off_day_work['count'] - $employee->leave_day['count'] - $employee->absent['count'] }}</td>
                        <td>
                            {{ $employee->tada }}
                        </td>
                        <td>
                            {{ $work * $employee->tada }}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td><strong>{{ $con_total }}</strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    {{--  Vehicle Fuel Cost --}}
    <div class="row">
        <h1 class="text-center">Vehicle Fuel Cost</h1>
        <div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Vehicle No.</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($vehicle_fuel as $expense)
                    <tr>
                        <td>{{ $expense->id }}</td>
                        <td>{{ $expense->vehicle_no }}</td>
                        <td>{{ $expense->amount }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td><strong>
                            <?php
                            echo $vehicle_fuel->sum('amount');
                            ?>
                        </strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    
    {{--  Generator fule Cost --}}
    <div class="row">
        <h1 class="text-center">Generator Fuel Cost</h1>
        <div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Fuel Type</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($generator_fuel as $expense)
                    <tr>
                        <td>{{ $expense->id }}</td>
                        <td>{{ $expense->fuel_type }}</td>
                        <td>{{ $expense->amount }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td><strong>
                            {{ $generator_fuel->sum('amount') }}
                        </strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    {{--  Vehicle Maintainance Cost --}}
    <div class="row">
        <h1 class="text-center">Vehicle Maintainance Cost</h1>
        <div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Fuel Type</th>
                        <th>Purpose</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($vehicle_main as $expense)
                    <tr>
                        <td>{{ $expense->id }}</td>
                        <td>{{ $expense->fuel_type }}</td>
                        <td>{{ $expense->purpose }}</td>
                        <td>{{ $expense->amount }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td><strong>
                            <?php
                            echo $vehicle_main->sum('amount');
                            ?>
                        </strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    {{--  Furniture Maintainance Cost --}}
    <div class="row">
        <h1 class="text-center">Furniture Maintainance Cost</h1>
        <div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Total Furniture</th>
                        <th>Purpose</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($furniture_main as $expense)
                    <tr>
                        <td>{{ $expense->id }}</td>
                        <td>{{ $expense->vehicle_no }}</td>
                        <td>{{ $expense->purpose }}</td>
                        <td>{{ $expense->amount }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td><strong>
                            {{ $furniture_main->sum('amount') }}
                        </strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    {{--  Computer Maintainance Cost --}}
    <div class="row">
        <h1 class="text-center">Computer Maintainance Cost</h1>
        <div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Purpose</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($computer_main as $expense)
                    <tr>
                        <td>{{ $expense->id }}</td>
                        <td>{{ $expense->purpose }}</td>
                        <td>{{ $expense->amount }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td><strong>
                            {{ $computer_main->sum('amount') }}
                        </strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>

    {{--  Total Rent Cost --}}
    <div class="row">
        <h1 class="text-center">Total Rent</h1>
        <div id="example3_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example3_info">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Purpose</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($total_rent as $expense)
                    <tr>
                        <td>{{ $expense->id }}</td>
                        <td>{{ $expense->purpose }}</td>
                        <td>{{ $expense->amount }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td><strong>Total</strong></td>
                        <td><strong>
                            {{ $total_rent->sum('amount') }}
                        </strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
    localStorage.clear();
</script>
@endsection