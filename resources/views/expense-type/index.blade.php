@extends('layouts.master')
@section('page_main_content')
@php ksort($months) @endphp
<div class="msg"></div>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-primary">
    <div class="btn btn-success pull-right"><a href="{{ url('expense-type/create') }}">Add Expense Type</a></div>
    <h1>Generate Expense</h1>
    <div class="row">
        <div class="col-md-12">
        	<form action="{{ url('generate-expense-type') }}" method="post">
        		@csrf
            <div class="form-group col-sm-6">
                <label class="control-label">Month: </label>
                <div class="input-group col-md-12">
                    <div class="input-group-addon">
                        <select name="month" required class="form-control">
                            <option value="">Pick a Month</option>
                            @foreach($months as $number => $month)
                            <option value="{{ $number }}">{{ $month }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group col-sm-6">
                <label class="control-label">Year: </label>
                <div class="input-group col-md-12">
                    <div class="input-group-addon">
                        <input type="text" placeholder="Enter the year" class="form-control" name="year" required>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-group col-sm-6">
                    <button class="btn btn-info" type="submit">Generate</button>
                </div>
            </div>
        </div>
    </div>
    
</div>
		</div>
	</div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
    if (localStorage.getItem('msg')) {
        document.getElementById("demo").innerHTML = "<div class='alert alert-success'>"+ localStorage.getItem('msg'); +"</div>";
    }
</script>
@endsection
@section('style')
<style type="text/css">
    .btn.btn-success.pull-right {
    margin-top: 23px;
}
.btn.btn-success.pull-right a{color: #fff!important;}
</style>
@endsection