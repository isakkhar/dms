@extends('layouts.master')
@section('page_main_content')
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<expensetype :vehicles="{{ json_encode($vehicle_no) }}"></expensetype>
		</div>
	</div>
</section>
@endsection