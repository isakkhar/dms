<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon; 


class Employee extends Model
{
    protected $guarded = [];

    public function employee_expense()
    {
        return $this->hasOne('App\EmployeeExpense');
    }
    public function designation_name()
    {
        return $this->hasOne('App\Designation','id','designation');
    }
    public function cash_flow()
    {
        return $this->hasOne('App\CashFlow');
    }
    public function paid_tada()
    {
        return $this->hasOne('App\Tada');
    }
    public function attendance()
    {
        return $this->hasMany('App\Attendance');
    }
    public function provident()
    {
        return $this->hasMany('App\ProvidentFund');
    }
    
    public function employee()
    {
        return $this->hasMany('App\OffDay');
    }

    
    // results in a "problem", se examples below
    public function off_today() {
        return $this->hasMany('App\OffDay')->where('off_date', '=', Carbon::now()->format('l'));
    }
    public function count_off_day_duty()
    {
        return $this->hasOne('App\Attendance')->selectRaw("count(*) as count,employee_id")->where('off_day_work',1)->whereMonth('absent_date' ,date('m'))->groupBy('employee_id');
    }
    public function off_day_duty()
    {
        return $this->hasOne('App\Attendance')->selectRaw("count(*) as count,employee_id")->where('off_day_work',1)->groupBy('employee_id');
    }
    public function count_absent()
    {
        return $this->hasOne('App\Attendance')->selectRaw("count(*) as count,employee_id")->where('off_day_work',NULL)->groupBy('employee_id');
    }
    public function count_off_day()
    {
        return $this->hasOne('App\OffDay')->selectRaw("count(*) as count,employee_id")->whereYear('created_at', date('Y'))
                        ->whereMonth('created_at', date('m'))->groupBy('employee_id');
    }
    public function all_off_day()
    {
        return $this->hasOne('App\OffDay')->selectRaw("count(*) as count,employee_id")->groupBy('employee_id');
    }
    public function absent()
    {
        return $this->hasOne('App\Attendance')->selectRaw("count(*) as count,employee_id")->where('absent',1)->whereYear('created_at', date('Y'))->whereMonth('created_at', date('m'))->groupBy('employee_id');
    }
    public function off_day()
    {
        return $this->hasOne('App\Attendance')->selectRaw("count(*) as count,employee_id")->where('off_day',1)->whereYear('created_at', date('Y'))->whereMonth('created_at', date('m'))->groupBy('employee_id');
    }
    public function leave_day()
    {
        return $this->hasOne('App\Attendance')->selectRaw("count(*) as count,employee_id")->where('leave_day',1)->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->groupBy('employee_id');
    }
    public function off_day_work()
    {
        return $this->hasOne('App\Attendance')->selectRaw("count(*) as count,employee_id")->where('off_day_work',1)->whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->groupBy('employee_id');
    }
}
