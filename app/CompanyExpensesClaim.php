<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyExpensesClaim extends Model
{
    protected $table = "company_expenses_claim";
}
