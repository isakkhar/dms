<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestmentDetails extends Model
{
    protected $table = "investment_details";
}
