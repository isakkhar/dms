<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $guarded = [];
    public function group_salary()
    {
        return $this->hasOne('App\Employee')->selectRaw("count(*) as count,designation")->groupBy('designation');
    }
}
