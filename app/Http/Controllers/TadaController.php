<?php

namespace App\Http\Controllers;

use App\Employee;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DateTime;
class TadaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function PayTada(Request $request)
    {
    	if ($request['tada'] < 0) {
    		$cash_short = $request['tada'] * (-1);
    		$net_tada = 0;
    		$cash_flow = \App\CashFlow::where('employee_id' , $request['employee_id'])->update([
	    		'cash_short' => $cash_short
	    	]);
    	} else{
            $cash_flow = \App\CashFlow::where('employee_id' , $request['employee_id'])->update([
                'cash_short' => 0,
            ]);
    		$cash_short = 0;
    		$net_tada = $request['tada'];
    	}
    	$tada = \App\Tada::create([
    		'employee_id' => $request['employee_id'],
    		'start_date' => $request['start_date'],
    		'tada' => $net_tada,
    	]);
    }
    public function export()
    {
        $request = Session::get('request');
        // dd($request);
        if ($request['date'] == 'first') {
            $day1 = '01';
            $day2 = '15';
        } else {
            $day1 = '16';
            $day2 = cal_days_in_month(CAL_GREGORIAN, $request['month'], $request['year']);
        }
        if ($request['month'] > 9) {
            $month = $request['month'];
        } else {
            $month = '0'.$request['month'];
        }
        

        
        $from = date(Carbon::parse($month.'/'.$day1.'/'.$request['year'])->format('Y-m-d'));
        $to = date(Carbon::parse($month.'/'.$day2.'/'.$request['year'])->format('Y-m-d'));

        $datetime1 = new DateTime($from);
        $datetime2 = new DateTime($to);
        $interval = $datetime1->diff($datetime2);
        $data['working_period'] = $interval->days;


        $data['all_employees'] = Employee::with('off_day',"count_absent","cash_flow")->get();
        
        $data['employees'] = Employee::with([
            "absent" => function ($query) use ($from,$to)
            {
                $query->whereBetween('date',array($from,$to));
            },
            "off_day" => function($query) use  ($from,$to)
            {
                $query->whereBetween('date',array($from,$to));
                
            },
            "off_day_work" => function($query) use  ($from,$to)
            {
                $query->whereBetween('date',array($from,$to));
                
            },
            "leave_day" => function($query) use  ($from,$to)
            {
                $query->whereBetween('date',array($from,$to));
                
            },
            "cash_flow",
            "paid_tada" => function($query) use  ($from,$to)
            {
                $query->whereBetween('start_date',array($from,$to));
            },
        ])->get();
        $output ='';
        $output .= '
        <table border="1">
        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Employee Name</th>
                            <th>Designation</th>
                            <th>Total Present</th>
                            <th>Ta/Da </th>
                            <th>Gross Ta/Da </th>
                            <th>Cash Short </th>
                            <th>Net TADA </th>
                        </tr>
                    </thead>
                            
                    <tbody>';
            foreach($data['employees'] as $employee) {
                $cut =0;
                        if(strtotime($employee->join_date) >= strtotime($from) && strtotime($employee->join_date) <= strtotime($to)){
                            if (date('j', strtotime($from)) ==16) {
                                $cut= (int) date('j', strtotime($employee->join_date)) -15;
                            } else{
                                $cut= (int) date('j', strtotime($employee->join_date));
                            }
                        }else {
                            $cut =0;
                        }
                $output .= '
                        <tr>
                            <td>'.$employee->id .'</td>
                            <td>'.$employee->name .'</td>
                            <td>'.$employee->designation_name->name .'</td>
                            <td>'.$work = $data['working_period'] - $employee->off_day['count'] + $employee->off_day_work['count'] - $employee->leave_day['count'] - $employee->absent['count'] -$cut.'</td>
                            <td>
                                '.$employee->tada .'
                            </td>
                            <td>
                                '.$gross = ($work = $data['working_period'] - $employee->off_day['count'] + $employee->off_day_work['count'] - $employee->leave_day['count'] - $employee->absent['count']) * $employee->tada .'
                            </td>
                            <td>'.$employee->cash_flow->cash_short .'</td>';
                            // if($employee->paid_tada){
                            // $output .= '<td>'.$employee->paid_tada->tada.'</td>';
                            // } else {
                                
                                $output .= '<td>' . (($work = $data['working_period'] - $employee->off_day['count'] + $employee->off_day_work['count'] - $employee->leave_day['count'] - $employee->absent['count']) * $employee->tada - $employee->cash_flow->cash_short)   .'</td>';
                            // }
                            $output .= '</tr>';
                    }
            $output .= '</tbody></table>';
        header('content-type: application/xls');
        header('Content-Disposition: attachment; filename=tadas.xls');
        echo $output;
        Session::put('request',NULL);
    }
}
