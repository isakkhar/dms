<?php
namespace App\Http\Controllers;

use App\Attendance;
use App\Employee;
use App\LeaveDay;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AttendanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function AllAttendance()
    {
        $attendances = Attendance::all();
        return $attendances;   
    }
    public function AllOff()
    {
        $attendances = Attendance::where('off_day_work',1)->get();
        return $attendances;   
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['employees'] = Employee::with('attendance','absent','leave_day','off_day','off_day_work')->has('attendance')->get();
        return view('employees.attendance-list',$data);
    }

    public function exportAttendance()
    {
        $data['employees'] = Employee::with('attendance','absent','leave_day','off_day','off_day_work')->has('attendance')->get();
        $output  = '';
        $output .= '<table border="1">
            <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Designation</th>
                            <th>Off Day duty</th>
                            <th>Off Day </th>
                            <th>Present</th>
                            <th>Total Present</th>
                            <th>Leave</th>
                            <th>Absent Days</th>
                        </tr>
                    </thead>
                            
                    <tbody>';
                    foreach($data['employees'] as $employee) {
                        $p = (date('d') - $employee->off_day['count'] - $employee->leave['count'] - $employee->absent['count'] + $employee->off_day_work['count'] > date('d')) ? date('d') :date('d') - $employee->off_day['count'] - $employee->leave['count'] - $employee->absent['count'] + $employee->off_day_work['count'];
                        $t_p = (date('d') + $employee->off_day['count'] + $employee->leave['count'] - $employee->absent['count'] + $employee->off_day_work['count'] > date('d')) ? date('d') :date('d') + $employee->off_day['count'] + $employee->leave['count'] - $employee->absent['count'] + $employee->off_day_work['count'];
                        $output .= '<tr>
                            <td>'. $employee->id .'</td>
                            <td>'. $employee->name .'</td>
                            <td>'. $employee->designation_name->name .'</td>
                            <td>'. ($employee->off_day_work['count']) .'</td>
                            <td>'. $employee->off_day['count'] .'</td>
                            <td>'. $p .'</td>
                            <td>'. $t_p .'</td>
                            <td>'. $employee->leave_day['count'] .'</td>
                            <td>'. $employee->absent['count'] .'</td>
                        </tr>';
                    }
            $output .= '</tbody> </table>';
        header('content-type: application/xls');
        header('Content-Disposition: attachment; filename=attendance.xls');
        echo $output;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['employees'] = Employee::all();
        return view('employees.take-attendance' ,$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

            foreach ($request->actions as $key => $action) {
                $find = Attendance::query()
                ->when($action, function ($query, $action) {
                    return $query->where('date', date('Y-m-d'));
                })
                ->when($action, function ($query, $action) {
                    return $query->where('employee_id', $action['id']);
                })
                ->get();
                if(!$find->isEmpty()){
                    $attendance = Attendance::where('employee_id',$action['id'])
                    ->delete();
                    $attendance2 = Attendance::create([
                        'employee_id' => $action['id'],
                        $action['action'] => 1,
                        'date' => date('Y-m-d')
                    ]);

                } else {
                    // create new attendance
                    $attendance2 = Attendance::create([
                        'employee_id' => $action['id'],
                        $action['action'] => 1,
                        'date' => date('Y-m-d')
                    ]);
                }
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function show(Attendance $attendance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function edit(Attendance $attendance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attendance $attendance)
    {
        //
    }
    public function addLeave(Request $request)
    {
        LeaveDay::create([
            'employee_id' => $request->id,
            'leave_date' => Carbon::now()->toDateString(),
        ]);
        return ['Leave day added.',200];
    }
    public function getLeave(Request $request)
    {
        $leave_day = LeaveDay::where('leave_date',Carbon::now()->toDateString())->get();
        return $leave_day;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendance  $attendance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attendance $attendance)
    {
        //
    }
    public function editAttendance()
    {
        return view('employees.attendance-edit');
    }
    public function updateAttendance(Request $request)
    {
        $attendance = Attendance::where('date',$request->date)->first();
        $data['date'] = $request->date;
        $data['employees'] = Employee::all();
        return view('employees.attendance-single',$data);
    }
}
