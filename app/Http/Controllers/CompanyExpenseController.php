<?php

namespace App\Http\Controllers;

use App\CompanyExpense;
use App\CompanyExpensesClaim;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

class CompanyExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('company-expense.index');
    }
    public function list(Request $request)
    {   
        $from = $request->from_date;
        $to = $request->to_date;
        $data['companyExpenses'] = CompanyExpense::select('company_expenses.*', 'c_claim.amount as claim_pass')
                                   ->whereDate('company_expenses.created_at', '>=', $from)
                                   ->whereDate('company_expenses.created_at', '<=', $to)
                                   ->leftJoin('company_expenses_claim as c_claim', 'company_expenses.id', 'c_claim.company_expenses_id')
                                   ->get();
                                   
        return view('company-expense.list', $data, compact('from', 'to'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $company_expenses = CompanyExpense::where('amount', '>', 0)->get();
        return view('company-expense.add', compact('company_expenses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'expenses_type' => 'required',
            'amount' => 'numeric|required',
        ]);

        if ($request->expenses_type == 'claim' && empty($request->claim_pass))
        {
            $companyExpense = new CompanyExpense;
            $companyExpense->name = $request->name;
            $companyExpense->amount = $request->amount;
            $companyExpense->save();
            $request->session()->flash('msg', 'Company expense added Successfully!');
        }
        else
        {
            $claim_expenses = new CompanyExpensesClaim;
            $claim_expenses->company_expenses_id = $request->claim_pass;
            $claim_expenses->amount = $request->amount;
            $claim_expenses->save();
            //decrement passed amount from company expenses
            CompanyExpense::find($request->claim_pass)->decrement('amount', $request->amount);
            $request->session()->flash('msg', 'Claim Passed Successfully!');
        }

        return redirect()->route('company-expense.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CompanyExpense  $companyExpense
     * @return \Illuminate\Http\Response
     */
    public function show(CompanyExpense $companyExpense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CompanyExpense  $companyExpense
     * @return \Illuminate\Http\Response
     */
    public function edit(CompanyExpense $companyExpense)
    {
        $id = $companyExpense;
        $expense = CompanyExpense::findOrFail($id)->first();
        if($expense){
            return view('company-expense.edit')->with('expense' , $expense);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CompanyExpense  $companyExpense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CompanyExpense $companyExpense)
    {
        $request->validate([
            'name' => 'required',
            'amount' => 'numeric|required',
        ]);

        $id = $companyExpense;
        $find_expense = CompanyExpense::findOrFail($id)->first();
        $expense = $find_expense->update([
            'name' => $request->name,
            'amount' => $request->amount,
        ]);

        if ($expense) {
            $request->session()->flash('msg', 'Expense\'s Information Updated Successfully!');
            return redirect()->route('company-expense.index');
        } else{
            return redirect()->back();
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CompanyExpense  $companyExpense
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompanyExpense $companyExpense)
    {
        $expense = CompanyExpense::find($companyExpense)->first();
        $expense->delete();
        return "Expense deleted Successfully!";
    }
}
