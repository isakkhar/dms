<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use DateTime;
use App\OfficeExpense;
use Illuminate\Http\Request;

class OfficeExpenseController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('office-expense.index');
    }
    public function list(Request $request)
    {   
        $from = $request->from_date;
        $to = $request->to_date;
        $data['officeExpenses'] = OfficeExpense::whereDate('office_expenses.created_at', '>=', $from)
                                   ->whereDate('office_expenses.created_at', '<=', $to)
                                   ->get();
        return view('office-expense.list', $data, compact('from', 'to'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('office-expense.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            // 'name' => 'required',
            'amount' => 'numeric|required',
        ]);


        $expense = OfficeExpense::create($request->except(['_token']));
        if ($expense) {
            $request->session()->flash('msg', 'Office expense added Successfully!');
            return redirect()->route('office-expense.index');
        } else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OfficeExpense  $officeExpense
     * @return \Illuminate\Http\Response
     */
    public function show(OfficeExpense $officeExpense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OfficeExpense  $officeExpense
     * @return \Illuminate\Http\Response
     */
    public function edit(OfficeExpense $officeExpense)
    {
        $id = $officeExpense;
        $expense = OfficeExpense::findOrFail($id)->first();
        if($expense){
            return view('office-expense.edit')->with('expense' , $expense);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OfficeExpense  $officeExpense
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OfficeExpense $officeExpense)
    {
        $request->validate([
            'name' => 'required',
            'amount' => 'numeric|required',
        ]);

        $id = $officeExpense;
        $find_expense = OfficeExpense::findOrFail($id)->first();
        $expense = $find_expense->update([
            'name' => $request->name,
            'amount' => $request->amount,
        ]);

        if ($expense) {
            $request->session()->flash('msg', 'Expense\'s Information Updated Successfully!');
            return redirect()->route('office-expense.index');
        } else{
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OfficeExpense  $officeExpense
     * @return \Illuminate\Http\Response
     */
    public function destroy(OfficeExpense $officeExpense)
    {
        $expense = OfficeExpense::find($officeExpense)->first();
        $expense->delete();
        return "Expense deleted Successfully!";
    }
}
