<?php

namespace App\Http\Controllers;

use App\Sale_value;
use App\Due;
use App\DueRealize;
use App\Cheque;
use Illuminate\Http\Request;
use Carbon\Carbon;

class RegisterController extends Controller
{
    public function index()
    {	
    	$today = Carbon::today()->format('y-m-d');

    	$sales_details = Sale_value::whereDate('sale_values.created_at', $today)
    							->orderBy('sale_values.id')
    							->get();
		

    	$data = array();
    	$due_details = array();
    	$i = 0;
    	foreach ($sales_details as $key => $value) 
    	{
    		$due = Due::where('sales_id', $value->id)->sum('amount');
    		$due_realizes_cheque = DueRealize::where('sales_id', $value->id)->where('payment_type', 'cheque')->sum('amount');
    		$due_realizes_cash = DueRealize::where('sales_id', $value->id)->where('payment_type', 'cash')->sum('amount');
    		$cheque_payment = Cheque::where('sales_id', $value->id)->where('type', 'payment')->sum('amount');





    		$data[$i]['sales'] = $value->cash_issue;
    		$data[$i]['expenses'] = $value->free + $value->discount + $value->promo_discount + $value->credit_given + $value->prev_credit_collected + $value->expenses + $value->market_return + $value->store_discount;
    		$data[$i]['cheque_due'] = $due + $cheque_payment;
    		$data[$i]['due_realizes_cheque'] = intval($due_realizes_cheque);
    		$data[$i]['due_realizes_cash'] = intval($due_realizes_cash);
    		$data[$i]['cash'] = $value->cash_to_be_deposite - intval($due_realizes_cash);
    		$data[$i]['diff'] = $data[$i]['sales'] - $data[$i]['expenses'] - $data[$i]['cheque_due'] - $data[$i]['cash'];

			$i++;

    		$due_details[] = Due::select('dues.amount', 'shops.shop_name')
    							  ->leftJoin('shops', 'dues.shop_id', 'shops.id')
    							  ->where('sales_id', $value->id)
    							  ->get();
    		
    		$due_realize[] = DueRealize::select('due_realizes.amount', 'shops.shop_name')
    							  ->leftJoin('shops', 'due_realizes.shop_id', 'shops.id')
    							  ->where('sales_id', $value->id)
    							  ->get();

    		$cheque_details[] = Cheque::select('cheques.amount', 'shops.shop_name')
    							  ->leftJoin('shops', 'cheques.shop_id', 'shops.id')
    							  ->where('sales_id', $value->id)
    							  ->get();

    	}
    	return view('register.index', compact('data', 'due_details', 'due_realize', 'cheque_details'));

    }
}
