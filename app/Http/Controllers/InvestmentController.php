<?php

namespace App\Http\Controllers;

use App\Investment;
use App\InvestmentDetails;
use Illuminate\Http\Request;

class InvestmentController extends Controller
{
	public function index()
	{	
		/*
			MV = Mechanized Vehicle
			NMV = Non Mechanized Vehicle
			CE = Computer Equipment
			OFE= Office Furniture and Other Equipment
		*/
		$MV = Investment::select('investments.id', 'investments.name as name', 'investments.life_in_year', 'investments.life_in_month', 'investment_details.name as particuler', 'investment_details.puchase_date', 'investment_details.unit_price', 'investment_details.no_of_unit', 'investment_details.total_cost', 'investment_details.month_in_use', 'investment_details.monthly_depcreciation', 'investment_details.depreciation', 'investment_details.balance_remain')
						->where('investments.name', 1)
						->join('investment_details', 'investments.name', '=', 'investment_details.investment_id')
						->get();

		$NMV = Investment::select('investments.id', 'investments.name as name', 'investments.life_in_year', 'investments.life_in_month', 'investment_details.name as particuler', 'investment_details.puchase_date', 'investment_details.unit_price', 'investment_details.no_of_unit', 'investment_details.total_cost', 'investment_details.month_in_use', 'investment_details.monthly_depcreciation', 'investment_details.depreciation', 'investment_details.balance_remain')
						->where('investments.name', 2)
						->join('investment_details', 'investments.name', '=', 'investment_details.investment_id')
						->get();

		$CE = Investment::select('investments.id', 'investments.name as name', 'investments.life_in_year', 'investments.life_in_month', 'investment_details.name as particuler', 'investment_details.puchase_date', 'investment_details.unit_price', 'investment_details.no_of_unit', 'investment_details.total_cost', 'investment_details.month_in_use', 'investment_details.monthly_depcreciation', 'investment_details.depreciation', 'investment_details.balance_remain')						
						->where('investments.name', 3)
						->join('investment_details', 'investments.name', '=', 'investment_details.investment_id')
						->get();

		$OFE = Investment::select('investments.id', 'investments.name as name', 'investments.life_in_year', 'investments.life_in_month', 'investment_details.name as particuler', 'investment_details.puchase_date', 'investment_details.unit_price', 'investment_details.no_of_unit', 'investment_details.total_cost', 'investment_details.month_in_use', 'investment_details.monthly_depcreciation', 'investment_details.depreciation', 'investment_details.balance_remain')
						->where('investments.name', 4)
						->join('investment_details', 'investments.name', '=', 'investment_details.investment_id')
						->get();

		$tabs = Investment::select('investments.id', 'investments.name as name', 'investments.life_in_year', 'investments.life_in_month', 'investment_details.name as particuler', 'investment_details.puchase_date', 'investment_details.unit_price', 'investment_details.no_of_unit', 'investment_details.total_cost', 'investment_details.month_in_use', 'investment_details.monthly_depcreciation', 'investment_details.depreciation', 'investment_details.balance_remain')
						->where('investments.name', 5)
						->join('investment_details', 'investments.name', '=', 'investment_details.investment_id')
						->get();

		return view('roi.investments', compact('MV', 'NMV', 'CE', 'OFE', 'tabs'));
	}

	public function create()
	{
		return view('roi.create_investment');
	}

	public function store(Request $request)
	{
		$request->validate([
		    'name' => 'required',
		    'year' => 'numeric|required',
		]);

		$investment = new Investment;
		$investment->name = $request->name;
		$investment->life_in_year = $request->year;
		$investment->life_in_month = $request->year * 12;
		$investment->save();

		return redirect('/add-investment')->with('success', 'Successfully Added!');
	}

	public function create_details()
	{	
		$investments = Investment::all();
		return view('roi.create_investments_details', compact('investments'));
	}

	public function store_details(Request $request)
	{
		$request->validate([
			'investment' => 'required',
			'name' => 'required',
			'date' => 'required',
			'price' => 'numeric|required',
			'no_of_unit' => 'numeric|required',
			'used_month' => 'numeric|required',
		]);

		$investment = Investment::find($request->investment);

		$total_cost = $request->price * $request->no_of_unit;
		$monthly_depcreciation = $total_cost / $investment->life_in_month;
		$depreciation = $monthly_depcreciation * $request->used_month;
		$balance_remain = ($total_cost > $depreciation) ? ($total_cost - $depreciation) : 0;

		$investment_details = new InvestmentDetails;
		$investment_details->investment_id = $request->investment;
		$investment_details->name = $request->name;
		$investment_details->puchase_date = $request->date;
		$investment_details->unit_price = $request->price;
		$investment_details->no_of_unit = $request->no_of_unit;
		$investment_details->total_cost = $total_cost;
		$investment_details->month_in_use = $request->used_month;
		$investment_details->monthly_depcreciation = $monthly_depcreciation;
		$investment_details->depreciation = $depreciation;
		$investment_details->balance_remain = $balance_remain;
		$investment_details->save();

		return redirect('/add-investment-details')->with('success', 'Successfully Added!');
	}
}
