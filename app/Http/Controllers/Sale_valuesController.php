<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sale_value;
use App\Cash_flow;

class Sale_valuesController extends Controller
{
    public function edit($id)
    {
    	$sales_info = Sale_value::find($id);
    	return view('sales.edit', compact('sales_info'));
    }

    public function update(Request $request, $id)
    {
    	$sale = Sale_value::find($id);

    	$cash_to_deposite = $sale->cash_to_be_deposite;
    	$new_cash_deposite = $request->cash_deposite;

    	$cash_flow = Cash_flow::where('employee_id', $sale->employee_id)->first();
    	if ($sale->cash_short > 0) 
    	{
    		$cash_flow->decrement('cash_short', $sale->cash_short);
    	}
    	if ($sale->cash_excess > 0) 
    	{
    		$cash_flow->decrement('cash_excess', $sale->cash_excess);
    	}


    	$sale->cash_deposited = $new_cash_deposite;
    	if ($cash_to_deposite > $new_cash_deposite) 
    	{
    		$cash_short = $cash_to_deposite - $new_cash_deposite;
    		$sale->cash_short = $cash_short;
    		$sale->cash_excess = 0;
    		$cash_flow->increment('cash_short', $cash_short);
    	}
    	else
    	{
    		$cash_excess = $new_cash_deposite - $cash_to_deposite;
    		$sale->cash_excess = $cash_excess;
    		$sale->cash_short = 0;
    		$cash_flow->increment('cash_excess', $cash_excess);
    	}
    	$sale->save();

    	return redirect()->back()->with('success', 'Sales Successfully Updated!');
    }
}
