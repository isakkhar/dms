<?php

namespace App\Http\Controllers;

use App\Cash_flow;
use App\Cheque;
use App\CompanyExpense;
use App\Due;
use App\DueRealize;
use App\Employee;
use App\Employee_exchange;
use App\Imports\SalesImport;
use App\Sale;
use App\Sale_value;
use App\Shop;
use App\Stock;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SalesController extends Controller
{	
    public function __construct()
    {
        $this->middleware('auth');
    }
	public function index()
    {
        $sales = Sale_value::leftJoin('employees', function($join) {
                      $join->on('sale_values.employee_id', '=', 'employees.id');
                    })
                ->select('sale_values.*', 'employees.name', 'employees.id as employee_id')
                ->orderBy('sale_values.id', 'DESC')
                ->get();
		return view('sales.index', compact('sales'));
	}

	public function create()
    {
		$employee_data = Employee::all();
        $shops = Shop::all();
		return view('sales.add', compact("employee_data", "shops"));
	}

	//imports excel 
    public function import(Request $request)
    {      
        $array = (new SalesImport)->toArray(request()->file('excel_file'));
        //insert sales list
        for ($i = 12; $i < count($array[0]); $i++) 
        {
        	if ($array[0][$i][1] !== "Total") 
            {   
        		$sale = new Sale();
        		$sale->employee_id = $request->employee;
        		$sale->sku_code = $array[0][$i][1];
        		$sale->sku = $array[0][$i][2];
        		$sale->pack_size = $array[0][$i][5];
        		$sale->ctn_sale = $array[0][$i][9];
        		$sale->unit_sale = $array[0][$i][11];
        		$sale->ctn_free = $array[0][$i][14];
        		$sale->unit_free = $array[0][$i][16];
        		$sale->ctn_return = $array[0][$i][17];
        		$sale->unit_return = $array[0][$i][18];
        		$sale->sales_value = $array[0][$i][23];
                $sale->created_at = $request->date;
        		$sale->save();

                //decrement from stock
                $total_sale_ctn = $array[0][$i][9] + $array[0][$i][14];
                $total_sale_unit = $array[0][$i][11] + $array[0][$i][16];

                $stocks = Stock::where('sku_code', $array[0][$i][1])->count();
                if ($stocks > 0) 
                {
                    Stock::where('sku_code', $array[0][$i][1])->decrement('available_ctn', $total_sale_ctn);
                    Stock::where('sku_code', $array[0][$i][1])->decrement('available_pcs', $total_sale_unit);
                }
        	}
        	else
            {
        		break;
        	}  	
        } //end for

        //insert sales values
        for ($i = 12; $i < count($array[0]); $i++) 
        {

        	if ($array[0][$i][1] == "Free Value                      :")
            {

        		$credit_given = (float) filter_var( $array[0][$i+17][4], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
        		$prev_credit_collected = (float) filter_var( $array[0][$i+21][4], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
        		$expenses = (float) filter_var( $array[0][$i+25][4], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
        		$store_discount = (float) filter_var( $array[0][$i+33][4], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
        		$free = $array[0][$i+1][4];
        		$discount = $array[0][$i+5][4];
        		$promo_discount = $array[0][$i+9][4];
        		$market_return = $array[0][$i+29][4];
        		$cash_to_be_deposite = $array[0][$i+37][4];
                $cash_deposite = $request->cash_deposite;

        		$cash_issue = ($free + $discount + $promo_discount + $credit_given +  $prev_credit_collected + $expenses + $market_return + $store_discount + $cash_to_be_deposite);

        		$sale_value = new Sale_value();
        		$sale_value->employee_id = $request->employee;
        		$sale_value->cash_issue = $cash_issue;
        		$sale_value->free = $free;
        		$sale_value->discount = $discount;
        		$sale_value->promo_discount = $promo_discount;
        		$sale_value->credit_given = $credit_given;
        		$sale_value->prev_credit_collected = $prev_credit_collected;
        		$sale_value->expenses = $expenses;
        		$sale_value->market_return = $market_return;
        		$sale_value->store_discount = $store_discount;
                $sale_value->cash_to_be_deposite = $cash_to_be_deposite;
        		$sale_value->cash_deposited = $cash_deposite;
                $sale_value->created_at = $request->date;
        		$sale_value->save();
        	}
       	}//end for


        //shoppers
        $total_due = $total_due_realize = $total_cheque_amount = 0;

        //due
        $due_shop = $request->shop_due;
        $due_amount = $request->due_amount;

        $sales = Sale_value::select('id')
                           ->where('cash_to_be_deposite', $cash_to_be_deposite)
                           ->where('cash_deposited', $cash_deposite)
                           ->where('employee_id', $request->employee)
                           ->where('cash_issue', $cash_issue)
                           ->first();

        if ($request->has('shop_due') && $request->has('due_amount')) 
        {   
            for ($i=0; $i < count($due_amount); $i++) 
            { 
                $dues = new Due;
                $dues->shop_id = $due_shop[$i];
                $dues->sales_id = $sales->id;
                $dues->amount = $due_amount[$i];
                $dues->save();
                $total_due += $due_amount[$i];
            }
        }

        //cheque payment
        $shop_cheque_pay = $request->shop_cheque_pay;
        $cheque_amount = $request->cheque_amount;
        $cheque_number = $request->cheque_number;
        $deposite_date = $request->deposite_date;

        if ($request->has('shop_cheque_pay') && $request->has('cheque_amount') && $request->has('cheque_number') && $request->has('deposite_date')) 
        {   
            for ($i=0; $i < count($cheque_amount); $i++) 
            { 
                $cheques = new Cheque;
                $cheques->shop_id = $shop_cheque_pay[$i];
                $cheques->sales_id = $sales->id;
                $cheques->number = $cheque_number[$i];
                $cheques->amount = $cheque_amount[$i];
                $cheques->received_date = $request->date;
                $cheques->deposite_date = $deposite_date[$i];
                $cheques->type = 'payment';
                $cheques->save();
                $total_cheque_amount += $cheque_amount[$i];
            }
        }

        //due realize
        $shop_due_realize = $request->shop_due_realize;
        $due_realize_amount = $request->amount;
        $payment_type = $request->payment_type;
        $due_realize_cheque_number = $request->due_realize_cheque_number;
        $deposite_date_due_realize = $request->deposite_date_due_realize;

        if ($request->has('shop_due_realize') && $request->has('amount') && $request->has('payment_type') && $request->has('due_realize_cheque_number') && $request->has('deposite_date_due_realize')) 
        {   
            for ($i=0; $i < count($due_realize_amount); $i++) 
            { 
                $due_realizes = new DueRealize;
                $due_realizes->shop_id = $shop_due_realize[$i];
                $due_realizes->sales_id = $sales->id;
                $due_realizes->amount = $due_realize_amount[$i];
                $due_realizes->payment_type = $payment_type[$i];

                if ($payment_type[$i] == 'cheque') {
                    $cheques = new Cheque;
                    $cheques->shop_id = $shop_due_realize[$i];
                    $cheques->sales_id = $sales->id;
                    $cheques->number = $due_realize_cheque_number[$i];
                    $cheques->amount = $due_realize_amount[$i];
                    $cheques->received_date = $request->date;
                    $cheques->deposite_date = $deposite_date_due_realize[$i];
                    $cheques->type = 'due_realize';
                    $cheques->save();
                }else {
                    $total_due_realize += $due_realize_amount[$i];
                }

                $cheque_no = Cheque::where('number', $due_realize_cheque_number[$i])->first();

                (!empty($cheque_no)) ? $due_realizes->cheque_id = $cheque_no->id : '';
                $due_realizes->save();
                
            }
        }

        //final cash to be deposite
        $final_cash_to_be_deposite = $cash_to_be_deposite - $total_due - $total_cheque_amount + $total_due_realize;
        

        if ($cash_deposite > $final_cash_to_be_deposite) 
        {
            $cash_excess =  $cash_deposite - $final_cash_to_be_deposite;
            Sale_value::find($sales->id)->update([
                        'cash_to_be_deposite' => $cash_excess,
                        'cash_excess' => $cash_excess,
                    ]);
            Cash_flow::where('employee_id', $request->employee)->increment('cash_excess', round($cash_excess));
        }
        else
        {
            $cash_short = $final_cash_to_be_deposite - $cash_deposite;
            Sale_value::find($sales->id)->update([
                        'cash_to_be_deposite' => $final_cash_to_be_deposite,
                        'cash_short' => $cash_short,
                    ]);
            Cash_flow::where('employee_id', $request->employee)->increment('cash_short', round($cash_short));
        }
		
        return back()->with('success', 'Sales Excel Sheet is Successfully Imported');
    }

    public function sales_details(sale_value $sale_value, $id)
    {   
        $sales_value_details = Sale_value::where('id', $id)
                                    ->where('employee_id', request()->employee)
                                    ->whereDate('created_at', request()->date)
                                    ->first();

        $sales_details = Sale::where('employee_id', request()->employee)
                                    ->whereDate('created_at', request()->date)
                                    ->get();
        $employee = Employee::where('id', request()->employee)->first();

        return view('sales.sales_details', compact('sales_value_details', 'sales_details', 'employee'));
    }

    public function sales_report(Request $request)
    {   
        if ($request->method() == 'GET')
        {
            return view('sales.sales_report');
        } 
        else
        {   
            $from = $request->from;
            $to = $request->to;

            $sales_details = Sale_value::whereDate('created_at', '>=', $from)
                                       ->whereDate('created_at', '<=', $to)
                                       ->get(
                                              array(
                                                DB::raw('SUM(discount) as discount'),
                                                DB::raw('SUM(free) as free'),
                                                DB::raw('SUM(cash_issue) as cash_issue'),
                                                DB::raw('SUM(cash_to_be_deposite) as cash_to_be_deposite'),
                                                DB::raw('SUM(cash_deposited) as cash_deposited'),
                                                DB::raw('SUM(cash_short) as cash_short'),
                                                DB::raw('SUM(free) as free'),
                                                DB::raw('SUM(promo_discount) as promo_discount'),
                                                DB::raw('SUM(credit_given) as credit_given'),
                                                DB::raw('SUM(prev_credit_collected) as prev_credit_collected'),
                                                DB::raw('SUM(expenses) as expenses'),
                                                DB::raw('SUM(market_return) as market_return'),
                                                DB::raw('SUM(store_discount) as store_discount'),
                                              )
                                            );

            return view('sales.sales_report', compact('sales_details'));
        }   
        
    }

}
