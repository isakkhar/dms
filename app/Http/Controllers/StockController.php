<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\StocksImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Stock;


class StockController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {   
        $stocks = Stock::all();
    	return view('stocks.index', compact('stocks'));
    }

    public function create()
    {
    	return view('stocks.add');
    }

    public function import(Request $request)
    {   
    	$array = (new StocksImport)->toArray(request()->file('excel_file')); 

    	for ($i = 8; $i < count($array[0]); $i++) 
    	{
    		if ($array[0][$i][0] != null) 
            {   
                $available_stocks = Stock::where('sku_code', $array[0][$i][0])->get();
                if (count($available_stocks) > 0) 
                {
                    Stock::where('sku_code', $array[0][$i][0])->increment('available_ctn', $array[0][$i][3]);
                    Stock::where('sku_code', $array[0][$i][0])->increment('available_pcs', $array[0][$i][4]);
                }
                else
                {
                    $stocks = new Stock();
                    $stocks->sku_code = $array[0][$i][0];
                    $stocks->sku_name = $array[0][$i][1];
                    $stocks->pack_size = $array[0][$i][2];
                    $stocks->available_ctn = $array[0][$i][3];
                    $stocks->available_pcs = $array[0][$i][4];
                    $stocks->save();
                }
            }
    	}

        return redirect('/stocks-list')->with('success', 'Stocks Sheet is Successfully Updated');
    }
}
