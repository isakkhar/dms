<?php

namespace App\Http\Controllers;

use App\LeaveDay;
use Illuminate\Http\Request;

class LeaveDayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\LeaveDay  $leaveDay
     * @return \Illuminate\Http\Response
     */
    public function show(LeaveDay $leaveDay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\LeaveDay  $leaveDay
     * @return \Illuminate\Http\Response
     */
    public function edit(LeaveDay $leaveDay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\LeaveDay  $leaveDay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveDay $leaveDay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\LeaveDay  $leaveDay
     * @return \Illuminate\Http\Response
     */
    public function destroy(LeaveDay $leaveDay)
    {
        //
    }
}
