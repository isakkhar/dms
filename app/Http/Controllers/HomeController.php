<?php

namespace App\Http\Controllers;

use App\Attendance;
use App\Cash_flow;
use App\CompanyExpense;
use App\Employee;
use App\EmployeeExpense;
use App\OfficeExpense;
use App\Sale;
use App\Stock;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $stocks = Stock::all()->count();
        $employee_expenses = EmployeeExpense::sum('expenses_amount');
        $employees = Employee::all()->count();
        $users = User::all()->count();
        $cash_short = Cash_flow::sum('cash_short');
        $cash_excess = Cash_flow::sum('cash_excess');
        $sales = Sale::all()->count();
        $company_expenses = CompanyExpense::sum('amount');
        $office_expenses = OfficeExpense::sum('amount');
        $absent_days = Attendance::all()->count();   

        return view('dashboard.index', compact('stocks', 'employee_expenses', 'employees', 'users', 'cash_short', 'cash_excess', 'sales', 'company_expenses', 'office_expenses', 'absent_days'));
    }
}
