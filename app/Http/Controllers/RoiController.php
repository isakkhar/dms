<?php

namespace App\Http\Controllers;

use App\CompanyExpense;
use App\Employee;
use App\ExpenseType;
use App\InvestmentDetails;
use App\OfficeExpense;
use App\OtherInfo;
use App\Sale_value;
use App\Investment;
use DB;
use Illuminate\Http\Request;

class RoiController extends Controller
{	
	public function index()
	{
		return view('roi.index');
	}

	public function details(Request $request)
	{	
		$request->validate([
			'month' => 'required',
			'year' => 'numeric|required'
		]);

		//sales
		$month = $request->month;
		$year = $request->year;

		$sales = array();
		$sales['total_sales'] = Sale_value::whereMonth('created_at', $month)
								 ->whereYear('created_at', $year)
								 ->sum('cash_to_be_deposite');

		$sales['total_free'] = Sale_value::whereMonth('created_at', $month)
								 ->whereYear('created_at', $year)
								 ->sum('free');

		$sales['total_earn'] = $sales['total_sales'] - $sales['total_free'];
		$sales['regular_list_price'] = round(($sales['total_earn'] * 4.67)/100);

		//Deprecation
		$dep = array();
		$dep['vehicle_dep'] = InvestmentDetails::whereIn('investment_id', [1, 2])->sum('monthly_depcreciation');
		$dep['computer_dep'] = InvestmentDetails::where('investment_id', 3)->sum('monthly_depcreciation');
		$dep['furniture_dep'] = InvestmentDetails::where('investment_id', 4)->sum('monthly_depcreciation');
		$dep['tabs_dep'] = InvestmentDetails::where('investment_id', 5)->sum('monthly_depcreciation');

		$dep['total'] = $dep['vehicle_dep'] + $dep['computer_dep'] + $dep['furniture_dep'] + $dep['tabs_dep'];

		//office staff salary
		$salary = array();
		$salary['office'] = Employee::select('designations.name AS designation', DB::raw('SUM(salary) AS salary'))
								 ->where('employee_type', 1)
								 ->leftJoin('designations', 'employees.designation', 'designations.id')
								 ->groupBy('employees.designation')
								 ->get();
		//field staff salary
		$salary['field'] = Employee::select('designations.name AS designation', DB::raw('SUM(salary) AS salary'))
								 ->where('employee_type', 2)
								 ->leftJoin('designations', 'employees.designation', 'designations.id')
								 ->groupBy('employees.designation')
								 ->get();

		//vehicle cost
		$vehicle = array();
		$vehicle['fuel'] = ExpenseType::whereMonth('created_at', $month)
									  ->whereYear('created_at', $year)
									  ->where('type', 1)
									  ->sum('amount');
		$vehicle['maintenance'] = ExpenseType::whereMonth('created_at', $month)
									  ->whereYear('created_at', $year)
									  ->where('type', 3)
									  ->sum('amount');
		$vehicle['total'] = $vehicle['fuel'] + $vehicle['maintenance'];

		//rent
		$rent = ExpenseType::whereMonth('created_at', $month)
									  ->whereYear('created_at', $year)
									  ->where('type', 6)
									  ->sum('amount');

		//offfice_expenses
		$offfice_expenses = array();
		$offfice_expenses['generator'] = ExpenseType::whereMonth('created_at', $month)
									  ->whereYear('created_at', $year)
									  ->where('type', 2)
									  ->sum('amount');
		$offfice_expenses['computer'] = ExpenseType::whereMonth('created_at', $month)
									  ->whereYear('created_at', $year)
									  ->where('type', 4)
									  ->sum('amount');
		$offfice_expenses['furniture'] = ExpenseType::whereMonth('created_at', $month)
									  ->whereYear('created_at', $year)
									  ->where('type', 5)
									  ->sum('amount');
        $offfice_expenses['other'] = OfficeExpense::whereMonth('created_at', $month)
									  ->whereYear('created_at', $year)
									  ->sum('amount');

		$offfice_expenses['total'] = $offfice_expenses['generator'] + $offfice_expenses['computer'] + $offfice_expenses['furniture'] + $offfice_expenses['other'];

		//investment
		$investment = array();
		$investment['investment'] = OtherInfo::where('info_id', 1)
											 ->whereMonth('created_at', $month)
											 ->whereYear('created_at', $year)
											 ->sum('amount');
		$investment['credit_to_trade'] = OtherInfo::where('info_id', 2)
												  ->whereMonth('created_at', $month)
												  ->whereYear('created_at', $year)
												  ->sum('amount');
		$investment['income_tax'] = ($sales['regular_list_price'] * 5)/100;
		$investment['cash'] = round($sales['total_sales'] / 30);
		$investment['pending_claims'] = $this->pending_claim_ubl();
		$investment['tab'] = Investment::where('investments.name', 5)
						->join('investment_details', 'investments.name', '=', 'investment_details.investment_id')
						->sum('balance_remain');
		$investment['computer'] = Investment::where('investments.name', 3)
						->join('investment_details', 'investments.name', '=', 'investment_details.investment_id')
						->sum('balance_remain');
		$investment['furniture'] = Investment::where('investments.name', 4)
						->join('investment_details', 'investments.name', '=', 'investment_details.investment_id')
						->sum('balance_remain');
		$investment['nmv'] = Investment::where('investments.name', 2)
						->join('investment_details', 'investments.name', '=', 'investment_details.investment_id')
						->sum('balance_remain');
		$investment['mv'] = Investment::where('investments.name', 1)
						->join('investment_details', 'investments.name', '=', 'investment_details.investment_id')
						->sum('balance_remain');
		$investment['vehicle'] = $investment['nmv'] + $investment['mv'];

		$investment['total'] = $investment['investment']+$investment['credit_to_trade']+$investment['income_tax']+$investment['cash']+$investment['pending_claims']+$investment['tab']+$investment['computer']+$investment['furniture']+$investment['vehicle'];

		//redistribution cost
		$redistribution = array();
		$redistribution['load_unload'] = OtherInfo::where('info_id', 3)
												  ->whereMonth('created_at', $month)
												  ->whereYear('created_at', $year)
												  ->sum('amount');
		$redistribution['head_load'] = OtherInfo::where('info_id', 4)
												  ->whereMonth('created_at', $month)
												  ->whereYear('created_at', $year)
												  ->sum('amount');
		$redistribution['employees'] = Employee::with([
						            "absent" => function ($query) use ($request)
						            {
						                $query->whereMonth('created_at','=',$request->month);
						                $query->whereYear('created_at','=',$request->year);
						            },
						            "off_day" => function($query) use  ($request)
						            {
						                $query->whereMonth('created_at','=',$request->month);
						                $query->whereYear('created_at','=',$request->year);
						            },
						            "off_day_work" => function($query) use  ($request)
						            {
						                $query->whereMonth('created_at','=',$request->month);
						                $query->whereYear('created_at','=',$request->year);
						            },
						            "leave_day" => function($query) use  ($request)
						            {
						                $query->whereMonth('created_at','=',$request->month);        
						                $query->whereYear('created_at','=',$request->year);        
						            },
        							])->groupBy('designation')
        							->get();
        $redistribution['working_period'] = cal_days_in_month(CAL_GREGORIAN, $request->month, $request->year);
        $redistribution['ta_da'] = 0;
        foreach ($redistribution['employees'] as $each) {
            $work = $redistribution['working_period'] - $each->off_day['count'] + $each->off_day_work['count'] - $each->leave_day['count'] - $each->absent['count'];
            $redistribution['ta_da'] += $work * $each->tada;
        }
        $redistribution['total'] = $redistribution['load_unload'] + $redistribution['head_load'] + $redistribution['ta_da'];

        //Utility
        $utility = array();
        $utility['landline'] = OtherInfo::where('info_id', 5)
												  ->whereMonth('created_at', $month)
												  ->whereYear('created_at', $year)
												  ->sum('amount');
        $utility['mobile'] = OtherInfo::where('info_id', 6)
												  ->whereMonth('created_at', $month)
												  ->whereYear('created_at', $year)
												  ->sum('amount');
        $utility['wasa'] = OtherInfo::where('info_id', 7)
												  ->whereMonth('created_at', $month)
												  ->whereYear('created_at', $year)
												  ->sum('amount');
        $utility['electricity'] = OtherInfo::where('info_id', 8)
												  ->whereMonth('created_at', $month)
												  ->whereYear('created_at', $year)
												  ->sum('amount');
        $utility['gas'] = OtherInfo::where('info_id', 9)
												  ->whereMonth('created_at', $month)
												  ->whereYear('created_at', $year)
												  ->sum('amount');
        $utility['office_internet'] = OtherInfo::where('info_id', 10)
												  ->whereMonth('created_at', $month)
												  ->whereYear('created_at', $year)
												  ->sum('amount');
        $utility['hht_internet'] = OtherInfo::where('info_id', 11)
												  ->whereMonth('created_at', $month)
												  ->whereYear('created_at', $year)
												  ->sum('amount');
		$utility['total'] = $utility['landline'] + $utility['mobile'] + $utility['wasa'] + $utility['electricity'] + $utility['gas'] + $utility['office_internet'] +$utility['hht_internet'];

		//Miscellaneous
		$miscellaneous = OfficeExpense::where('name', 'other')
									  ->whereMonth('created_at', $month)
									  ->whereYear('created_at', $year)
									  ->sum('amount');

		return view('roi.details', compact('sales', 'month', 'year', 'dep', 'salary', 'vehicle', 'rent', 'offfice_expenses', 'investment', 'redistribution', 'utility', 'miscellaneous'));
	}

    public function pending_claim()
    {	
    	$pending_claims = CompanyExpense::select('company_expenses.*', DB::raw('SUM(c_claim.amount) as pass'))
    									->leftJoin('company_expenses_claim as c_claim', 'company_expenses.id', 'c_claim.company_expenses_id')
    									->groupBy('company_expenses.id', 'company_expenses.name', 'company_expenses.amount', 'company_expenses.created_at', 'company_expenses.updated_at')
    									->get();

    	return view('roi.pending_claim', compact('pending_claims'));

    }

    public function pending_claim_ubl()
    {	
    	$pending_claims = CompanyExpense::select('company_expenses.*', DB::raw('SUM(c_claim.amount) as pass'))
    									->leftJoin('company_expenses_claim as c_claim', 'company_expenses.id', 'c_claim.company_expenses_id')
    									->groupBy('company_expenses.id', 'company_expenses.name', 'company_expenses.amount', 'company_expenses.created_at', 'company_expenses.updated_at')
    									->get();

    	$due = 0;
    	foreach ($pending_claims as $each) 
    	{
    		$due += $each->amount;
    	}
    									
    	return $due;
    }
}
