<?php

namespace App\Http\Controllers;

use App\Employee;
use App\OffDay;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OffDayController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['off_day'] = OffDay::with('employee')->get();
        $data['employees'] = Employee::all();
        return view('employees.off-day-add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() ,[
            'employee_id' => 'string|required|max:255',
            'off_day' => 'required|unique:off_days,off_date,NULL,id,employee_id,'.$request->employee_id
        ]);

        if ($validator->fails()) {
            $request->session()->flash('err', 'Please check the inputs!');
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        $new_off_day = OffDay::create([
            'employee_id' => $request->employee_id,
            'off_date' => $request->off_day
        ]);
        $request->session()->flash('msg', 'Off Day Added Successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OffDay  $offDay
     * @return \Illuminate\Http\Response
     */
    public function show(OffDay $offDay)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OffDay  $offDay
     * @return \Illuminate\Http\Response
     */
    public function edit(OffDay $offDay)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OffDay  $offDay
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OffDay $offDay)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OffDay  $offDay
     * @return \Illuminate\Http\Response
     */
    public function destroy(OffDay $offDay)
    {
        $off_day = OffDay::find($offDay)->first();
        $off_day->delete();
        return "Off day deleted Successfully!";
    }
}
