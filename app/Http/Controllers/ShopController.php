<?php

namespace App\Http\Controllers;

use App\Shop;
use Illuminate\Http\Request;
use DB;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shops = Shop::all();

        $due = Shop::select(DB::raw('shops.id, SUM(dues.amount) as due'))
                             ->join('dues', 'shops.id', 'dues.shop_id')
                             ->groupBy('shops.id')
                             ->get();

        $due_realize = Shop::select(DB::raw('shops.id, SUM(due_realizes.amount) as due_realize'))
                             ->join('due_realizes', 'shops.id', 'due_realizes.shop_id')
                             ->groupBy('shops.id')
                             ->get();

        $cheque = Shop::select(DB::raw('shops.id, SUM(cheques.amount) as cheque'))
                             ->join('cheques', 'shops.id', 'cheques.shop_id')
                             ->where('status', 0)
                             ->orWhere('status', NULL)
                             ->groupBy('shops.id')
                             ->get();

        return view('shop.index', compact('shops', 'due', 'due_realize', 'cheque'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shop = new Shop;
        $shop->shop_name = $request->shop;
        $shop->save();
        return redirect()->back()->with('success', 'New Shop Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $shop = Shop::find($id);
        $shop->shop_name = $request->shop_edit;
        $shop->save();
        return redirect()->back()->with('success', 'Shop Name Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
