<?php

namespace App\Http\Controllers;

use App\Cash_flow;
use App\CompanyExpense;
use App\CompanyExpensesClaim;
use App\Employee;
use App\EmployeeExpense;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class RetailMarketController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {	
    	return view('retail_market.index');
    }

    public function retail_market(Request $request)
    {	
    	$request->validate([
    	    'month' => 'numeric|required',
    	    'year' => 'numeric|required',
    	]);

    	$month = $request->month; 
    	$year = $request->year;

    	$employees = Employee::all();
    	foreach ($employees as $employee) 
    	{
    		$cash_flows[] = Cash_flow::where('employee_id', $employee->id)->first();
    		$cash_short[] = EmployeeExpense::whereMonth('created_at', $month)
    		    					->whereYear('created_at', $year)
    		    					->where('employee_id', $employee->id)
    		    					->where('expenses_type', 0)
    		    					->sum('expenses_amount');
    		$cash_excess[] = EmployeeExpense::whereMonth('created_at', '=', date($month))
    		    					->whereYear('created_at', '=', date($year))
    		    					->where('employee_id', $employee->id)
    		    					->where('expenses_type', 1)
    		    					->sum('expenses_amount');
    	}

    	$employee_expenses = EmployeeExpense::whereMonth('created_at', '=', date($month))
    		    					->whereYear('created_at', '=', date($year))
    		    					->get();

    	
    	if (count($employee_expenses) > 0) 
    	{
			foreach ($employee_expenses as $employee_expense) 
    		{
    			$one[] = EmployeeExpense::whereDay('created_at', '=', date(1))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$two[] = EmployeeExpense::whereDay('created_at', '=', date(2))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$three[] = EmployeeExpense::whereDay('created_at', '=', date(3))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$four[] = EmployeeExpense::whereDay('created_at', '=', date(4))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$five[] = EmployeeExpense::whereDay('created_at', '=', date(5))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$six[] = EmployeeExpense::whereDay('created_at', '=', date(6))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$seven[] = EmployeeExpense::whereDay('created_at', '=', date(7))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$eigth[] = EmployeeExpense::whereDay('created_at', '=', date(8))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$nine[] = EmployeeExpense::whereDay('created_at', '=', date(9))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$ten[] = EmployeeExpense::whereDay('created_at', '=', date(10))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$eleven[] = EmployeeExpense::whereDay('created_at', '=', date(11))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$twelve[] = EmployeeExpense::whereDay('created_at', '=', date(12))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$thirteen[] = EmployeeExpense::whereDay('created_at', '=', date(13))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$fourteen[] = EmployeeExpense::whereDay('created_at', '=', date(14))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$fifteen[] = EmployeeExpense::whereDay('created_at', '=', date(15))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$sixteen[] = EmployeeExpense::whereDay('created_at', '=', date(16))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$seventeen[] = EmployeeExpense::whereDay('created_at', '=', date(17))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$eigthteen[] = EmployeeExpense::whereDay('created_at', '=', date(18))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$ninteen[] = EmployeeExpense::whereDay('created_at', '=', date(19))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$twenty[] = EmployeeExpense::whereDay('created_at', '=', date(20))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$twentyone[] = EmployeeExpense::whereDay('created_at', '=', date(21))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$twentytwo[] = EmployeeExpense::whereDay('created_at', '=', date(22))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$twentythree[] = EmployeeExpense::whereDay('created_at', '=', date(23))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$twentyfour[] = EmployeeExpense::whereDay('created_at', '=', date(24))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$twentyfive[] = EmployeeExpense::whereDay('created_at', '=', date(25))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$twentysix[] = EmployeeExpense::whereDay('created_at', '=', date(26))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$twentyseven[] = EmployeeExpense::whereDay('created_at', '=', date(27))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$twentyeight[] = EmployeeExpense::whereDay('created_at', '=', date(28))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$twentynine[] = EmployeeExpense::whereDay('created_at', '=', date(29))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$thirty[] = EmployeeExpense::whereDay('created_at', '=', date(30))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();
    			$thirtyone[] = EmployeeExpense::whereDay('created_at', '=', date(31))
    			    		->where('created_at', $employee_expense->created_at)
    			    		->get();

    		}//end foreach
    	}//end if

        //company expenses
        $company_expenses_prev_month = CompanyExpense::whereMonth('created_at', '<', $month)
                                                       ->whereYear('created_at', $year)
                                                       ->where('amount', '>', 0)
                                                       ->get();

        if (count($company_expenses_prev_month) > 0) 
        {   
            foreach ($company_expenses_prev_month as $value) 
            {
                for ($i=1; $i <= 31 ; $i++) 
                { 
                    $data[$i][] = CompanyExpensesClaim::where('company_expenses_id', $value->id)
                                                        ->whereDay('created_at', $i)
                                                        ->whereMonth('created_at', $month)
                                                        ->whereYear('created_at', $year)
                                                        ->get();
                }

                $claim[] = CompanyExpensesClaim::where('company_expenses_id', $value->id)
                                                 ->whereMonth('created_at', $month)
                                                 ->whereYear('created_at', $year)
                                                 ->sum('amount');

            }            
        }

        $company_expenses_current_month = CompanyExpense::whereMonth('created_at', $month)
                                                          ->whereYear('created_at', $year)
                                                          ->where('amount', '>', 0)
                                                          ->get();

        if (count($company_expenses_current_month) > 0) 
        {   
            foreach ($company_expenses_current_month as $value) 
            {
                for ($i=1; $i <= 31 ; $i++) 
                { 
                    $current_data[$i][] = CompanyExpensesClaim::where('company_expenses_id', $value->id)
                                                                ->whereDay('created_at', $i)
                                                                ->whereMonth('created_at', $month)
                                                                ->whereYear('created_at', $year)
                                                                ->get();

                    $current_claim_data[$i][] = CompanyExpense::where('id', $value->id)
                                                                ->whereDay('created_at', $i)
                                                                ->whereMonth('created_at', $month)
                                                                ->whereYear('created_at', $year)
                                                                ->get();
                }

                $current_claim[] = CompanyExpensesClaim::where('company_expenses_id', $value->id)
                                                         ->whereMonth('created_at', $month)
                                                         ->whereYear('created_at', $year)
                                                         ->sum('amount');

            }            
        }

        
 	

    	return view('retail_market.retail_market', compact('employee_expenses', 'employees', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eigth', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eigthteen', 'ninteen', 'twenty', 'twentyone', 'twentytwo', 'twentythree', 'twentyfour', 'twentyfive', 'twentysix', 'twentyseven', 'twentyeight', 'twentynine', 'thirty', 'thirtyone', 'cash_flows', 'cash_short', 'cash_excess', 'month', 'year', 'company_expenses_prev_month', 'data', 'claim', 'company_expenses_current_month', 'current_data', 'current_claim', 'current_claim_data'));
    }
}
