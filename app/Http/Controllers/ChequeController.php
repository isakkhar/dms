<?php

namespace App\Http\Controllers;

use App\Cheque;
use Illuminate\Http\Request;

class ChequeController extends Controller
{
    public function index()
    {	
    	$cheque_list = Cheque::select('cheques.*', 'shops.shop_name')
    						 ->join('shops', 'cheques.shop_id', 'shops.id')
    						 ->orderBy('cheques.id', 'DESC')
    						 ->get();
    						 
    	return view('cheque.index', compact('cheque_list'));
    }

    public function cheque_pass($id)
    {
    	$cheque = Cheque::find($id);
    	$status_update = $cheque->update([
    		'status' => 1
    	]);

    	if ($status_update) 
    	{
    		return redirect()->back()->with('success', 'This Cheque has been Passed!');
    	}
    }

    public function cheque_bounce($id)
    {
    	$cheque = Cheque::find($id);
    	$status_update = $cheque->update([
    		'status' => 0
    	]);

    	if ($status_update) 
    	{
    		return redirect()->back()->with('success', 'This Cheque has been Bounced!');
    	}
    }
}
