<?php

namespace App\Http\Controllers;

use App\Designation;
use App\Employee;
use App\ExpenseType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ExpenseTypeController extends Controller
{
    private $request;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $months = array();
        for ($i = 0; $i < 12; $i++) {
            $timestamp = mktime(0, 0, 0, date('n') - $i, 1);
            $months[date('n', $timestamp)] = date('F', $timestamp);
        }
        return view('expense-type.index')->with('months',$months);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['vehicle_no'] = ExpenseType::where('type',1)->get();
        return view('expense-type.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        switch($request->expense_type) {
            case "1":
                $create = ExpenseType::create([
                        'type' => $request->expense_type,
                        'vehicle_no' => $request->expense['vehicle_no'],
                        'amount' => $request->expense['amount']
                    ]);
            break;
            case "2":
                $create = ExpenseType::create([
                    'type' => $request->expense_type,
                    'fuel_type' => $request->expense['fuel_type'],
                    'amount' => $request->expense['amount']
                ]);
            break;
            case "3":
                $create = ExpenseType::create([
                    'type' => $request->expense_type,
                    'vehicle_no' => $request->expense['vehicle_no'],
                    'purpose' => $request->expense['purpose'],
                    'amount' => $request->expense['amount']
                ]);
            break;
            case "4":
                $create = ExpenseType::create([
                    'type' => $request->expense_type,
                    'purpose' => $request->expense['purpose'],
                    'amount' => $request->expense['amount']
                ]);
            break;
            case "5":
                $create = ExpenseType::create([
                    'type' => $request->expense_type,
                    'vehicle_no' => $request->expense['total_furniture'],
                    'purpose' => $request->expense['purpose'],
                    'amount' => $request->expense['amount']
                ]);
            break;
            default:
            $create = ExpenseType::create([
                'type' => $request->expense_type,
                'purpose' => $request->expense['purpose'],
                'amount' => $request->expense['amount']
            ]);
        }
        if ($create) {
            return ['status' => 200];
        } else{
            return ['status' => 404];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExpenseType  $expenseType
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        
        Session::put('request', $request->all());
        // $data['office_employee'] = Employee::where('employee_type',1)->get();
        // $data['field_employee'] = Employee::where('employee_type',2)->get();
        $data['office_employee'] = DB::table('employees')
                 ->select('*', DB::raw('sum(salary) as total,designation as d,count(id) as ind'))
                 ->groupBy('designation')
                 ->where('employee_type',1)
                 ->get();
        $data['field_employee'] = DB::table('employees')
                 ->select('*', DB::raw('sum(salary) as total,designation as d,count(id) as ind'))
                 ->groupBy('designation')
                 ->where('employee_type',2)
                 ->get();
        $data['office_employee_salary'] = DB::table('employees')
                 ->select('salary', DB::raw('sum(salary) as total'))
                 ->groupBy('designation')
                 ->where('employee_type',1)
                 ->first();

        $data['field_employee_salary'] = DB::table('employees')
                 ->select('salary', DB::raw('sum(salary) as total'))
                 ->groupBy('designation')
                 ->where('employee_type',2)
                 ->first();
        $data['employees'] = Employee::with([
            "absent" => function ($query) use ($request)
            {
                $query->whereMonth('created_at','=',$request->month);
                $query->whereYear('created_at','=',$request->year);
            },
            "off_day" => function($query) use  ($request)
            {
                $query->whereMonth('created_at','=',$request->month);
                $query->whereYear('created_at','=',$request->year);
            },
            "off_day_work" => function($query) use  ($request)
            {
                $query->whereMonth('created_at','=',$request->month);
                $query->whereYear('created_at','=',$request->year);
            },
            "leave_day" => function($query) use  ($request)
            {
                $query->whereMonth('created_at','=',$request->month);        
                $query->whereYear('created_at','=',$request->year);        
            },
        ])->groupBy('designation')
        ->get();
        $data['vehicle_fuel'] = ExpenseType::where('type',1)->get(); 
        $data['generator_fuel'] = ExpenseType::where('type',2)->get(); 
        $data['vehicle_main'] = ExpenseType::where('type',3)->get(); 
        $data['computer_main'] = ExpenseType::where('type',4)->get(); 
        $data['furniture_main'] = ExpenseType::where('type',5)->get(); 
        $data['total_rent'] = ExpenseType::where('type',6)->get();
        $data['working_period'] = cal_days_in_month(CAL_GREGORIAN, $request->month, $request->year);
        
        $data['con_total'] = 0;
        foreach ($data['employees'] as $employee) {
            $work = $data['working_period'] - $employee->off_day['count'] + $employee->off_day_work['count'] - $employee->leave_day['count'] - $employee->absent['count'];
            $data['con_total'] += $work * $employee->tada;
        }
        $data['v_f_total'] = 0;
        return view('expense-type.generate',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExpenseType  $expenseType
     * @return \Illuminate\Http\Response
     */
    public function edit(ExpenseType $expenseType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExpenseType  $expenseType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExpenseType $expenseType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExpenseType  $expenseType
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExpenseType $expenseType)
    {
        //
    }
    public function export()
    {
        $request = Session::get('request');
        $data['office_employee'] = DB::table('employees')
                 ->select('*', DB::raw('sum(salary) as total,designation as d,count(id) as ind'))
                 ->groupBy('designation')
                 ->where('employee_type',1)
                 ->get();
        $data['field_employee'] = DB::table('employees')
                 ->select('*', DB::raw('sum(salary) as total,designation as d,count(id) as ind'))
                 ->groupBy('designation')
                 ->where('employee_type',2)
                 ->get();
        $data['office_employee_salary'] = DB::table('employees')
                 ->select('salary', DB::raw('sum(salary) as total'))
                 ->groupBy('designation')
                 ->where('employee_type',1)
                 ->first();

        $data['field_employee_salary'] = DB::table('employees')
                 ->select('salary', DB::raw('sum(salary) as total'))
                 ->groupBy('designation')
                 ->where('employee_type',2)
                 ->first();
        $data['employees'] = Employee::with([
            "absent" => function ($query) use ($request)
            {
                $query->whereMonth('created_at','=',$request['month']);
                $query->whereYear('created_at','=',$request['year']);
            },
            "off_day" => function($query) use  ($request)
            {
                $query->whereMonth('created_at','=',$request['month']);
                $query->whereYear('created_at','=',$request['year']);
            },
            "off_day_work" => function($query) use  ($request)
            {
                $query->whereMonth('created_at','=',$request['month']);
                $query->whereYear('created_at','=',$request['year']);
            },
            "leave_day" => function($query) use  ($request)
            {
                $query->whereMonth('created_at','=',$request['month']);        
                $query->whereYear('created_at','=',$request['year']);        
            },
        ])->groupBy('designation')
        ->get();
        $data['vehicle_fuel'] = ExpenseType::where('type',1)->get(); 
        $data['generator_fuel'] = ExpenseType::where('type',2)->get(); 
        $data['vehicle_main'] = ExpenseType::where('type',3)->get(); 
        $data['computer_main'] = ExpenseType::where('type',4)->get(); 
        $data['furniture_main'] = ExpenseType::where('type',5)->get(); 
        $data['total_rent'] = ExpenseType::where('type',6)->get();
        $data['working_period'] = cal_days_in_month(CAL_GREGORIAN, $request['month'], $request['year']);
        
        $data['con_total'] = 0;
        foreach ($data['employees'] as $employee) {
            $work = $data['working_period'] - $employee->off_day['count'] + $employee->off_day_work['count'] - $employee->leave_day['count'] - $employee->absent['count'];
            $data['con_total'] += $work * $employee->tada;
        }
        $output = '';
        $output .='
        <table border="1">
            <thead>
            <tr>
            <th colspan="5" style="background-color:#0000FF;color:white">Office staff Salaries</th>
            </tr>
                    <tr>
                        <th>ID</th>
                        <th>Designation</th>
                        <th>Total No. of employees</th>
                        <th>Salary</th>
                        <th>Total Salary</th>
                    </tr>
                </thead>
                <tbody>';
                $i =0;
        foreach($data['office_employee'] as $employee){
            $i++;
            $output .= '<tr>
                        <td>'.$i.'</td>
                        <td>'. Designation::find($employee->d)->name.'</td>
                        <td>'. $employee->ind .'</td>
                        <td>'. round($employee->total / $employee->ind) .'</td>
                        <td>'. $employee->total .'</td>
                    </tr>';
              }
             $output .= '</tbody>
                <tfoot>
                    <tr style="">
                        <th colspan="4" style="text-align:left;  background-color:#DCE6F1;"><strong>Total</strong></th>
                        <th style="background-color:#DCE6F1;text-align:right;"><strong>'.$data['office_employee_salary']->total.'</strong></th>
                    </tr>
                </tfoot></table>';
        $output .= '<table border="1"><thead>
                <tr>
                    <th colspan="5" style="background-color:#0000FF;color:white">Office staff Salaries</th>
                </tr>
                    <tr>
                        <th>ID</th>
                        <th>Designation</th>
                        <th>Total No. of employees</th>
                        <th>Salary</th>
                        <th>Total Salary</th>
                    </tr>
                </thead>
                <tbody>';
                $j =0;
        foreach($data['field_employee'] as $employee){
            $j++;
            $output .= '<tr>
                        <td>'.$j.'</td>
                        <td>'. Designation::find($employee->d)->name.'</td>
                        <td>'. $employee->ind .'</td>
                        <td>'. round($employee->total / $employee->ind) .'</td>
                        <td>'. $employee->total .'</td>
                    </tr>';
              }
            $output .= '</tbody>
                <tfoot>
                    <tr>
                        <th colspan="4" style="text-align:left;  background-color:#DCE6F1;"><strong>Total</strong></th>
                        <th style="background-color:#DCE6F1;text-align:right;"><strong>'.$data['field_employee_salary']->total.'</strong></th>
                    </tr>
                </tfoot></table>';

        $output .= '<table border="1"><thead>
                <tr>
                    <th colspan="5" style="background-color:#0000FF;color:white">Conveyance</th>
                </tr>
                    <tr>
                        <th>No</th>
                        <th>Designation</th>
                        <th>No. of Days</th>
                        <th>Rate Per day </th>
                        <th>Total Cost </th>
                    </tr>
                </thead>
                <tbody>';
            foreach($data['employees'] as $employee){
            $output .= '<tr>
                        <td>'.$employee->id .'</td>
                        <td>'.$employee->designation_name->name .'</td>
                        <td>'.$work = $data['working_period'] + $employee->off_day['count'] + $employee->off_day_work['count'] + $employee->leave_day['count'] - $employee->absent['count'] .'</td>';

                    $output .= '<td>
                            '.$employee->tada .'
                        </td>
                        <td>
                            ';
                $output .= ($data['working_period'] - $employee->off_day['count'] + $employee->off_day_work['count'] - $employee->leave_day['count'] - $employee->absent['count']) * $employee->tada;
                $output .= '
                        </td>
                    </tr>';
            }
        $output .= '
            </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4" style="text-align:left;  background-color:#DCE6F1;"><strong>Total</strong></td>
                        <td style="background-color:#DCE6F1;text-align:right;"><strong>'. $data['con_total'] .'</strong></td>
                    </tr>
                </tfoot>
            </table>
        ';
        $output .= '<table border="1">
            <thead>
            <tr>
                    <th colspan="3" style="background-color:#0000FF;color:white">Vehicle Fuel Cost</th>
                </tr>
                    <tr>
                        <th>ID</th>
                        <th>Vehicle No.</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>';
                foreach($data['vehicle_fuel'] as $expense){
            $output .= '<tr>
                        <td>'. $expense->id .'</td>
                        <td>'. $expense->vehicle_no .'</td>
                        <td>'. $expense->amount .'</td>
                    </tr>';
            }
        $output .= '
            </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" style="text-align:left;  background-color:#DCE6F1;"><strong>Total</strong></td>
                        <td style="background-color:#DCE6F1;text-align:right;"><strong>'. $data['vehicle_fuel']->sum('amount') .'</strong></td>
                    </tr>
                </tfoot>
            </table>
        ';
        $output .= '<table border="1">
            <thead>
            <tr>
                    <th colspan="3" style="background-color:#0000FF;color:white">Generator Fuel Cost</th>
                </tr>
                    <tr>
                        <th>ID</th>
                        <th>Vehicle No.</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>';
                foreach($data['generator_fuel'] as $expense){
            $output .= '<tr>
                        <td>'. $expense->id .'</td>
                        <td>'. $expense->fuel_type .'</td>
                        <td>'. $expense->amount .'</td>
                    </tr>';
            }
        $output .= '
            </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" style="text-align:left;  background-color:#DCE6F1;"><strong>Total</strong></td>
                        <td style="background-color:#DCE6F1;text-align:right;"><strong>'. $data['generator_fuel']->sum('amount') .'</strong></td>
                    </tr>
                </tfoot>
            </table>
        ';
        $output .= '<table border="1">
            <thead>
            <tr>
                    <th colspan="4" style="background-color:#0000FF;color:white">Vehicle Maintainance Cost</th>
                </tr>
                    <tr>
                        <th>ID</th>
                        <th>Fuel Type</th>
                        <th>Purpose</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>';
                foreach($data['vehicle_main'] as $expense){
            $output .= '<tr>
                        <td>'. $expense->id .'</td>
                        <td>'. $expense->fuel_type .'</td>
                        <td>'. $expense->purpose .'</td>
                        <td>'. $expense->amount .'</td>
                    </tr>';
            }
        $output .= '
            </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" style="text-align:left;  background-color:#DCE6F1;"><strong>Total</strong></td>
                        <td style="background-color:#DCE6F1;text-align:right;"><strong>'. $data['vehicle_main']->sum('amount') .'</strong></td>
                    </tr>
                </tfoot>
            </table>
        ';

        $output .= '<table border="1">
            <thead>
            <tr>
                    <th colspan="4" style="background-color:#0000FF;color:white">Furniture Maintainance Cost</th>
                </tr>
                    <tr>
                        <th>ID</th>
                        <th>Total Furniture</th>
                        <th>Purpose</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>';
                foreach($data['furniture_main'] as $expense){
            $output .= '<tr>
                        <td>'. $expense->id .'</td>
                        <td>'. $expense->vehicle_no .'</td>
                        <td>'. $expense->purpose .'</td>
                        <td>'. $expense->amount .'</td>
                    </tr>';
            }
        $output .= '
            </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" style="text-align:left;  background-color:#DCE6F1;"><strong>Total</strong></td>
                        <td style="background-color:#DCE6F1;text-align:right;"><strong>'. $data['furniture_main']->sum('amount') .'</strong></td>
                    </tr>
                </tfoot>
            </table>
        ';
        $output .= '<table border="1">
            <thead>
            <tr>
                    <th colspan="3" style="background-color:#0000FF;color:white">Computer Maintainance Cost</th>
                </tr>
                    <tr>
                        <th>ID</th>
                        <th>Purpose</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>';
                foreach($data['computer_main'] as $expense){
            $output .= '<tr>
                        <td>'. $expense->id .'</td>
                        <td>'. $expense->purpose .'</td>
                        <td>'. $expense->amount .'</td>
                    </tr>';
            }
        $output .= '
            </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" style="text-align:left;  background-color:#DCE6F1;"><strong>Total</strong></td>
                        <td style="background-color:#DCE6F1;text-align:right;"><strong>'. $data['computer_main']->sum('amount') .'</strong></td>
                    </tr>
                </tfoot>
            </table>
        ';
        $output .= '<table border="1">
            <thead>
            <tr>
                    <th colspan="3" style="background-color:#0000FF;color:white">Total Rent</th>
                </tr>
                    <tr>
                        <th>ID</th>
                        <th>Purpose</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>';
                foreach($data['total_rent'] as $expense){
            $output .= '<tr>
                        <td>'. $expense->id .'</td>
                        <td>'. $expense->purpose .'</td>
                        <td>'. $expense->amount .'</td>
                    </tr>';
            }
        $output .= '
            </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2" style="text-align:left;  background-color:#DCE6F1;"><strong>Total</strong></td>
                        <td style="background-color:#DCE6F1;text-align:right;"><strong>'. $data['total_rent']->sum('amount') .'</strong></td>
                    </tr>
                </tfoot>
            </table>
        ';
        header('content-type: application/xls');
        header('Content-Disposition: attachment; filename=expenses.xls');
        echo $output;
        Session::put('request',NULL);
    }
}
