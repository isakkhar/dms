<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $products = Product::all();
        return view('products.index', compact("products"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'product_name' => 'required',
            'product_sku_code' => 'numeric|required',
            'trade_price' => 'numeric|required',
            'list_price' => 'numeric|required',
            'mrp' => 'numeric|required',
        ]);


        $product = Product::create([
            'product_name' => $request->product_name,
            'product_details' => $request->product_details,
            'product_sku_code' => $request->product_sku_code,
            'trade_price' => $request->trade_price,
            'list_price' => $request->list_price,
            'mrp' => $request->mrp,
        ]);
        if ($product) {
            $request->session()->flash('msg', 'Product added Successfully!');
            return redirect()->route('product.index');
        } else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $id = $product;
        $product = Product::findOrFail($id)->first();
        if($product){
            return view('products.edit')->with('product' , $product);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {   
        $request->validate([
            'product_name' => 'required',
            'product_sku_code' => 'numeric|required',
            'trade_price' => 'numeric|required',
            'list_price' => 'numeric|required',
            'mrp' => 'numeric|required',
        ]);

        $id = $product;
        $find_product = Product::findOrFail($id)->first();

        $product = $find_product->update([
            'product_name' => $request->product_name,
            'product_details' => $request->product_details,
            'product_sku_code' => $request->product_sku_code,
            'trade_price' => $request->trade_price,
            'list_price' => $request->list_price,
            'mrp' => $request->mrp,
        ]);

        if ($product) {
            $request->session()->flash('msg', 'Product\'s Information Updated Successfully!');
            return redirect()->route('product.index');
        } else{
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product = Product::find($product)->first();
        $product->delete();
        return "Product deleted Successfully!";
    }
}
