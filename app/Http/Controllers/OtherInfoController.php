<?php

namespace App\Http\Controllers;

use App\OtherInfo;
use Illuminate\Http\Request;

class OtherInfoController extends Controller
{
    public function index()
    {
    	return view('other_infos.index');
    }

    public function store(Request $request)
    {	
    	$request->validate([
    		'name' => 'required',
    		'amount' => 'numeric|required'
    	]);

    	$obj = json_decode($request->name);

    	$other_infos = New OtherInfo;
    	$other_infos->name = $obj[0];
    	$other_infos->info_id = $obj[1];
    	$other_infos->amount = $request->amount;
    	$other_infos->save();

    	return redirect('/other-info')->with('success', 'Successfully Added!');
    }
}
