<?php

namespace App\Http\Controllers;

use App\Attendance;

use App\ProvidentFund;
use App\CashFlow;
use App\Designation;
use App\Employee;
use App\EmployeeExpense;
use App\OffDay;
use App\Tada;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator; 

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function ProvidentList()
    {
        $data['employees'] = Employee::with('provident')->get();
        return view('provident_fund.list',$data);
    }
    
    public function ProvidentPay($employee)
    {
        $data['employee'] = Employee::find($employee)->first();
        return view('provident_fund.pay',$data);
    }
    public function ProvidentPost(Request $request)
    {
        $fund = ProvidentFund::create([
            'employee_id' => $request->id,
            'paid_amount' => $request->amount
            ]);
        if($fund){
            Session::flash('msg', 'Provident Fund paid!');
            return redirect()->route('provident-fund-list');    
        }
        
    }

    public function AllEmployee()
    {
        $employees = Employee::with('off_day')->get();
        return response()->json($employees);
    }

    
    public function all_off_employee()
    {
        // dd(Employee::with('off_today')->get());
        $employees = Employee::with('off_day')->get();
//         $employees = Employee::with("off_day")->whereHas("off_day",function($q){
//     $q->where('off_date', '=', Carbon::now()->format('l'));
// })->get();
        return response()->json($employees);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['employees'] = Employee::all();
        return view('employees.employee-list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['designations'] = Designation::all();
        return view('employees.add',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all() ,[
            'name' => 'string|required|max:255',
            'email' => 'required|email|unique:employees',
            'phone' => 'required|regex:/(01)[0-9]{9}/|unique:employees',
            'gender' => 'required',
            'dob' => 'required|date',
            'present_addr' => 'required',
            'permanent_addr' => 'required',
            'department' => 'required',
            'designation' => 'required',
            'join_date' => 'required',
            'provident_fund' => 'required',
            'salary' => 'required',
            'tada' => 'required',
            'employee_type' => 'required',
            'photo' => 'sometimes|image',
            'father_name' => 'sometimes|string',
        ]);

        if ($validator->fails()) {
            $request->session()->flash('err', 'Please check the highlighted inputs!');
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        if ($request->file('photo')) {
            $photo = $request->file('photo')->store('public');
        }


        $employee = Employee::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'dob' => date('Y-m-d', strtotime($request->dob)),
            'present_addr' => $request->present_addr,
            'permanent_addr' => $request->permanent_addr,
            'department' => $request->department,
            'designation' => $request->designation,
            'join_date' => date('Y-m-d', strtotime($request->join_date)),
            'provident_fund' => $request->provident_fund,
            'salary' => $request->salary,
            'father_name' => $request->father_name,
            'tada' => $request->tada,
            'employee_type' => $request->employee_type,
            'photo' => $photo ?? null
        ]);

        $cash_flow_for_employee = new CashFlow;
        $cash_flow_for_employee->employee_id = $employee->id;
        $cash_flow_for_employee->cash_short = 0;
        $cash_flow_for_employee->cash_excess = 0;
        $cash_flow_for_employee->save();

        if ($employee) {
            $request->session()->flash('msg', 'Employee Created Successfully!');
            return redirect()->route('employee.index');
        } else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        $id = $employee;
        $data['employee'] = Employee::findOrFail($id)->first();
        return view('employees.profile',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $id = $employee;
        $data['designations'] = Designation::all();
        $employee = Employee::findOrFail($id)->first();
        if($employee){
            return view('employees.edit',$data)->with('employee' , $employee);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $id = $employee;
        $f_employee = Employee::findOrFail($id)->first();

        $validator = Validator::make($request->all() ,[
            'name' => 'string|required|max:255',
            'email' => 'required|email|unique:employees,email,'.$f_employee->id,
            'phone' => 'required|regex:/(01)[0-9]{9}/|unique:employees,phone,'.$f_employee->id,
            'gender' => 'required',
            'dob' => 'required|date',
            'present_addr' => 'required',
            'permanent_addr' => 'required',
            'department' => 'required',
            'designation' => 'required',
            'join_date' => 'required',
            'provident_fund' => 'required',
            'salary' => 'required',
            'tada' => 'required',
            'photo' => 'sometimes|image'
        ]);

        if ($validator->fails()) {
            $request->session()->flash('err', 'Please check the highlighted inputs!');
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }
        if ($request->file('photo')) {
            $photo = $request->file('photo')->store('public');
            $photo = substr($photo, 7);
        } else{
            $photo = $f_employee->photo;
        }


        $employee = $f_employee->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'gender' => $request->gender,
            'dob' => date('Y-m-d', strtotime($request->dob)),
            'present_addr' => $request->present_addr,
            'permanent_addr' => $request->permanent_addr,
            'department' => $request->department,
            'designation' => $request->designation,
            'join_date' => date('Y-m-d', strtotime($request->join_date)),
            'provident_fund' => $request->provident_fund,
            'salary' => $request->salary,
            'father_name' => $request->father_name,
            'tada' => $request->tada,
            'photo' => $photo ?? null,
            'employee_type' => $request->employee_type,
        ]);

        if ($employee) {
            $request->session()->flash('msg', 'Employee\'s Information edited Successfully!');
            return redirect()->route('employee.index');
        } else{
            return redirect()->back();
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee = Employee::find($employee)->first();
        $employee_attendance = Attendance::where('employee_id',$employee)->get();
        $employee->delete();
        foreach ($employee_attendance as $attendance) {
            $attendance->delete();
        }
        $employee_expense = EmployeeExpense::where('employee_id',$employee)->get();
        $off_day = OffDay::where('employee_id',$employee)->get();
        $tada = Tada::where('employee_id',$employee)->get();
        $cash_flow = CashFlow::where('employee_id',$employee)->get();
        if($employee_expense){
            $employee_expense->each->delete();
        }
        if ($off_day) {
            $off_day->each->delete();
        }

        return "User deleted Successfully!";
    }


    // For Tada
    public function tada()
    {
        $months = array();
        for ($i = 0; $i < 12; $i++) {
            $timestamp = mktime(0, 0, 0, date('n') - $i, 1);
            $months[date('n', $timestamp)] = date('F', $timestamp);
        }
        $data['employees'] = Employee::with('off_day')->get();
        return view('employees.tada',$data)->with('months',$months);
    }
    public function tada_post(Request $request)
    {
        Session::put('request',$request->all());
        // $data['employees'] = Employee::with("count_absent","all_off_day")->whereHas("count_absent",function($q) use  ($from,$to){
        //     $q->whereBetween('absent_date', [$from, $to]);
        // })->get();
//         $data['employees'] = Employee::with("count_absent","all_off_day")->where(function($query) use ($from,$to){
//      $query->whereBetween('absent_date',array($from,$to))
//            ->whereBetween('all_off_day',array($from." 00:00:00" ,$to." 23:59:59"));
// })->get();
        // $absent_days = Attendance::whereBetween('absent_date', [$from, $to])->where('off_day_work',NULL)->get();
        // $absent_days = Attendance::whereBetween('absent_date', [$from, $to])->where('off_day_work',NULL)->get()->count();
        // $off_days = Attendance::whereBetween('absent_date', [$from, $to])->where('off_day_work',1)->get()->count();
        if ($request->date == 'first') {
            $day1 = '01';
            $day2 = '15';
        } else {
            $day1 = '16';
            $day2 = cal_days_in_month(CAL_GREGORIAN, $request->month, $request->year);
        }
        if ($request->month > 9) {
            $month = $request->month;
        } else {
            $month = '0'.$request->month;
        }
        

        
        $from = date(Carbon::parse($month.'/'.$day1.'/'.$request->year)->format('Y-m-d'));
        $to = date(Carbon::parse($month.'/'.$day2.'/'.$request->year)->format('Y-m-d'));

        $datetime1 = new DateTime($from);
        $datetime2 = new DateTime($to);
        $interval = $datetime1->diff($datetime2);
        $data['working_period'] = $interval->days;


        $data['all_employees'] = Employee::with('off_day',"count_absent","cash_flow")->get();
        
        $data['employees'] = Employee::with([
            "absent" => function ($query) use ($from,$to)
            {
                $query->whereBetween('date',array($from,$to));
            },
            "off_day" => function($query) use  ($from,$to)
            {
                $query->whereBetween('date',array($from,$to));
            },
            "off_day_work" => function($query) use  ($from,$to)
            {
                $query->whereBetween('date',array($from,$to));
            },
            "leave_day" => function($query) use  ($from,$to)
            {
                $query->whereBetween('date',array($from,$to));
            },
            "cash_flow",
            "paid_tada" => function($query) use  ($from,$to)
            {
                $query->whereBetween('start_date',array($from,$to));
            },
        ])->get();
        return view('employees.tada-list',$data)
        ->with('to',$to)
        ->with('from',$from);
    }


    // For Salary
    public function salary()
    {
        $months = array();
        for ($i = 0; $i < 12; $i++) {
            $timestamp = mktime(0, 0, 0, date('n') - $i, 1);
            $months[date('n', $timestamp)] = date('F', $timestamp);
        }
        
        // dd($data['employees']);
        return view('employees.salary')->with('months',$months);
    }
    public function CheckSalary(Request $request)
    {
        Session::put('salaryRequest',$request->all());
        $data['month'] = $request->month;
        $data['year'] = $request->year;
        $data['employees'] = Employee::with('off_day','attendance','cash_flow','absent','leave_day','off_day_work')->get();
        return view('employees.salary-list',$data);

    }

    // add Designation
    public function addDesignation()
    {
        return view('employees.add-designation');
    }
    // create Designation
    public function createDesignation(Request $request)
    {
        $designation = Designation::create([ 'name' => $request->name ]);
        if ($designation) {
            $request->session()->flash('msg', 'Designation Created Successfully!');
            return redirect()->route('add-designation');
        }
    }
    public function exportSalary()
    {
        $request = Session::get('salaryRequest');
        $data['month'] = $request['month'];
        $data['year'] = $request['year'];
        $data['employees'] = Employee::with('off_day','attendance','cash_flow','absent','leave_day','off_day_work')->get();

        $output  = '';
        $output .= '
        <table border="1">
            <thead>
                        <tr>
                            <th>ID</th>
                            <th>Employee Name</th>
                            <th>Designation</th>
                            <th>Off Day Duty</th>
                            <th>Off Day</th>
                            <th>Present</th>
                            <th>Total Present</th>
                            <th>Leave</th>
                            <th>Absent</th>
                            <th>Basic Salary</th>
                            <th>Ho</th>
                            <th>Total Absent(TK)</th>
                            <th>Provident Fund</th>
                            
                            <th>Cash Short </th>
                            <th>Net Payable </th>
                        </tr>
                    </thead>
                            
                    <tbody>';
            foreach($data['employees'] as $employee){
                $output .= "<tr>
                            <td> ".$employee->id ."</td>
                            <td> ".$employee->name ."</td>
                            <td> ".$employee->designation_name->name ."</td>
                            <td> ".$employee->off_day_work['count'] ."</td>
                            <td> ".$employee->off_day['count'] ."</td>
                            <td> ". (date('d') - $employee->off_day_work['count'] - $employee->off_day['count'] - $employee->absent['count']) ."</td>
                            <td> ". (date('d') + $employee->off_day_work['count'] -$employee->off_day['count'] - $employee->absent['count']) ."</td>
                            <td> ".$employee->leave['count'] ."</td>
                            <td> ".$employee->absent['count'] ."</td>
                            <td> ".$employee->salary ."</td>
                            <td> ".round($employee->salary / 30) * $employee->off_day['count'] ."</td>
                            <td> ".round($employee->salary / 30) * $employee->absent['count'] ."</td>
                            <td> ".$employee->provident_fund ."</td>
                            <td> ".$employee->cash_flow->cash_short ."</td>
                            
                            <td> ".round((date('d') - $employee->off_day_work['count'] -$employee->off_day['count']) * ($employee->salary / 30)
                            + 
                            (round($employee->salary / 30) * $employee->off_day['count'] )
                            - 
                            (round($employee->salary / 30) * $employee->absent['count'])
                            -
                            $employee->provident_fund - $employee->cash_flow->cash_short) ."</td>
                        </tr>";
                }
            $output .= '</tbody></table>';
        header('content-type: application/xls');
        header('Content-Disposition: attachment; filename=salary.xls');
        echo $output;
        Session::put('salaryRequest',NULL);
    }

    public function exportEmployee()
    {
        $data['employees'] = Employee::all();
        $output = '';
        $output .= '<table border="1">
                <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Department</th>
                            <th>Designation</th>
                            <th>Joining Date</th>
                            <th>TADA</th>
                            <th>Provident Fund</th>
                            <th>Current Salary</th>
                        </tr>
                    </thead>
                            
                    <tbody>';
                foreach($data['employees'] as $employee) {
                    $output .= '<tr>
                            <td>'. $employee->id .'</td>
                            <td>'. $employee->name .'</td>
                            <td>'. $employee->email .'</td>
                            <td>'. $employee->phone .'</td>
                            <td>'. $employee->department .'</td>
                            <td>'. $employee->designation_name->name .'</td>
                            <td>'. $employee->join_date .'</td>
                            <td>'. $employee->tada .'</td>
                            <td>'. $employee->provident_fund .'</td>
                            <td>'. $employee->salary .'</td>
                        </tr>';
                    }
            $output .= '</tbody></table>';
        header('content-type: application/xls');
        header('Content-Disposition: attachment; filename=employees.xls');
        echo $output;
    }
}
