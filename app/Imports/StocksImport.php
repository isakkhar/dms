<?php

namespace App\Imports;

use App\Stock;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\Importable;

class StocksImport implements ToModel
{
    use Importable;

    public function model(array $row)
    {
        return new Stock([
            'name' => $row[0],
        ]);
    }
}

