-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2019 at 04:37 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dms`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE `attendances` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `present` tinyint(4) DEFAULT NULL,
  `absent` tinyint(4) DEFAULT NULL,
  `off_day` tinyint(4) DEFAULT NULL,
  `leave_day` tinyint(4) DEFAULT NULL,
  `off_day_work` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendances`
--

INSERT INTO `attendances` (`id`, `employee_id`, `date`, `present`, `absent`, `off_day`, `leave_day`, `off_day_work`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-07-24', 1, 1, 1, 1, 1, '2019-07-23 19:26:06', '0000-00-00 00:00:00'),
(2, 1, '2019-07-24', 1, 1, 1, 1, 1, '2019-07-23 19:26:14', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cash_flows`
--

CREATE TABLE `cash_flows` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `cash_short` int(11) NOT NULL DEFAULT '0',
  `cash_excess` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cash_flows`
--

INSERT INTO `cash_flows` (`id`, `employee_id`, `cash_short`, `cash_excess`, `created_at`, `updated_at`) VALUES
(3, 16, 1798, 0, '2019-07-20 19:21:11', '2019-07-20 19:21:11'),
(4, 17, 3596298, 0, '2019-07-25 02:22:02', '2019-07-25 02:22:02'),
(5, 18, 6000, 2000, '2019-04-19 11:51:17', '2019-04-19 05:51:17'),
(6, 19, 0, 0, '2019-06-23 02:19:36', '2019-06-23 02:19:36'),
(7, 20, 0, 4963202, '2019-07-25 02:19:02', '2019-07-25 02:19:02'),
(8, 21, 0, 0, '2019-06-23 02:36:11', '2019-06-23 02:36:11'),
(9, 22, 0, 0, '2019-06-23 02:51:46', '2019-06-23 02:51:46');

-- --------------------------------------------------------

--
-- Table structure for table `company_expenses`
--

CREATE TABLE `company_expenses` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_expenses`
--

INSERT INTO `company_expenses` (`id`, `name`, `amount`, `created_at`, `updated_at`) VALUES
(3, 'pepsi', 200, '2019-02-06 11:27:16', '2019-04-26 21:36:39'),
(4, 'Sales Expenses', 803, '2019-03-24 10:47:19', '2019-03-24 10:47:19'),
(5, 'Sales Expenses', 803, '2019-03-28 13:05:27', '2019-03-28 13:05:27'),
(6, 'Sales Expenses', 803, '2019-03-30 13:15:15', '2019-03-30 13:15:15'),
(7, 'Sales Expenses', 803, '2019-04-01 12:16:56', '2019-04-01 12:16:56'),
(8, 'Sales Expenses', 803, '2019-04-01 12:18:30', '2019-04-01 12:18:30'),
(9, 'Sales Expenses', 803, '2019-04-01 12:19:54', '2019-04-01 12:19:54');

-- --------------------------------------------------------

--
-- Table structure for table `company_expenses_claim`
--

CREATE TABLE `company_expenses_claim` (
  `id` int(11) NOT NULL,
  `company_expenses_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_expenses_claim`
--

INSERT INTO `company_expenses_claim` (`id`, `company_expenses_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 3, 2000, '2019-05-04 11:14:41', '2019-05-04 11:14:41'),
(2, 3, 500, '2019-05-08 20:59:40', '2019-05-04 17:20:18'),
(3, 2, 500, '2019-05-04 17:20:27', '2019-05-04 17:20:27'),
(4, 5, 22, '2019-05-07 21:20:18', '2019-05-06 21:19:21'),
(5, 4, 22, '2019-05-06 20:27:00', '2019-05-06 21:19:26'),
(6, 4, 423, '2019-05-09 19:41:37', '2019-05-07 19:34:20'),
(7, 6, 100, '2019-05-07 20:02:02', '2019-05-07 20:02:02'),
(8, 7, 2500, '2019-05-09 20:24:49', '2019-05-09 20:24:49');

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'CTO', '2019-07-07 12:47:52', '2019-07-07 12:47:52'),
(2, 'CFO', '2019-07-07 12:48:28', '2019-07-07 12:48:28'),
(3, 'Manager', '2019-07-07 12:48:50', '2019-07-07 12:48:50'),
(4, 'Employee', '2019-07-07 12:48:57', '2019-07-07 12:48:57'),
(5, 'Guard', '2019-07-20 19:27:52', '2019-07-20 19:27:52');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `DOB` date NOT NULL,
  `present_addr` text NOT NULL,
  `permanent_addr` text NOT NULL,
  `father_name` varchar(100) DEFAULT NULL,
  `department` varchar(30) NOT NULL,
  `designation` varchar(30) NOT NULL,
  `join_date` date NOT NULL,
  `joining_salary` int(11) NOT NULL,
  `salary` int(11) NOT NULL,
  `tada` int(11) NOT NULL,
  `photo` varchar(500) DEFAULT NULL,
  `employee_type` int(11) NOT NULL,
  `provident_fund` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `name`, `email`, `phone`, `gender`, `DOB`, `present_addr`, `permanent_addr`, `father_name`, `department`, `designation`, `join_date`, `joining_salary`, `salary`, `tada`, `photo`, `employee_type`, `provident_fund`, `created_at`, `updated_at`) VALUES
(1, 'Hridoy', 'zihad@dms.com', '01414562565', 'm', '2019-04-07', 'ads', 'afd', 'afd', 'Sales', '1', '2019-04-01', 651, 1000, 651, NULL, 1, 100, '2019-07-23 19:29:02', '2019-04-18 14:15:50'),
(17, 'ajib', 'ajib@sojib.co', '01579344525', 'm', '2019-04-08', 'adfs', 'afd', 'afd', 'Stock', '5', '2019-04-01', 200, 2003, 288, NULL, 2, 100, '2019-07-20 19:34:20', '2019-04-18 14:22:48'),
(18, 'john doe', 'doe@john.com', '01547896512', 'm', '2019-04-01', 'lakjdfslafkjds', 'oirjoifdjoiro', 'aoijeroiqjo', 'IT', '3', '2019-04-01', 255536, 1000, 5353, NULL, 1, 100, '2019-07-20 19:34:56', '2019-04-19 04:21:51'),
(19, 'Rafid', 'admin@example.com', '01967890878', 'm', '2015-01-20', 'sompara', 'Dhaka', 'Abdul Kadir', 'Sales', '4', '2017-12-09', 10000, 1500, 50, NULL, 1, 100, '2019-07-20 19:35:02', '2019-06-23 02:19:36'),
(20, 'sahil', 'princerafid01@gmail.coms', '01967890871', 'm', '2015-01-21', 'dfrd', 'dfdfdfd', 'Abdul Kadir', 'Accounts', '2', '2006-08-12', 20000, 2000, 78, NULL, 1, 100, '2019-07-20 19:35:06', '2019-06-23 02:26:03'),
(21, 'opey', 'admin@mail.com', '01967890877', 'm', '2000-01-31', 'bb', 'rwfr', 'Abdul Kadir', 'Stock', '5', '2019-06-18', 1000000, 2000, 50, 'public/YsG0slbWgnd3zOn3mZuclV3zgkyrKpkWVzZy2DOc.jpeg', 2, 100, '2019-07-20 19:35:10', '2019-06-23 02:36:11'),
(22, 'Sarkar', 'sabuj@gmail.com', '01967890814', 'm', '1915-02-03', 'hbngthbgtynjh', 'drfvgbhbf', 'Abdul Kadir', 'Stock', '5', '2019-06-18', 1000000, 1500, 50, 'public/slA1pqmpq0a7FQJNKQvNjCCrdNJ60z9ZlQN1n63d.jpeg', 2, 100, '2019-07-20 19:35:15', '2019-06-23 02:51:45');

-- --------------------------------------------------------

--
-- Table structure for table `employee_expenses`
--

CREATE TABLE `employee_expenses` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `expenses_type` varchar(100) NOT NULL,
  `expenses_amount` int(11) NOT NULL,
  `expenses_details` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_expenses`
--

INSERT INTO `employee_expenses` (`id`, `employee_id`, `expenses_type`, `expenses_amount`, `expenses_details`, `created_at`, `updated_at`) VALUES
(48, 16, '0', 2000, 'bkjn', '2019-04-18 14:16:06', '2019-04-18 14:16:06'),
(49, 16, '1', 1000, 'sfrgdhfjgh', '2019-04-19 11:04:07', '2019-04-18 14:16:16'),
(50, 17, '1', 1000, 'fdsasdfadf', '2019-04-19 04:09:21', '2019-04-19 04:09:21'),
(51, 18, '1', 2000, 'j.lkjh', '2019-04-19 04:50:09', '2019-04-19 04:50:09'),
(52, 18, '0', 1000, 'dfsadsf', '2019-01-01 05:51:17', '2019-04-19 05:51:17');

-- --------------------------------------------------------

--
-- Table structure for table `expense_types`
--

CREATE TABLE `expense_types` (
  `id` int(11) NOT NULL,
  `type` enum('1','2','3','4','5','6') NOT NULL COMMENT '1.''Vehicle Fuel Cost'',2.''Generator Fuel Cost'',3.''Vehicle maintenance Cost'',4.''Computer Maintenance Cost'',5.''Furniture Maintenance cost'',6.''Total Rent''',
  `vehicle_no` int(11) DEFAULT NULL,
  `fuel_type` varchar(255) DEFAULT NULL,
  `purpose` varchar(5000) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `expense_types`
--

INSERT INTO `expense_types` (`id`, `type`, `vehicle_no`, `fuel_type`, `purpose`, `amount`, `created_at`, `updated_at`) VALUES
(3, '4', NULL, NULL, 'dd', 150, '2019-06-23 06:57:41', '2019-06-23 06:57:41'),
(4, '2', NULL, 'Kerosine', NULL, 1500, '2019-06-26 16:02:38', '2019-06-26 16:02:38'),
(6, '1', 455, NULL, NULL, 434, '2019-06-23 07:11:48', '2019-06-23 07:11:48'),
(7, '3', 55, NULL, 'food', 1000, '2019-06-26 16:04:20', '2019-06-26 16:04:20'),
(8, '2', NULL, 'ugyugu', NULL, 56, '2019-07-01 19:08:19', '2019-07-01 19:08:19'),
(9, '6', NULL, NULL, 'uhiugu', 6515, '2019-07-01 19:08:49', '2019-07-01 19:08:49'),
(10, '1', 3434, NULL, NULL, 2000, '2019-07-25 01:39:38', '2019-07-25 01:39:38'),
(11, '3', 3434, NULL, 'afds', 2500, '2019-07-25 01:39:55', '2019-07-25 01:39:55');

-- --------------------------------------------------------

--
-- Table structure for table `investments`
--

CREATE TABLE `investments` (
  `id` int(11) NOT NULL,
  `name` int(200) NOT NULL COMMENT '1:Mechanized Vehicle 2:Non Mechanized Vehicle 3: Computer Equipment 4:Office Furniture and Other Equipment 5: TABS',
  `life_in_year` int(11) NOT NULL,
  `life_in_month` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `investments`
--

INSERT INTO `investments` (`id`, `name`, `life_in_year`, `life_in_month`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 36, '2019-07-06 18:38:18', '2019-07-01 19:11:24'),
(2, 3, 2, 24, '2019-07-06 18:38:25', '2019-07-05 18:48:34'),
(3, 2, 5, 60, '2019-07-06 18:37:08', '2019-07-06 18:37:08'),
(4, 4, 10, 120, '2019-07-06 18:38:33', '2019-07-06 18:38:33'),
(5, 5, 2, 24, '2019-07-20 19:23:38', '2019-07-20 19:23:38');

-- --------------------------------------------------------

--
-- Table structure for table `investment_details`
--

CREATE TABLE `investment_details` (
  `id` int(11) NOT NULL,
  `investment_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `puchase_date` date NOT NULL,
  `unit_price` int(11) NOT NULL,
  `no_of_unit` int(11) NOT NULL,
  `total_cost` int(11) NOT NULL,
  `month_in_use` int(11) NOT NULL,
  `monthly_depcreciation` int(11) NOT NULL,
  `depreciation` int(11) NOT NULL,
  `balance_remain` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `investment_details`
--

INSERT INTO `investment_details` (`id`, `investment_id`, `name`, `puchase_date`, `unit_price`, `no_of_unit`, `total_cost`, `month_in_use`, `monthly_depcreciation`, `depreciation`, `balance_remain`, `created_at`, `updated_at`) VALUES
(1, 1, 'printer', '2019-07-01', 62000, 1, 62000, 26, 1722, 66, 61934, '2019-07-05 19:16:25', '2019-07-05 19:16:25'),
(2, 1, 'computer', '2019-07-01', 78, 77, 6006, 34, 167, 5, 6001, '2019-07-05 20:00:04', '2019-07-05 19:17:12'),
(3, 2, 'laptop', '2019-07-01', 46350, 2, 92700, 26, 2575, 66950, 25750, '2019-07-05 20:00:10', '2019-07-05 19:20:09'),
(4, 3, 'printer', '2019-07-01', 23333, 2, 46666, 23, 778, 17889, 28777, '2019-07-06 18:49:21', '2019-07-06 18:49:21'),
(5, 1, 'afds', '2019-07-02', 43324, 23, 996452, 234, 27679, 6476938, 0, '2019-07-06 19:36:57', '2019-07-06 19:36:57'),
(6, 4, 'wre', '2019-07-01', 20202, 2, 40404, 20, 337, 6734, 33670, '2019-07-06 19:37:07', '2019-07-06 19:37:07'),
(7, 4, 'dghfd', '2019-07-02', 20000, 2, 40000, 8, 333, 2667, 37333, '2019-07-06 19:37:24', '2019-07-06 19:37:24'),
(8, 5, 'dfs', '2019-07-01', 25, 20, 500, 20, 21, 417, 83, '2019-07-20 19:24:18', '2019-07-20 19:24:18');

-- --------------------------------------------------------

--
-- Table structure for table `leave_days`
--

CREATE TABLE `leave_days` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `leave_date` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leave_days`
--

INSERT INTO `leave_days` (`id`, `employee_id`, `leave_date`, `created_at`, `updated_at`) VALUES
(1, 16, '2019-03-22', '2019-03-22 04:22:02', '2019-04-23 16:28:31'),
(2, 17, '2019-03-26', '2019-03-26 11:29:25', '2019-04-23 16:28:38'),
(3, 16, '2019-03-26', '2019-03-26 11:29:44', '2019-04-23 16:28:42'),
(4, 18, '2019-03-26', '2019-03-26 11:30:14', '2019-04-23 16:29:56'),
(9, 18, '2019-03-30', '2019-03-30 13:30:33', '2019-04-23 16:30:41'),
(10, 17, '2019-04-20', '2019-04-20 00:58:53', '2019-04-20 00:58:53'),
(11, 16, '2019-04-20', '2019-04-20 01:07:21', '2019-04-20 01:07:21');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_12_17_230433_create_employees_table', 2),
(4, '2018_12_17_235018_create_employees_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `office_expenses`
--

CREATE TABLE `office_expenses` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `office_expenses`
--

INSERT INTO `office_expenses` (`id`, `name`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'Di', 123, '2019-02-06 11:21:25', '2019-02-06 11:21:25'),
(2, 'food', 1500, '2019-04-27 03:35:33', '0000-00-00 00:00:00'),
(3, 'pepsi', 200, '2019-04-27 03:35:33', '2019-04-26 21:43:57'),
(4, 'other', 100, '2019-07-20 19:21:58', '2019-07-20 19:21:58'),
(5, 'other', 4500, '2019-07-25 01:58:29', '2019-07-25 01:58:29');

-- --------------------------------------------------------

--
-- Table structure for table `off_days`
--

CREATE TABLE `off_days` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `off_date` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `off_days`
--

INSERT INTO `off_days` (`id`, `employee_id`, `off_date`, `created_at`, `updated_at`) VALUES
(6, 17, 'Friday', '2019-04-23 15:40:19', '2019-01-08 07:15:10'),
(7, 18, 'Sunday', '2019-04-23 15:40:43', '2019-01-19 03:23:47'),
(8, 16, 'Saturday', '2019-02-03 15:40:54', '2019-02-07 10:37:05'),
(9, 17, 'Monday', '2019-04-23 15:40:59', '2019-03-18 11:26:05'),
(11, 18, 'Wednessday', '2019-04-23 15:41:05', '2019-03-20 04:03:15'),
(12, 17, 'Wednessday', '2019-04-23 15:41:12', '2019-03-20 04:03:46'),
(13, 16, 'Wednessday', '2019-04-23 15:41:15', '2019-03-20 04:04:42'),
(14, 16, 'Thursday', '2019-04-23 15:41:22', '2019-03-21 03:38:01'),
(15, 17, 'Thursday', '2019-04-23 15:41:27', '2019-03-21 03:39:30');

-- --------------------------------------------------------

--
-- Table structure for table `other_infos`
--

CREATE TABLE `other_infos` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `info_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `other_infos`
--

INSERT INTO `other_infos` (`id`, `name`, `info_id`, `amount`, `created_at`, `updated_at`) VALUES
(1, 'landline_telephone_bills', 5, 2000, '2019-07-21 19:50:28', '2019-07-21 19:50:28'),
(2, 'internet_bills_for_office', 10, 1500, '2019-07-21 19:54:07', '2019-07-21 19:54:07'),
(3, 'investment_in_stock', 1, 2020, '2019-07-23 21:25:23', '2019-07-23 20:26:28'),
(4, 'investment_in_stock', 1, 200, '2019-07-23 20:34:42', '2019-07-23 20:34:42'),
(5, 'loading_unloading', 3, 2000, '2019-07-25 01:03:04', '2019-07-25 01:03:04'),
(6, 'head_load_delivery', 4, 4500, '2019-07-25 01:03:14', '2019-07-25 01:03:14'),
(7, 'landline_telephone_bills', 5, 4500, '2019-07-25 01:43:25', '2019-07-25 01:43:25'),
(8, 'mobile_phone_bills', 6, 1500, '2019-07-25 01:43:30', '2019-07-25 01:43:30'),
(9, 'wasa_bills', 7, 1000, '2019-07-25 01:43:36', '2019-07-25 01:43:36'),
(10, 'electricity_bills', 8, 2000, '2019-07-25 01:43:46', '2019-07-25 01:43:46'),
(11, 'gas_bills', 9, 1000, '2019-07-25 01:43:53', '2019-07-25 01:43:53'),
(12, 'internet_bills_for_hht', 11, 1500, '2019-07-25 01:44:00', '2019-07-25 01:44:00'),
(13, 'internet_bills_for_hht', 11, 1500, '2019-07-25 01:44:07', '2019-07-25 01:44:07'),
(14, 'internet_bills_for_office', 10, 1000, '2019-07-25 01:44:59', '2019-07-25 01:44:59');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_details` text,
  `trade_price` double NOT NULL,
  `product_sku_code` int(11) NOT NULL,
  `list_price` int(11) DEFAULT NULL,
  `mrp` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_name`, `product_details`, `trade_price`, `product_sku_code`, `list_price`, `mrp`, `created_at`, `updated_at`) VALUES
(5, 'lip jel', '10g', 7.25, 5969885, NULL, NULL, '2019-01-03 22:57:04', '2019-01-03 16:57:04'),
(6, 'mum waters', '5 ltr', 70, 1221, NULL, NULL, '2019-01-03 22:52:43', '2019-01-03 16:52:43'),
(7, 'Meril', 'Lip gel', 1500, 121, NULL, NULL, '2019-01-07 05:37:12', '2019-01-07 05:37:12'),
(8, 'frfr', 'ww', 132, 121, 123, 22, '2019-02-03 06:46:11', '2019-02-03 06:46:11');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `sku_code` int(11) NOT NULL,
  `sku` varchar(100) NOT NULL,
  `pack_size` int(11) NOT NULL,
  `ctn_sale` int(11) NOT NULL,
  `unit_sale` int(11) NOT NULL,
  `ctn_free` int(11) NOT NULL,
  `unit_free` int(11) NOT NULL,
  `ctn_return` int(11) NOT NULL,
  `unit_return` int(11) NOT NULL,
  `sales_value` float NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `employee_id`, `sku_code`, `sku`, `pack_size`, `ctn_sale`, `unit_sale`, `ctn_free`, `unit_free`, `ctn_return`, `unit_return`, `sales_value`, `created_at`, `updated_at`) VALUES
(1, 16, 67464688, 'WHEEL LAUNDRY SOAP 20TK 72X130G', 72, 0, 36, 0, 2, 0, 0, 669, '2019-07-20 19:21:06', '2019-07-20 19:21:06'),
(2, 16, 67637988, 'RIN STD PWD PWR BRT SI NAW 60TK 60X500G', 60, 1, 48, 0, 4, 0, 0, 5940, '2019-07-20 19:21:06', '2019-07-20 19:21:06'),
(3, 16, 67623345, 'SURF EXCEL STD PWDR S+ UL 9T 5TK 864X20G', 864, 0, 276, 0, 0, 0, 0, 1236.26, '2019-07-20 19:21:06', '2019-07-20 19:21:06'),
(4, 16, 67662906, 'SURF EXCEL STD PWDR SKY+ 99TK 12X500G', 12, 1, 9, 0, 0, 0, 0, 1871.1, '2019-07-20 19:21:06', '2019-07-20 19:21:06'),
(5, 16, 67657470, 'LIFEBUOY SKN BAR TOTAL RL 30TK 72X100G', 72, 0, 36, 0, 0, 0, 0, 993, '2019-07-20 19:21:06', '2019-07-20 19:21:06'),
(6, 16, 67722714, 'LIFEBUOY BAR LMON-FRSH 32TK 72X100G', 72, 0, 36, 0, 1, 0, 0, 1059, '2019-07-20 19:21:06', '2019-07-20 19:21:06'),
(7, 16, 67722776, 'LIFEBUOY SKIN BAR CARE 44TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 485, '2019-07-20 19:21:07', '2019-07-20 19:21:07'),
(8, 16, 67657494, 'LIFEBUOY SKN BAR TOTAL RL 42TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 463, '2019-07-20 19:21:07', '2019-07-20 19:21:07'),
(9, 16, 21162220, 'Lifebuoy lq hw total 200', 24, 0, 3, 0, 0, 0, 0, 238, '2019-07-20 19:21:07', '2019-07-20 19:21:07'),
(10, 16, 21007328, 'Vim abr powder 500g', 60, 0, 6, 0, 0, 0, 0, 162, '2019-07-20 19:21:07', '2019-07-20 19:21:07'),
(11, 16, 67469858, 'VIM HDW NSD BAR FLOW_WRAP T_10 72X100G', 72, 1, 12, 0, 0, 0, 0, 756, '2019-07-20 19:21:07', '2019-07-20 19:21:07'),
(12, 16, 67469864, 'VIM HDW NSD BAR FLOW_WRAP T_30 36X300G', 36, 0, 12, 0, 0, 0, 0, 324, '2019-07-20 19:21:07', '2019-07-20 19:21:07'),
(13, 16, 67562353, 'VIM HDW STD LIQ SPOUT PACK 20TK 36X100ML', 36, 0, 6, 0, 0, 0, 0, 110, '2019-07-20 19:21:07', '2019-07-20 19:21:07'),
(14, 16, 67660995, 'LUX SKIN BAR SFT TCH FW BLS 47TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 518, '2019-07-20 19:21:07', '2019-07-20 19:21:07'),
(15, 16, 67654896, 'BB TAAZA BLACK TEA RL 30TK 288X50G', 288, 0, 72, 0, 0, 0, 0, 1852.44, '2019-07-20 19:21:08', '2019-07-20 19:21:08'),
(16, 16, 67654887, 'BB TAAZA BLACK TEA 110TK 48X200G', 48, 0, 21, 0, 0, 0, 0, 2089.71, '2019-07-20 19:21:08', '2019-07-20 19:21:08'),
(17, 16, 67654890, 'BB TAAZA BLACK TEA RL 58TK 144X100G', 144, 0, 12, 0, 0, 0, 0, 623.84, '2019-07-20 19:21:08', '2019-07-20 19:21:08'),
(18, 16, 67699927, 'BB TAAZA BLACK TEA 215TK 24X400G', 24, 0, 10, 0, 0, 0, 0, 1935, '2019-07-20 19:21:08', '2019-07-20 19:21:08'),
(19, 16, 67461620, 'CLOSE-UP TH PST MINT BEYONC 40TK 72X45G', 72, 0, 18, 0, 0, 0, 0, 639, '2019-07-20 19:21:08', '2019-07-20 19:21:08'),
(20, 16, 67088334, 'PEPSODENT EXP-PRTCTN PRO-SENSTIV 36X140G', 36, 0, 3, 0, 0, 0, 0, 342, '2019-07-20 19:21:08', '2019-07-20 19:21:08'),
(21, 16, 67461882, 'PEPSODANT EXP-PRTCTN PRO-SENSITVE 72X90G', 72, 0, 3, 0, 0, 0, 0, 228.75, '2019-07-20 19:21:08', '2019-07-20 19:21:08'),
(22, 16, 67310105, 'PEPSODENT THPAST GC AGNI 35TK 72X45G', 72, 0, 6, 0, 0, 0, 0, 184, '2019-07-20 19:21:08', '2019-07-20 19:21:08'),
(23, 16, 67090451, 'SUNSILK SHMP BLK SHN SRNDPTY 2TK 864X6ML', 864, 0, 624, 0, 0, 0, 0, 1092, '2019-07-20 19:21:08', '2019-07-20 19:21:08'),
(24, 16, 67090439, 'SUNSILK SHAMP T&L SERENDIPTY 2TK 864X6ML', 864, 0, 408, 0, 10, 0, 0, 714, '2019-07-20 19:21:09', '2019-07-20 19:21:09'),
(25, 16, 67658905, 'SUNSILK SHAMPO HFS SDPTY 165TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 291.68, '2019-07-20 19:21:09', '2019-07-20 19:21:09'),
(26, 16, 67090444, 'SUNSILK SHAMP HFS SERENDIPTY 2TK 864X6ML', 864, 0, 192, 0, 4, 0, 0, 336, '2019-07-20 19:21:09', '2019-07-20 19:21:09'),
(27, 16, 67474215, 'CLEAR SHMPOO CAC OCEAN 3TK 864X5ML', 864, 0, 336, 0, 0, 0, 0, 877.23, '2019-07-20 19:21:09', '2019-07-20 19:21:09'),
(28, 16, 67379570, 'CLEAR SHAMPOO CAC OCEAN 95TK 48X90ML', 48, 0, 3, 0, 0, 0, 0, 250.5, '2019-07-20 19:21:09', '2019-07-20 19:21:09'),
(29, 16, 67379512, 'CLEAR SHAMPOO AHF OCEAN 190TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 336, '2019-07-20 19:21:09', '2019-07-20 19:21:09'),
(30, 16, 67535278, 'CLEAR SHAMPO MEN CSM OCEAN 4TK_864X5ML', 864, 0, 264, 0, 0, 0, 0, 912.99, '2019-07-20 19:21:09', '2019-07-20 19:21:09'),
(31, 16, 67470849, 'DOVE SHAMPOO HFR DELUXE 190TK 24X170ML', 24, 0, 2, 0, 0, 0, 0, 327.33, '2019-07-20 19:21:10', '2019-07-20 19:21:10'),
(32, 16, 67470855, 'DOVE SHAMPOO HFR DELUXE 4TK 864X6ML', 864, 0, 600, 0, 32, 0, 0, 2074.98, '2019-07-20 19:21:10', '2019-07-20 19:21:10'),
(33, 16, 67470838, 'DOVE SHAMPOO IRP DELUXE 4TK 864X6ML', 864, 0, 276, 0, 12, 0, 0, 954.49, '2019-07-20 19:21:10', '2019-07-20 19:21:10'),
(34, 16, 67110005, 'DOVE CONDITIONER IRP 4TK 864X7ML', 864, 0, 72, 0, 5, 0, 0, 250.5, '2019-07-20 19:21:10', '2019-07-20 19:21:10'),
(35, 16, 67575091, 'FAIR & LOVELY FW INSTA GLOW 75TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 182.41, '2019-07-20 19:21:10', '2019-07-20 19:21:10'),
(36, 16, 67605461, 'FAIR&LOVELY FM WINTER CREAM 65TK 72X25G', 72, 0, 3, 0, 0, 0, 0, 170.07, '2019-07-20 19:21:10', '2019-07-20 19:21:10'),
(37, 16, 67605458, 'FAIR&LOVELY FM WINTER CREAM 120TK 72X50G', 72, 0, 3, 0, 0, 0, 0, 314.25, '2019-07-20 19:21:10', '2019-07-20 19:21:10'),
(38, 16, 67608979, 'FAIR&LOVELY FC MVIT FIZZA 115TK 72X50G', 72, 0, 15, 0, 0, 0, 0, 1497.75, '2019-07-20 19:21:10', '2019-07-20 19:21:10'),
(39, 16, 67608976, 'FAIR&LOVELY FC MULTVT FIZA 60TK 72X25G', 72, 0, 24, 0, 0, 0, 0, 1256.88, '2019-07-20 19:21:10', '2019-07-20 19:21:10'),
(40, 16, 67564233, 'PONDS BODY LOTION MOIST 100TK 48X100ML', 48, 0, 6, 0, 0, 0, 0, 529.98, '2019-07-20 19:21:10', '2019-07-20 19:21:10'),
(41, 16, 67609012, 'PONDS BODY LOTION MOIST 165TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 435.45, '2019-07-20 19:21:10', '2019-07-20 19:21:10'),
(42, 16, 67579217, 'PONDS FW ESS CARE DALY 3DPAK 85TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 213.5, '2019-07-20 19:21:11', '2019-07-20 19:21:11'),
(43, 16, 67499032, 'PONDS WHITE BEUTY FACEWASH 99TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 246, '2019-07-20 19:21:11', '2019-07-20 19:21:11'),
(44, 16, 67295873, 'VASELINE PET JELLY ICON 15TK ENT 384X9ML', 384, 0, 48, 0, 0, 0, 0, 640, '2019-07-20 19:21:11', '2019-07-20 19:21:11'),
(45, 16, 67609060, 'VASELINE LOTION HLTHY WHT 190TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 497.16, '2019-07-20 19:21:11', '2019-07-20 19:21:11'),
(46, 20, 67464688, 'WHEEL LAUNDRY SOAP 20TK 72X130G', 72, 0, 36, 0, 2, 0, 0, 669, '2019-07-25 02:18:45', '2019-07-25 02:18:45'),
(47, 20, 67637988, 'RIN STD PWD PWR BRT SI NAW 60TK 60X500G', 60, 1, 48, 0, 4, 0, 0, 5940, '2019-07-25 02:18:46', '2019-07-25 02:18:46'),
(48, 20, 67623345, 'SURF EXCEL STD PWDR S+ UL 9T 5TK 864X20G', 864, 0, 276, 0, 0, 0, 0, 1236.26, '2019-07-25 02:18:46', '2019-07-25 02:18:46'),
(49, 20, 67662906, 'SURF EXCEL STD PWDR SKY+ 99TK 12X500G', 12, 1, 9, 0, 0, 0, 0, 1871.1, '2019-07-25 02:18:47', '2019-07-25 02:18:47'),
(50, 20, 67657470, 'LIFEBUOY SKN BAR TOTAL RL 30TK 72X100G', 72, 0, 36, 0, 0, 0, 0, 993, '2019-07-25 02:18:47', '2019-07-25 02:18:47'),
(51, 20, 67722714, 'LIFEBUOY BAR LMON-FRSH 32TK 72X100G', 72, 0, 36, 0, 1, 0, 0, 1059, '2019-07-25 02:18:48', '2019-07-25 02:18:48'),
(52, 20, 67722776, 'LIFEBUOY SKIN BAR CARE 44TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 485, '2019-07-25 02:18:48', '2019-07-25 02:18:48'),
(53, 20, 67657494, 'LIFEBUOY SKN BAR TOTAL RL 42TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 463, '2019-07-25 02:18:49', '2019-07-25 02:18:49'),
(54, 20, 21162220, 'Lifebuoy lq hw total 200', 24, 0, 3, 0, 0, 0, 0, 238, '2019-07-25 02:18:49', '2019-07-25 02:18:49'),
(55, 20, 21007328, 'Vim abr powder 500g', 60, 0, 6, 0, 0, 0, 0, 162, '2019-07-25 02:18:50', '2019-07-25 02:18:50'),
(56, 20, 67469858, 'VIM HDW NSD BAR FLOW_WRAP T_10 72X100G', 72, 1, 12, 0, 0, 0, 0, 756, '2019-07-25 02:18:50', '2019-07-25 02:18:50'),
(57, 20, 67469864, 'VIM HDW NSD BAR FLOW_WRAP T_30 36X300G', 36, 0, 12, 0, 0, 0, 0, 324, '2019-07-25 02:18:51', '2019-07-25 02:18:51'),
(58, 20, 67562353, 'VIM HDW STD LIQ SPOUT PACK 20TK 36X100ML', 36, 0, 6, 0, 0, 0, 0, 110, '2019-07-25 02:18:51', '2019-07-25 02:18:51'),
(59, 20, 67660995, 'LUX SKIN BAR SFT TCH FW BLS 47TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 518, '2019-07-25 02:18:52', '2019-07-25 02:18:52'),
(60, 20, 67654896, 'BB TAAZA BLACK TEA RL 30TK 288X50G', 288, 0, 72, 0, 0, 0, 0, 1852.44, '2019-07-25 02:18:52', '2019-07-25 02:18:52'),
(61, 20, 67654887, 'BB TAAZA BLACK TEA 110TK 48X200G', 48, 0, 21, 0, 0, 0, 0, 2089.71, '2019-07-25 02:18:52', '2019-07-25 02:18:52'),
(62, 20, 67654890, 'BB TAAZA BLACK TEA RL 58TK 144X100G', 144, 0, 12, 0, 0, 0, 0, 623.84, '2019-07-25 02:18:53', '2019-07-25 02:18:53'),
(63, 20, 67699927, 'BB TAAZA BLACK TEA 215TK 24X400G', 24, 0, 10, 0, 0, 0, 0, 1935, '2019-07-25 02:18:53', '2019-07-25 02:18:53'),
(64, 20, 67461620, 'CLOSE-UP TH PST MINT BEYONC 40TK 72X45G', 72, 0, 18, 0, 0, 0, 0, 639, '2019-07-25 02:18:54', '2019-07-25 02:18:54'),
(65, 20, 67088334, 'PEPSODENT EXP-PRTCTN PRO-SENSTIV 36X140G', 36, 0, 3, 0, 0, 0, 0, 342, '2019-07-25 02:18:54', '2019-07-25 02:18:54'),
(66, 20, 67461882, 'PEPSODANT EXP-PRTCTN PRO-SENSITVE 72X90G', 72, 0, 3, 0, 0, 0, 0, 228.75, '2019-07-25 02:18:54', '2019-07-25 02:18:54'),
(67, 20, 67310105, 'PEPSODENT THPAST GC AGNI 35TK 72X45G', 72, 0, 6, 0, 0, 0, 0, 184, '2019-07-25 02:18:54', '2019-07-25 02:18:54'),
(68, 20, 67090451, 'SUNSILK SHMP BLK SHN SRNDPTY 2TK 864X6ML', 864, 0, 624, 0, 0, 0, 0, 1092, '2019-07-25 02:18:55', '2019-07-25 02:18:55'),
(69, 20, 67090439, 'SUNSILK SHAMP T&L SERENDIPTY 2TK 864X6ML', 864, 0, 408, 0, 10, 0, 0, 714, '2019-07-25 02:18:55', '2019-07-25 02:18:55'),
(70, 20, 67658905, 'SUNSILK SHAMPO HFS SDPTY 165TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 291.68, '2019-07-25 02:18:56', '2019-07-25 02:18:56'),
(71, 20, 67090444, 'SUNSILK SHAMP HFS SERENDIPTY 2TK 864X6ML', 864, 0, 192, 0, 4, 0, 0, 336, '2019-07-25 02:18:56', '2019-07-25 02:18:56'),
(72, 20, 67474215, 'CLEAR SHMPOO CAC OCEAN 3TK 864X5ML', 864, 0, 336, 0, 0, 0, 0, 877.23, '2019-07-25 02:18:56', '2019-07-25 02:18:56'),
(73, 20, 67379570, 'CLEAR SHAMPOO CAC OCEAN 95TK 48X90ML', 48, 0, 3, 0, 0, 0, 0, 250.5, '2019-07-25 02:18:57', '2019-07-25 02:18:57'),
(74, 20, 67379512, 'CLEAR SHAMPOO AHF OCEAN 190TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 336, '2019-07-25 02:18:57', '2019-07-25 02:18:57'),
(75, 20, 67535278, 'CLEAR SHAMPO MEN CSM OCEAN 4TK_864X5ML', 864, 0, 264, 0, 0, 0, 0, 912.99, '2019-07-25 02:18:57', '2019-07-25 02:18:57'),
(76, 20, 67470849, 'DOVE SHAMPOO HFR DELUXE 190TK 24X170ML', 24, 0, 2, 0, 0, 0, 0, 327.33, '2019-07-25 02:18:58', '2019-07-25 02:18:58'),
(77, 20, 67470855, 'DOVE SHAMPOO HFR DELUXE 4TK 864X6ML', 864, 0, 600, 0, 32, 0, 0, 2074.98, '2019-07-25 02:18:58', '2019-07-25 02:18:58'),
(78, 20, 67470838, 'DOVE SHAMPOO IRP DELUXE 4TK 864X6ML', 864, 0, 276, 0, 12, 0, 0, 954.49, '2019-07-25 02:18:58', '2019-07-25 02:18:58'),
(79, 20, 67110005, 'DOVE CONDITIONER IRP 4TK 864X7ML', 864, 0, 72, 0, 5, 0, 0, 250.5, '2019-07-25 02:18:59', '2019-07-25 02:18:59'),
(80, 20, 67575091, 'FAIR & LOVELY FW INSTA GLOW 75TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 182.41, '2019-07-25 02:18:59', '2019-07-25 02:18:59'),
(81, 20, 67605461, 'FAIR&LOVELY FM WINTER CREAM 65TK 72X25G', 72, 0, 3, 0, 0, 0, 0, 170.07, '2019-07-25 02:18:59', '2019-07-25 02:18:59'),
(82, 20, 67605458, 'FAIR&LOVELY FM WINTER CREAM 120TK 72X50G', 72, 0, 3, 0, 0, 0, 0, 314.25, '2019-07-25 02:19:00', '2019-07-25 02:19:00'),
(83, 20, 67608979, 'FAIR&LOVELY FC MVIT FIZZA 115TK 72X50G', 72, 0, 15, 0, 0, 0, 0, 1497.75, '2019-07-25 02:19:00', '2019-07-25 02:19:00'),
(84, 20, 67608976, 'FAIR&LOVELY FC MULTVT FIZA 60TK 72X25G', 72, 0, 24, 0, 0, 0, 0, 1256.88, '2019-07-25 02:19:00', '2019-07-25 02:19:00'),
(85, 20, 67564233, 'PONDS BODY LOTION MOIST 100TK 48X100ML', 48, 0, 6, 0, 0, 0, 0, 529.98, '2019-07-25 02:19:00', '2019-07-25 02:19:00'),
(86, 20, 67609012, 'PONDS BODY LOTION MOIST 165TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 435.45, '2019-07-25 02:19:01', '2019-07-25 02:19:01'),
(87, 20, 67579217, 'PONDS FW ESS CARE DALY 3DPAK 85TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 213.5, '2019-07-25 02:19:01', '2019-07-25 02:19:01'),
(88, 20, 67499032, 'PONDS WHITE BEUTY FACEWASH 99TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 246, '2019-07-25 02:19:01', '2019-07-25 02:19:01'),
(89, 20, 67295873, 'VASELINE PET JELLY ICON 15TK ENT 384X9ML', 384, 0, 48, 0, 0, 0, 0, 640, '2019-07-25 02:19:01', '2019-07-25 02:19:01'),
(90, 20, 67609060, 'VASELINE LOTION HLTHY WHT 190TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 497.16, '2019-07-25 02:19:01', '2019-07-25 02:19:01'),
(91, 1, 67464688, 'WHEEL LAUNDRY SOAP 20TK 72X130G', 72, 0, 36, 0, 2, 0, 0, 669, '2019-07-25 02:20:02', '2019-07-25 02:20:02'),
(92, 1, 67637988, 'RIN STD PWD PWR BRT SI NAW 60TK 60X500G', 60, 1, 48, 0, 4, 0, 0, 5940, '2019-07-25 02:20:03', '2019-07-25 02:20:03'),
(93, 1, 67623345, 'SURF EXCEL STD PWDR S+ UL 9T 5TK 864X20G', 864, 0, 276, 0, 0, 0, 0, 1236.26, '2019-07-25 02:20:03', '2019-07-25 02:20:03'),
(94, 1, 67662906, 'SURF EXCEL STD PWDR SKY+ 99TK 12X500G', 12, 1, 9, 0, 0, 0, 0, 1871.1, '2019-07-25 02:20:03', '2019-07-25 02:20:03'),
(95, 1, 67657470, 'LIFEBUOY SKN BAR TOTAL RL 30TK 72X100G', 72, 0, 36, 0, 0, 0, 0, 993, '2019-07-25 02:20:04', '2019-07-25 02:20:04'),
(96, 1, 67722714, 'LIFEBUOY BAR LMON-FRSH 32TK 72X100G', 72, 0, 36, 0, 1, 0, 0, 1059, '2019-07-25 02:20:04', '2019-07-25 02:20:04'),
(97, 1, 67722776, 'LIFEBUOY SKIN BAR CARE 44TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 485, '2019-07-25 02:20:04', '2019-07-25 02:20:04'),
(98, 1, 67657494, 'LIFEBUOY SKN BAR TOTAL RL 42TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 463, '2019-07-25 02:20:05', '2019-07-25 02:20:05'),
(99, 1, 21162220, 'Lifebuoy lq hw total 200', 24, 0, 3, 0, 0, 0, 0, 238, '2019-07-25 02:20:06', '2019-07-25 02:20:06'),
(100, 1, 21007328, 'Vim abr powder 500g', 60, 0, 6, 0, 0, 0, 0, 162, '2019-07-25 02:20:08', '2019-07-25 02:20:08'),
(101, 1, 67469858, 'VIM HDW NSD BAR FLOW_WRAP T_10 72X100G', 72, 1, 12, 0, 0, 0, 0, 756, '2019-07-25 02:20:09', '2019-07-25 02:20:09'),
(102, 1, 67469864, 'VIM HDW NSD BAR FLOW_WRAP T_30 36X300G', 36, 0, 12, 0, 0, 0, 0, 324, '2019-07-25 02:20:10', '2019-07-25 02:20:10'),
(103, 1, 67562353, 'VIM HDW STD LIQ SPOUT PACK 20TK 36X100ML', 36, 0, 6, 0, 0, 0, 0, 110, '2019-07-25 02:20:12', '2019-07-25 02:20:12'),
(104, 1, 67660995, 'LUX SKIN BAR SFT TCH FW BLS 47TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 518, '2019-07-25 02:20:13', '2019-07-25 02:20:13'),
(105, 1, 67654896, 'BB TAAZA BLACK TEA RL 30TK 288X50G', 288, 0, 72, 0, 0, 0, 0, 1852.44, '2019-07-25 02:20:16', '2019-07-25 02:20:16'),
(106, 1, 67654887, 'BB TAAZA BLACK TEA 110TK 48X200G', 48, 0, 21, 0, 0, 0, 0, 2089.71, '2019-07-25 02:20:17', '2019-07-25 02:20:17'),
(107, 1, 67654890, 'BB TAAZA BLACK TEA RL 58TK 144X100G', 144, 0, 12, 0, 0, 0, 0, 623.84, '2019-07-25 02:20:20', '2019-07-25 02:20:20'),
(108, 1, 67699927, 'BB TAAZA BLACK TEA 215TK 24X400G', 24, 0, 10, 0, 0, 0, 0, 1935, '2019-07-25 02:20:22', '2019-07-25 02:20:22'),
(109, 1, 67461620, 'CLOSE-UP TH PST MINT BEYONC 40TK 72X45G', 72, 0, 18, 0, 0, 0, 0, 639, '2019-07-25 02:20:23', '2019-07-25 02:20:23'),
(110, 1, 67088334, 'PEPSODENT EXP-PRTCTN PRO-SENSTIV 36X140G', 36, 0, 3, 0, 0, 0, 0, 342, '2019-07-25 02:20:25', '2019-07-25 02:20:25'),
(111, 1, 67461882, 'PEPSODANT EXP-PRTCTN PRO-SENSITVE 72X90G', 72, 0, 3, 0, 0, 0, 0, 228.75, '2019-07-25 02:20:27', '2019-07-25 02:20:27'),
(112, 1, 67310105, 'PEPSODENT THPAST GC AGNI 35TK 72X45G', 72, 0, 6, 0, 0, 0, 0, 184, '2019-07-25 02:20:28', '2019-07-25 02:20:28'),
(113, 1, 67090451, 'SUNSILK SHMP BLK SHN SRNDPTY 2TK 864X6ML', 864, 0, 624, 0, 0, 0, 0, 1092, '2019-07-25 02:20:30', '2019-07-25 02:20:30'),
(114, 1, 67090439, 'SUNSILK SHAMP T&L SERENDIPTY 2TK 864X6ML', 864, 0, 408, 0, 10, 0, 0, 714, '2019-07-25 02:20:31', '2019-07-25 02:20:31'),
(115, 1, 67658905, 'SUNSILK SHAMPO HFS SDPTY 165TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 291.68, '2019-07-25 02:20:32', '2019-07-25 02:20:32'),
(116, 1, 67090444, 'SUNSILK SHAMP HFS SERENDIPTY 2TK 864X6ML', 864, 0, 192, 0, 4, 0, 0, 336, '2019-07-25 02:20:34', '2019-07-25 02:20:34'),
(117, 1, 67474215, 'CLEAR SHMPOO CAC OCEAN 3TK 864X5ML', 864, 0, 336, 0, 0, 0, 0, 877.23, '2019-07-25 02:20:36', '2019-07-25 02:20:36'),
(118, 1, 67379570, 'CLEAR SHAMPOO CAC OCEAN 95TK 48X90ML', 48, 0, 3, 0, 0, 0, 0, 250.5, '2019-07-25 02:20:38', '2019-07-25 02:20:38'),
(119, 1, 67379512, 'CLEAR SHAMPOO AHF OCEAN 190TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 336, '2019-07-25 02:20:38', '2019-07-25 02:20:38'),
(120, 1, 67535278, 'CLEAR SHAMPO MEN CSM OCEAN 4TK_864X5ML', 864, 0, 264, 0, 0, 0, 0, 912.99, '2019-07-25 02:20:42', '2019-07-25 02:20:42'),
(121, 1, 67470849, 'DOVE SHAMPOO HFR DELUXE 190TK 24X170ML', 24, 0, 2, 0, 0, 0, 0, 327.33, '2019-07-25 02:20:43', '2019-07-25 02:20:43'),
(122, 1, 67470855, 'DOVE SHAMPOO HFR DELUXE 4TK 864X6ML', 864, 0, 600, 0, 32, 0, 0, 2074.98, '2019-07-25 02:20:44', '2019-07-25 02:20:44'),
(123, 1, 67470838, 'DOVE SHAMPOO IRP DELUXE 4TK 864X6ML', 864, 0, 276, 0, 12, 0, 0, 954.49, '2019-07-25 02:20:45', '2019-07-25 02:20:45'),
(124, 1, 67110005, 'DOVE CONDITIONER IRP 4TK 864X7ML', 864, 0, 72, 0, 5, 0, 0, 250.5, '2019-07-25 02:20:48', '2019-07-25 02:20:48'),
(125, 1, 67575091, 'FAIR & LOVELY FW INSTA GLOW 75TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 182.41, '2019-07-25 02:20:50', '2019-07-25 02:20:50'),
(126, 1, 67605461, 'FAIR&LOVELY FM WINTER CREAM 65TK 72X25G', 72, 0, 3, 0, 0, 0, 0, 170.07, '2019-07-25 02:20:51', '2019-07-25 02:20:51'),
(127, 1, 67605458, 'FAIR&LOVELY FM WINTER CREAM 120TK 72X50G', 72, 0, 3, 0, 0, 0, 0, 314.25, '2019-07-25 02:20:51', '2019-07-25 02:20:51'),
(128, 1, 67608979, 'FAIR&LOVELY FC MVIT FIZZA 115TK 72X50G', 72, 0, 15, 0, 0, 0, 0, 1497.75, '2019-07-25 02:20:52', '2019-07-25 02:20:52'),
(129, 1, 67608976, 'FAIR&LOVELY FC MULTVT FIZA 60TK 72X25G', 72, 0, 24, 0, 0, 0, 0, 1256.88, '2019-07-25 02:20:52', '2019-07-25 02:20:52'),
(130, 1, 67564233, 'PONDS BODY LOTION MOIST 100TK 48X100ML', 48, 0, 6, 0, 0, 0, 0, 529.98, '2019-07-25 02:20:52', '2019-07-25 02:20:52'),
(131, 1, 67609012, 'PONDS BODY LOTION MOIST 165TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 435.45, '2019-07-25 02:20:53', '2019-07-25 02:20:53'),
(132, 1, 67579217, 'PONDS FW ESS CARE DALY 3DPAK 85TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 213.5, '2019-07-25 02:20:53', '2019-07-25 02:20:53'),
(133, 1, 67499032, 'PONDS WHITE BEUTY FACEWASH 99TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 246, '2019-07-25 02:20:53', '2019-07-25 02:20:53'),
(134, 1, 67295873, 'VASELINE PET JELLY ICON 15TK ENT 384X9ML', 384, 0, 48, 0, 0, 0, 0, 640, '2019-07-25 02:20:54', '2019-07-25 02:20:54'),
(135, 1, 67609060, 'VASELINE LOTION HLTHY WHT 190TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 497.16, '2019-07-25 02:20:54', '2019-07-25 02:20:54'),
(136, 17, 67464688, 'WHEEL LAUNDRY SOAP 20TK 72X130G', 72, 0, 36, 0, 2, 0, 0, 669, '2019-07-25 02:21:41', '2019-07-25 02:21:41'),
(137, 17, 67637988, 'RIN STD PWD PWR BRT SI NAW 60TK 60X500G', 60, 1, 48, 0, 4, 0, 0, 5940, '2019-07-25 02:21:42', '2019-07-25 02:21:42'),
(138, 17, 67623345, 'SURF EXCEL STD PWDR S+ UL 9T 5TK 864X20G', 864, 0, 276, 0, 0, 0, 0, 1236.26, '2019-07-25 02:21:42', '2019-07-25 02:21:42'),
(139, 17, 67662906, 'SURF EXCEL STD PWDR SKY+ 99TK 12X500G', 12, 1, 9, 0, 0, 0, 0, 1871.1, '2019-07-25 02:21:42', '2019-07-25 02:21:42'),
(140, 17, 67657470, 'LIFEBUOY SKN BAR TOTAL RL 30TK 72X100G', 72, 0, 36, 0, 0, 0, 0, 993, '2019-07-25 02:21:43', '2019-07-25 02:21:43'),
(141, 17, 67722714, 'LIFEBUOY BAR LMON-FRSH 32TK 72X100G', 72, 0, 36, 0, 1, 0, 0, 1059, '2019-07-25 02:21:43', '2019-07-25 02:21:43'),
(142, 17, 67722776, 'LIFEBUOY SKIN BAR CARE 44TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 485, '2019-07-25 02:21:43', '2019-07-25 02:21:43'),
(143, 17, 67657494, 'LIFEBUOY SKN BAR TOTAL RL 42TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 463, '2019-07-25 02:21:44', '2019-07-25 02:21:44'),
(144, 17, 21162220, 'Lifebuoy lq hw total 200', 24, 0, 3, 0, 0, 0, 0, 238, '2019-07-25 02:21:44', '2019-07-25 02:21:44'),
(145, 17, 21007328, 'Vim abr powder 500g', 60, 0, 6, 0, 0, 0, 0, 162, '2019-07-25 02:21:45', '2019-07-25 02:21:45'),
(146, 17, 67469858, 'VIM HDW NSD BAR FLOW_WRAP T_10 72X100G', 72, 1, 12, 0, 0, 0, 0, 756, '2019-07-25 02:21:45', '2019-07-25 02:21:45'),
(147, 17, 67469864, 'VIM HDW NSD BAR FLOW_WRAP T_30 36X300G', 36, 0, 12, 0, 0, 0, 0, 324, '2019-07-25 02:21:46', '2019-07-25 02:21:46'),
(148, 17, 67562353, 'VIM HDW STD LIQ SPOUT PACK 20TK 36X100ML', 36, 0, 6, 0, 0, 0, 0, 110, '2019-07-25 02:21:47', '2019-07-25 02:21:47'),
(149, 17, 67660995, 'LUX SKIN BAR SFT TCH FW BLS 47TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 518, '2019-07-25 02:21:47', '2019-07-25 02:21:47'),
(150, 17, 67654896, 'BB TAAZA BLACK TEA RL 30TK 288X50G', 288, 0, 72, 0, 0, 0, 0, 1852.44, '2019-07-25 02:21:47', '2019-07-25 02:21:47'),
(151, 17, 67654887, 'BB TAAZA BLACK TEA 110TK 48X200G', 48, 0, 21, 0, 0, 0, 0, 2089.71, '2019-07-25 02:21:47', '2019-07-25 02:21:47'),
(152, 17, 67654890, 'BB TAAZA BLACK TEA RL 58TK 144X100G', 144, 0, 12, 0, 0, 0, 0, 623.84, '2019-07-25 02:21:48', '2019-07-25 02:21:48'),
(153, 17, 67699927, 'BB TAAZA BLACK TEA 215TK 24X400G', 24, 0, 10, 0, 0, 0, 0, 1935, '2019-07-25 02:21:48', '2019-07-25 02:21:48'),
(154, 17, 67461620, 'CLOSE-UP TH PST MINT BEYONC 40TK 72X45G', 72, 0, 18, 0, 0, 0, 0, 639, '2019-07-25 02:21:49', '2019-07-25 02:21:49'),
(155, 17, 67088334, 'PEPSODENT EXP-PRTCTN PRO-SENSTIV 36X140G', 36, 0, 3, 0, 0, 0, 0, 342, '2019-07-25 02:21:49', '2019-07-25 02:21:49'),
(156, 17, 67461882, 'PEPSODANT EXP-PRTCTN PRO-SENSITVE 72X90G', 72, 0, 3, 0, 0, 0, 0, 228.75, '2019-07-25 02:21:50', '2019-07-25 02:21:50'),
(157, 17, 67310105, 'PEPSODENT THPAST GC AGNI 35TK 72X45G', 72, 0, 6, 0, 0, 0, 0, 184, '2019-07-25 02:21:50', '2019-07-25 02:21:50'),
(158, 17, 67090451, 'SUNSILK SHMP BLK SHN SRNDPTY 2TK 864X6ML', 864, 0, 624, 0, 0, 0, 0, 1092, '2019-07-25 02:21:51', '2019-07-25 02:21:51'),
(159, 17, 67090439, 'SUNSILK SHAMP T&L SERENDIPTY 2TK 864X6ML', 864, 0, 408, 0, 10, 0, 0, 714, '2019-07-25 02:21:51', '2019-07-25 02:21:51'),
(160, 17, 67658905, 'SUNSILK SHAMPO HFS SDPTY 165TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 291.68, '2019-07-25 02:21:51', '2019-07-25 02:21:51'),
(161, 17, 67090444, 'SUNSILK SHAMP HFS SERENDIPTY 2TK 864X6ML', 864, 0, 192, 0, 4, 0, 0, 336, '2019-07-25 02:21:52', '2019-07-25 02:21:52'),
(162, 17, 67474215, 'CLEAR SHMPOO CAC OCEAN 3TK 864X5ML', 864, 0, 336, 0, 0, 0, 0, 877.23, '2019-07-25 02:21:52', '2019-07-25 02:21:52'),
(163, 17, 67379570, 'CLEAR SHAMPOO CAC OCEAN 95TK 48X90ML', 48, 0, 3, 0, 0, 0, 0, 250.5, '2019-07-25 02:21:53', '2019-07-25 02:21:53'),
(164, 17, 67379512, 'CLEAR SHAMPOO AHF OCEAN 190TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 336, '2019-07-25 02:21:53', '2019-07-25 02:21:53'),
(165, 17, 67535278, 'CLEAR SHAMPO MEN CSM OCEAN 4TK_864X5ML', 864, 0, 264, 0, 0, 0, 0, 912.99, '2019-07-25 02:21:53', '2019-07-25 02:21:53'),
(166, 17, 67470849, 'DOVE SHAMPOO HFR DELUXE 190TK 24X170ML', 24, 0, 2, 0, 0, 0, 0, 327.33, '2019-07-25 02:21:53', '2019-07-25 02:21:53'),
(167, 17, 67470855, 'DOVE SHAMPOO HFR DELUXE 4TK 864X6ML', 864, 0, 600, 0, 32, 0, 0, 2074.98, '2019-07-25 02:21:54', '2019-07-25 02:21:54'),
(168, 17, 67470838, 'DOVE SHAMPOO IRP DELUXE 4TK 864X6ML', 864, 0, 276, 0, 12, 0, 0, 954.49, '2019-07-25 02:21:54', '2019-07-25 02:21:54'),
(169, 17, 67110005, 'DOVE CONDITIONER IRP 4TK 864X7ML', 864, 0, 72, 0, 5, 0, 0, 250.5, '2019-07-25 02:21:54', '2019-07-25 02:21:54'),
(170, 17, 67575091, 'FAIR & LOVELY FW INSTA GLOW 75TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 182.41, '2019-07-25 02:21:55', '2019-07-25 02:21:55'),
(171, 17, 67605461, 'FAIR&LOVELY FM WINTER CREAM 65TK 72X25G', 72, 0, 3, 0, 0, 0, 0, 170.07, '2019-07-25 02:21:55', '2019-07-25 02:21:55'),
(172, 17, 67605458, 'FAIR&LOVELY FM WINTER CREAM 120TK 72X50G', 72, 0, 3, 0, 0, 0, 0, 314.25, '2019-07-25 02:21:55', '2019-07-25 02:21:55'),
(173, 17, 67608979, 'FAIR&LOVELY FC MVIT FIZZA 115TK 72X50G', 72, 0, 15, 0, 0, 0, 0, 1497.75, '2019-07-25 02:21:55', '2019-07-25 02:21:55'),
(174, 17, 67608976, 'FAIR&LOVELY FC MULTVT FIZA 60TK 72X25G', 72, 0, 24, 0, 0, 0, 0, 1256.88, '2019-07-25 02:21:56', '2019-07-25 02:21:56'),
(175, 17, 67564233, 'PONDS BODY LOTION MOIST 100TK 48X100ML', 48, 0, 6, 0, 0, 0, 0, 529.98, '2019-07-25 02:21:57', '2019-07-25 02:21:57'),
(176, 17, 67609012, 'PONDS BODY LOTION MOIST 165TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 435.45, '2019-07-25 02:21:57', '2019-07-25 02:21:57'),
(177, 17, 67579217, 'PONDS FW ESS CARE DALY 3DPAK 85TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 213.5, '2019-07-25 02:21:57', '2019-07-25 02:21:57'),
(178, 17, 67499032, 'PONDS WHITE BEUTY FACEWASH 99TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 246, '2019-07-25 02:21:58', '2019-07-25 02:21:58'),
(179, 17, 67295873, 'VASELINE PET JELLY ICON 15TK ENT 384X9ML', 384, 0, 48, 0, 0, 0, 0, 640, '2019-07-25 02:22:00', '2019-07-25 02:22:00'),
(180, 17, 67609060, 'VASELINE LOTION HLTHY WHT 190TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 497.16, '2019-07-25 02:22:00', '2019-07-25 02:22:00'),
(181, 1, 67464688, 'WHEEL LAUNDRY SOAP 20TK 72X130G', 72, 0, 36, 0, 2, 0, 0, 669, '2019-07-25 02:22:12', '2019-07-25 02:22:12'),
(182, 1, 67637988, 'RIN STD PWD PWR BRT SI NAW 60TK 60X500G', 60, 1, 48, 0, 4, 0, 0, 5940, '2019-07-25 02:22:13', '2019-07-25 02:22:13'),
(183, 1, 67623345, 'SURF EXCEL STD PWDR S+ UL 9T 5TK 864X20G', 864, 0, 276, 0, 0, 0, 0, 1236.26, '2019-07-25 02:22:13', '2019-07-25 02:22:13'),
(184, 1, 67662906, 'SURF EXCEL STD PWDR SKY+ 99TK 12X500G', 12, 1, 9, 0, 0, 0, 0, 1871.1, '2019-07-25 02:22:14', '2019-07-25 02:22:14'),
(185, 1, 67657470, 'LIFEBUOY SKN BAR TOTAL RL 30TK 72X100G', 72, 0, 36, 0, 0, 0, 0, 993, '2019-07-25 02:22:14', '2019-07-25 02:22:14'),
(186, 1, 67722714, 'LIFEBUOY BAR LMON-FRSH 32TK 72X100G', 72, 0, 36, 0, 1, 0, 0, 1059, '2019-07-25 02:22:14', '2019-07-25 02:22:14'),
(187, 1, 67722776, 'LIFEBUOY SKIN BAR CARE 44TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 485, '2019-07-25 02:22:15', '2019-07-25 02:22:15'),
(188, 1, 67657494, 'LIFEBUOY SKN BAR TOTAL RL 42TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 463, '2019-07-25 02:22:15', '2019-07-25 02:22:15'),
(189, 1, 21162220, 'Lifebuoy lq hw total 200', 24, 0, 3, 0, 0, 0, 0, 238, '2019-07-25 02:22:16', '2019-07-25 02:22:16'),
(190, 1, 21007328, 'Vim abr powder 500g', 60, 0, 6, 0, 0, 0, 0, 162, '2019-07-25 02:22:16', '2019-07-25 02:22:16'),
(191, 1, 67469858, 'VIM HDW NSD BAR FLOW_WRAP T_10 72X100G', 72, 1, 12, 0, 0, 0, 0, 756, '2019-07-25 02:22:16', '2019-07-25 02:22:16'),
(192, 1, 67469864, 'VIM HDW NSD BAR FLOW_WRAP T_30 36X300G', 36, 0, 12, 0, 0, 0, 0, 324, '2019-07-25 02:22:17', '2019-07-25 02:22:17'),
(193, 1, 67562353, 'VIM HDW STD LIQ SPOUT PACK 20TK 36X100ML', 36, 0, 6, 0, 0, 0, 0, 110, '2019-07-25 02:22:17', '2019-07-25 02:22:17'),
(194, 1, 67660995, 'LUX SKIN BAR SFT TCH FW BLS 47TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 518, '2019-07-25 02:22:17', '2019-07-25 02:22:17'),
(195, 1, 67654896, 'BB TAAZA BLACK TEA RL 30TK 288X50G', 288, 0, 72, 0, 0, 0, 0, 1852.44, '2019-07-25 02:22:18', '2019-07-25 02:22:18'),
(196, 1, 67654887, 'BB TAAZA BLACK TEA 110TK 48X200G', 48, 0, 21, 0, 0, 0, 0, 2089.71, '2019-07-25 02:22:18', '2019-07-25 02:22:18'),
(197, 1, 67654890, 'BB TAAZA BLACK TEA RL 58TK 144X100G', 144, 0, 12, 0, 0, 0, 0, 623.84, '2019-07-25 02:22:18', '2019-07-25 02:22:18'),
(198, 1, 67699927, 'BB TAAZA BLACK TEA 215TK 24X400G', 24, 0, 10, 0, 0, 0, 0, 1935, '2019-07-25 02:22:19', '2019-07-25 02:22:19'),
(199, 1, 67461620, 'CLOSE-UP TH PST MINT BEYONC 40TK 72X45G', 72, 0, 18, 0, 0, 0, 0, 639, '2019-07-25 02:22:19', '2019-07-25 02:22:19'),
(200, 1, 67088334, 'PEPSODENT EXP-PRTCTN PRO-SENSTIV 36X140G', 36, 0, 3, 0, 0, 0, 0, 342, '2019-07-25 02:22:19', '2019-07-25 02:22:19'),
(201, 1, 67461882, 'PEPSODANT EXP-PRTCTN PRO-SENSITVE 72X90G', 72, 0, 3, 0, 0, 0, 0, 228.75, '2019-07-25 02:22:20', '2019-07-25 02:22:20'),
(202, 1, 67310105, 'PEPSODENT THPAST GC AGNI 35TK 72X45G', 72, 0, 6, 0, 0, 0, 0, 184, '2019-07-25 02:22:20', '2019-07-25 02:22:20'),
(203, 1, 67090451, 'SUNSILK SHMP BLK SHN SRNDPTY 2TK 864X6ML', 864, 0, 624, 0, 0, 0, 0, 1092, '2019-07-25 02:22:21', '2019-07-25 02:22:21'),
(204, 1, 67090439, 'SUNSILK SHAMP T&L SERENDIPTY 2TK 864X6ML', 864, 0, 408, 0, 10, 0, 0, 714, '2019-07-25 02:22:22', '2019-07-25 02:22:22'),
(205, 1, 67658905, 'SUNSILK SHAMPO HFS SDPTY 165TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 291.68, '2019-07-25 02:22:22', '2019-07-25 02:22:22'),
(206, 1, 67090444, 'SUNSILK SHAMP HFS SERENDIPTY 2TK 864X6ML', 864, 0, 192, 0, 4, 0, 0, 336, '2019-07-25 02:22:23', '2019-07-25 02:22:23'),
(207, 1, 67474215, 'CLEAR SHMPOO CAC OCEAN 3TK 864X5ML', 864, 0, 336, 0, 0, 0, 0, 877.23, '2019-07-25 02:22:23', '2019-07-25 02:22:23'),
(208, 1, 67379570, 'CLEAR SHAMPOO CAC OCEAN 95TK 48X90ML', 48, 0, 3, 0, 0, 0, 0, 250.5, '2019-07-25 02:22:23', '2019-07-25 02:22:23'),
(209, 1, 67379512, 'CLEAR SHAMPOO AHF OCEAN 190TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 336, '2019-07-25 02:22:23', '2019-07-25 02:22:23'),
(210, 1, 67535278, 'CLEAR SHAMPO MEN CSM OCEAN 4TK_864X5ML', 864, 0, 264, 0, 0, 0, 0, 912.99, '2019-07-25 02:22:24', '2019-07-25 02:22:24'),
(211, 1, 67470849, 'DOVE SHAMPOO HFR DELUXE 190TK 24X170ML', 24, 0, 2, 0, 0, 0, 0, 327.33, '2019-07-25 02:22:24', '2019-07-25 02:22:24'),
(212, 1, 67470855, 'DOVE SHAMPOO HFR DELUXE 4TK 864X6ML', 864, 0, 600, 0, 32, 0, 0, 2074.98, '2019-07-25 02:22:24', '2019-07-25 02:22:24'),
(213, 1, 67470838, 'DOVE SHAMPOO IRP DELUXE 4TK 864X6ML', 864, 0, 276, 0, 12, 0, 0, 954.49, '2019-07-25 02:22:25', '2019-07-25 02:22:25'),
(214, 1, 67110005, 'DOVE CONDITIONER IRP 4TK 864X7ML', 864, 0, 72, 0, 5, 0, 0, 250.5, '2019-07-25 02:22:25', '2019-07-25 02:22:25'),
(215, 1, 67575091, 'FAIR & LOVELY FW INSTA GLOW 75TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 182.41, '2019-07-25 02:22:26', '2019-07-25 02:22:26'),
(216, 1, 67605461, 'FAIR&LOVELY FM WINTER CREAM 65TK 72X25G', 72, 0, 3, 0, 0, 0, 0, 170.07, '2019-07-25 02:22:26', '2019-07-25 02:22:26'),
(217, 1, 67605458, 'FAIR&LOVELY FM WINTER CREAM 120TK 72X50G', 72, 0, 3, 0, 0, 0, 0, 314.25, '2019-07-25 02:22:26', '2019-07-25 02:22:26'),
(218, 1, 67608979, 'FAIR&LOVELY FC MVIT FIZZA 115TK 72X50G', 72, 0, 15, 0, 0, 0, 0, 1497.75, '2019-07-25 02:22:26', '2019-07-25 02:22:26'),
(219, 1, 67608976, 'FAIR&LOVELY FC MULTVT FIZA 60TK 72X25G', 72, 0, 24, 0, 0, 0, 0, 1256.88, '2019-07-25 02:22:27', '2019-07-25 02:22:27'),
(220, 1, 67564233, 'PONDS BODY LOTION MOIST 100TK 48X100ML', 48, 0, 6, 0, 0, 0, 0, 529.98, '2019-07-25 02:22:27', '2019-07-25 02:22:27'),
(221, 1, 67609012, 'PONDS BODY LOTION MOIST 165TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 435.45, '2019-07-25 02:22:27', '2019-07-25 02:22:27'),
(222, 1, 67579217, 'PONDS FW ESS CARE DALY 3DPAK 85TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 213.5, '2019-07-25 02:22:28', '2019-07-25 02:22:28'),
(223, 1, 67499032, 'PONDS WHITE BEUTY FACEWASH 99TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 246, '2019-07-25 02:22:28', '2019-07-25 02:22:28'),
(224, 1, 67295873, 'VASELINE PET JELLY ICON 15TK ENT 384X9ML', 384, 0, 48, 0, 0, 0, 0, 640, '2019-07-25 02:22:28', '2019-07-25 02:22:28'),
(225, 1, 67609060, 'VASELINE LOTION HLTHY WHT 190TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 497.16, '2019-07-25 02:22:28', '2019-07-25 02:22:28'),
(226, 1, 67464688, 'WHEEL LAUNDRY SOAP 20TK 72X130G', 72, 0, 36, 0, 2, 0, 0, 669, '2019-07-25 02:22:37', '2019-07-25 02:22:37'),
(227, 1, 67637988, 'RIN STD PWD PWR BRT SI NAW 60TK 60X500G', 60, 1, 48, 0, 4, 0, 0, 5940, '2019-07-25 02:22:38', '2019-07-25 02:22:38'),
(228, 1, 67623345, 'SURF EXCEL STD PWDR S+ UL 9T 5TK 864X20G', 864, 0, 276, 0, 0, 0, 0, 1236.26, '2019-07-25 02:22:39', '2019-07-25 02:22:39'),
(229, 1, 67662906, 'SURF EXCEL STD PWDR SKY+ 99TK 12X500G', 12, 1, 9, 0, 0, 0, 0, 1871.1, '2019-07-25 02:22:39', '2019-07-25 02:22:39'),
(230, 1, 67657470, 'LIFEBUOY SKN BAR TOTAL RL 30TK 72X100G', 72, 0, 36, 0, 0, 0, 0, 993, '2019-07-25 02:22:39', '2019-07-25 02:22:39'),
(231, 1, 67722714, 'LIFEBUOY BAR LMON-FRSH 32TK 72X100G', 72, 0, 36, 0, 1, 0, 0, 1059, '2019-07-25 02:22:40', '2019-07-25 02:22:40'),
(232, 1, 67722776, 'LIFEBUOY SKIN BAR CARE 44TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 485, '2019-07-25 02:22:40', '2019-07-25 02:22:40'),
(233, 1, 67657494, 'LIFEBUOY SKN BAR TOTAL RL 42TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 463, '2019-07-25 02:22:41', '2019-07-25 02:22:41'),
(234, 1, 21162220, 'Lifebuoy lq hw total 200', 24, 0, 3, 0, 0, 0, 0, 238, '2019-07-25 02:22:41', '2019-07-25 02:22:41'),
(235, 1, 21007328, 'Vim abr powder 500g', 60, 0, 6, 0, 0, 0, 0, 162, '2019-07-25 02:22:41', '2019-07-25 02:22:41'),
(236, 1, 67469858, 'VIM HDW NSD BAR FLOW_WRAP T_10 72X100G', 72, 1, 12, 0, 0, 0, 0, 756, '2019-07-25 02:22:42', '2019-07-25 02:22:42'),
(237, 1, 67469864, 'VIM HDW NSD BAR FLOW_WRAP T_30 36X300G', 36, 0, 12, 0, 0, 0, 0, 324, '2019-07-25 02:22:42', '2019-07-25 02:22:42'),
(238, 1, 67562353, 'VIM HDW STD LIQ SPOUT PACK 20TK 36X100ML', 36, 0, 6, 0, 0, 0, 0, 110, '2019-07-25 02:22:43', '2019-07-25 02:22:43'),
(239, 1, 67660995, 'LUX SKIN BAR SFT TCH FW BLS 47TK 72X150G', 72, 0, 12, 0, 0, 0, 0, 518, '2019-07-25 02:22:43', '2019-07-25 02:22:43'),
(240, 1, 67654896, 'BB TAAZA BLACK TEA RL 30TK 288X50G', 288, 0, 72, 0, 0, 0, 0, 1852.44, '2019-07-25 02:22:44', '2019-07-25 02:22:44'),
(241, 1, 67654887, 'BB TAAZA BLACK TEA 110TK 48X200G', 48, 0, 21, 0, 0, 0, 0, 2089.71, '2019-07-25 02:22:44', '2019-07-25 02:22:44'),
(242, 1, 67654890, 'BB TAAZA BLACK TEA RL 58TK 144X100G', 144, 0, 12, 0, 0, 0, 0, 623.84, '2019-07-25 02:22:45', '2019-07-25 02:22:45'),
(243, 1, 67699927, 'BB TAAZA BLACK TEA 215TK 24X400G', 24, 0, 10, 0, 0, 0, 0, 1935, '2019-07-25 02:22:45', '2019-07-25 02:22:45'),
(244, 1, 67461620, 'CLOSE-UP TH PST MINT BEYONC 40TK 72X45G', 72, 0, 18, 0, 0, 0, 0, 639, '2019-07-25 02:22:45', '2019-07-25 02:22:45'),
(245, 1, 67088334, 'PEPSODENT EXP-PRTCTN PRO-SENSTIV 36X140G', 36, 0, 3, 0, 0, 0, 0, 342, '2019-07-25 02:22:46', '2019-07-25 02:22:46'),
(246, 1, 67461882, 'PEPSODANT EXP-PRTCTN PRO-SENSITVE 72X90G', 72, 0, 3, 0, 0, 0, 0, 228.75, '2019-07-25 02:22:46', '2019-07-25 02:22:46'),
(247, 1, 67310105, 'PEPSODENT THPAST GC AGNI 35TK 72X45G', 72, 0, 6, 0, 0, 0, 0, 184, '2019-07-25 02:22:46', '2019-07-25 02:22:46'),
(248, 1, 67090451, 'SUNSILK SHMP BLK SHN SRNDPTY 2TK 864X6ML', 864, 0, 624, 0, 0, 0, 0, 1092, '2019-07-25 02:22:47', '2019-07-25 02:22:47'),
(249, 1, 67090439, 'SUNSILK SHAMP T&L SERENDIPTY 2TK 864X6ML', 864, 0, 408, 0, 10, 0, 0, 714, '2019-07-25 02:22:47', '2019-07-25 02:22:47'),
(250, 1, 67658905, 'SUNSILK SHAMPO HFS SDPTY 165TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 291.68, '2019-07-25 02:22:48', '2019-07-25 02:22:48'),
(251, 1, 67090444, 'SUNSILK SHAMP HFS SERENDIPTY 2TK 864X6ML', 864, 0, 192, 0, 4, 0, 0, 336, '2019-07-25 02:22:48', '2019-07-25 02:22:48'),
(252, 1, 67474215, 'CLEAR SHMPOO CAC OCEAN 3TK 864X5ML', 864, 0, 336, 0, 0, 0, 0, 877.23, '2019-07-25 02:22:49', '2019-07-25 02:22:49'),
(253, 1, 67379570, 'CLEAR SHAMPOO CAC OCEAN 95TK 48X90ML', 48, 0, 3, 0, 0, 0, 0, 250.5, '2019-07-25 02:22:49', '2019-07-25 02:22:49'),
(254, 1, 67379512, 'CLEAR SHAMPOO AHF OCEAN 190TK 24X180ML', 24, 0, 2, 0, 0, 0, 0, 336, '2019-07-25 02:22:50', '2019-07-25 02:22:50'),
(255, 1, 67535278, 'CLEAR SHAMPO MEN CSM OCEAN 4TK_864X5ML', 864, 0, 264, 0, 0, 0, 0, 912.99, '2019-07-25 02:22:50', '2019-07-25 02:22:50'),
(256, 1, 67470849, 'DOVE SHAMPOO HFR DELUXE 190TK 24X170ML', 24, 0, 2, 0, 0, 0, 0, 327.33, '2019-07-25 02:22:50', '2019-07-25 02:22:50'),
(257, 1, 67470855, 'DOVE SHAMPOO HFR DELUXE 4TK 864X6ML', 864, 0, 600, 0, 32, 0, 0, 2074.98, '2019-07-25 02:22:51', '2019-07-25 02:22:51'),
(258, 1, 67470838, 'DOVE SHAMPOO IRP DELUXE 4TK 864X6ML', 864, 0, 276, 0, 12, 0, 0, 954.49, '2019-07-25 02:22:51', '2019-07-25 02:22:51'),
(259, 1, 67110005, 'DOVE CONDITIONER IRP 4TK 864X7ML', 864, 0, 72, 0, 5, 0, 0, 250.5, '2019-07-25 02:22:51', '2019-07-25 02:22:51'),
(260, 1, 67575091, 'FAIR & LOVELY FW INSTA GLOW 75TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 182.41, '2019-07-25 02:22:52', '2019-07-25 02:22:52'),
(261, 1, 67605461, 'FAIR&LOVELY FM WINTER CREAM 65TK 72X25G', 72, 0, 3, 0, 0, 0, 0, 170.07, '2019-07-25 02:22:53', '2019-07-25 02:22:53'),
(262, 1, 67605458, 'FAIR&LOVELY FM WINTER CREAM 120TK 72X50G', 72, 0, 3, 0, 0, 0, 0, 314.25, '2019-07-25 02:22:53', '2019-07-25 02:22:53'),
(263, 1, 67608979, 'FAIR&LOVELY FC MVIT FIZZA 115TK 72X50G', 72, 0, 15, 0, 0, 0, 0, 1497.75, '2019-07-25 02:22:53', '2019-07-25 02:22:53'),
(264, 1, 67608976, 'FAIR&LOVELY FC MULTVT FIZA 60TK 72X25G', 72, 0, 24, 0, 0, 0, 0, 1256.88, '2019-07-25 02:22:54', '2019-07-25 02:22:54'),
(265, 1, 67564233, 'PONDS BODY LOTION MOIST 100TK 48X100ML', 48, 0, 6, 0, 0, 0, 0, 529.98, '2019-07-25 02:22:55', '2019-07-25 02:22:55'),
(266, 1, 67609012, 'PONDS BODY LOTION MOIST 165TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 435.45, '2019-07-25 02:22:55', '2019-07-25 02:22:55'),
(267, 1, 67579217, 'PONDS FW ESS CARE DALY 3DPAK 85TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 213.5, '2019-07-25 02:22:56', '2019-07-25 02:22:56'),
(268, 1, 67499032, 'PONDS WHITE BEUTY FACEWASH 99TK 48X50G', 48, 0, 3, 0, 0, 0, 0, 246, '2019-07-25 02:22:56', '2019-07-25 02:22:56'),
(269, 1, 67295873, 'VASELINE PET JELLY ICON 15TK ENT 384X9ML', 384, 0, 48, 0, 0, 0, 0, 640, '2019-07-25 02:22:57', '2019-07-25 02:22:57'),
(270, 1, 67609060, 'VASELINE LOTION HLTHY WHT 190TK 24X200ML', 24, 0, 3, 0, 0, 0, 0, 497.16, '2019-07-25 02:22:57', '2019-07-25 02:22:57');

-- --------------------------------------------------------

--
-- Table structure for table `sale_values`
--

CREATE TABLE `sale_values` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `cash_issue` float NOT NULL,
  `free` float NOT NULL,
  `discount` float NOT NULL,
  `promo_discount` float NOT NULL,
  `credit_given` float NOT NULL,
  `prev_credit_collected` float NOT NULL,
  `expenses` int(11) NOT NULL,
  `market_return` float NOT NULL,
  `store_discount` float NOT NULL,
  `cash_to_be_deposite` float NOT NULL,
  `cash_deposited` int(11) NOT NULL,
  `cash_excess` int(11) NOT NULL DEFAULT '0',
  `cash_short` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sale_values`
--

INSERT INTO `sale_values` (`id`, `employee_id`, `cash_issue`, `free`, `discount`, `promo_discount`, `credit_given`, `prev_credit_collected`, `expenses`, `market_return`, `store_discount`, `cash_to_be_deposite`, `cash_deposited`, `cash_excess`, `cash_short`, `created_at`, `updated_at`) VALUES
(1, 16, 37600.9, 480.64, 322, 0, 0, 0, 0, 0, 0, 36798.2, 35000, 0, 1798, '2019-07-20 19:21:11', '2019-07-20 19:21:11'),
(2, 20, 37600.9, 480.64, 322, 0, 0, 0, 0, 0, 0, 36798.2, 5000000, 4963202, 0, '2019-07-25 02:19:02', '2019-07-25 02:19:02'),
(3, 1, 3601600, 480.64, 322, 0, 0, 0, 0, 0, 0, 3600800, 2500000, 0, 1100798, '2019-07-25 02:20:54', '2019-07-25 02:20:54'),
(4, 17, 3601600, 480.64, 322, 0, 0, 0, 0, 0, 0, 3600800, 4500, 0, 3596298, '2019-07-25 02:22:02', '2019-07-25 02:22:02'),
(5, 1, 3601600, 480.64, 322, 0, 0, 0, 0, 0, 0, 3600800, 450000, 0, 3150798, '2019-07-25 02:22:29', '2019-07-25 02:22:29'),
(6, 1, 35000, 480.64, 322, 0, 0, 0, 0, 0, 0, 360000, 450000, 0, 359550798, '2019-07-25 02:32:37', '2019-07-25 02:22:57');

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `sku_code` int(11) NOT NULL,
  `sku_name` varchar(100) NOT NULL,
  `pack_size` int(11) NOT NULL,
  `available_ctn` int(11) NOT NULL,
  `available_pcs` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`id`, `sku_code`, `sku_name`, `pack_size`, `available_ctn`, `available_pcs`, `created_at`, `updated_at`) VALUES
(1, 210073283, 'lux', 60, 2770, 100, '2019-03-28 19:18:32', '2019-03-28 13:12:41'),
(2, 21113459, 'KNORR SOUP THAI TOMATO T_40 RL 48X28G', 48, 341, 0, '2019-03-28 19:18:47', '2019-03-28 13:18:47'),
(3, 21119884, 'LIFEBUOY LQ HW CARE KALAL T_55 24X180ML', 24, 913, 0, '2019-03-28 19:18:47', '2019-03-28 13:18:47'),
(4, 21119881, 'LIFEBUOY LQ HW TOTAL KALAL T_55 24X180ML', 24, 198, 0, '2019-03-28 19:18:47', '2019-03-28 13:18:47'),
(5, 21162221, 'LIFEBUOY LQ HW CARE KALAL T_90 24X200ML', 24, 627, 0, '2019-03-28 19:18:47', '2019-03-28 13:18:47'),
(6, 21162220, 'LIFEBUOY LQ HW TOTAL KALAL T_90 24X200ML', 24, 1397, -30, '2019-07-25 02:22:41', '2019-07-25 02:22:41'),
(7, 67049133, 'KNORR SOUP HOT & SOUR 48X31G', 48, 55, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(8, 67043747, 'LIFEBUOY LQ HW TOTAL KALAL TK1100 4X5L', 4, 66, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(9, 67088334, 'PEPSODENT EXP-PRTCTN PRO-SENSTIV 36X140G', 36, 187, -30, '2019-07-25 02:22:46', '2019-07-25 02:22:46'),
(10, 67087759, 'REXONA MEN AP ROLL-ON QUANTUM 24X50ML', 24, 231, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(11, 67087760, 'REXONA MEN AP ROLL-ON QUANTUM 24X25ML', 24, 396, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(12, 67087756, 'REXONA MEN AP ROLL-ON V8 24X50ML', 24, 165, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(13, 67090439, 'SUNSILK SHAMP T&L SERENDIPTY 2TK 864X6ML', 864, 5357, -4180, '2019-07-25 02:22:48', '2019-07-25 02:22:48'),
(14, 67090444, 'SUNSILK SHAMP HFS SERENDIPTY 2TK 864X6ML', 864, 2024, -1960, '2019-07-25 02:22:49', '2019-07-25 02:22:49'),
(15, 67090451, 'SUNSILK SHMP BLK SHN SRNDPTY 2TK 864X6ML', 864, 9779, -6240, '2019-07-25 02:22:47', '2019-07-25 02:22:47'),
(16, 67090454, 'SUNSILK SHMP BLK SHN SDPTY 1TK 864X3.5ML', 864, 726, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(17, 67090459, 'SUNSILK COND HFS SERENDIPTY 3TK 864X7ML', 864, 3476, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(18, 67090678, 'SUNSILK SHMP BLK SHNE SDPTY 75TK 48X90ML', 48, 308, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(19, 67110005, 'DOVE CONDITIONER IRP 4TK 864X7ML', 864, 209, -770, '2019-07-25 02:22:52', '2019-07-25 02:22:52'),
(20, 67091394, 'DOVE SHAMPO OXYGEN MOISTURE DEMI 864X6ML', 864, 330, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(21, 67164005, 'DOVE SHAMP HAIRFLL RSCUE THRAPY 12X650ML', 12, 231, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(22, 67164002, 'DOVE SHAMP INTNS REPAIR THERAPY 12X650ML', 12, 231, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(23, 67230127, 'AXE BODY PERFUME SUAVE 12X122ML', 12, 44, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(24, 67230134, 'AXE BODY PERFUME MYSTERIOUS 12X122ML', 12, 88, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(25, 67230125, 'AXE BODY PERFUME INTENSE 12X122ML', 12, 66, 0, '2019-03-28 19:18:48', '2019-03-28 13:18:48'),
(26, 67257039, 'CLOSE-UP TH PST MINT BYONCE 6TK 480X10G', 480, 869, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(27, 67282784, 'SUNSILK COND PR-STRT SDPTY 65TK 48X80ML', 48, 110, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(28, 67310105, 'PEPSODENT THPAST GC AGNI 35TK 72X45G', 72, 3927, -60, '2019-07-25 02:22:47', '2019-07-25 02:22:47'),
(29, 67310125, 'PEPSODENT THPASTE GC AGNI 70TK 72X100G', 72, 902, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(30, 67310119, 'PEPSODENT THPAST GC AGNI 120TK 36X200G', 36, 2596, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(31, 67319402, 'PEPSODENT TOOTHPOWDER AGNI 40TK 72X100G', 72, 352, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(32, 67379700, 'CLEAR SHAMPO MEN OCEAN 205TK 24X180ML', 24, 352, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(33, 67379512, 'CLEAR SHAMPOO AHF OCEAN 190TK 24X180ML', 24, 99, -20, '2019-07-25 02:22:50', '2019-07-25 02:22:50'),
(34, 67379748, 'CLEAR SHAMPO MEN CSM OCEAN 99TK 48X80ML', 48, 143, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(35, 67379563, 'CLEAR SHMP CAC OCEAN 190TK 24X180ML', 24, 220, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(36, 67404707, 'SUNSILK SHMP HIJAB RCHRG REF 3TK 864X6ML', 864, 220, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(37, 67457568, 'LUX SKN CLN BAR CRMNG MGNLA 48TK 48X125G', 48, 55, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(38, 67457551, 'LUX SKN CLN BAR ICONC IRIS 48TK 48X125G', 48, 99, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(39, 67461540, 'CLOSE-UP TH PST MINT BYONC 80TK 72X100G', 72, 737, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(40, 67461531, 'CLOSE-UP TH PST MINT BYONC 110TK 36X145G', 36, 1892, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(41, 67461620, 'CLOSE-UP TH PST MINT BEYONC 40TK 72X45G', 72, 4851, -180, '2019-07-25 02:22:46', '2019-07-25 02:22:46'),
(42, 67461585, 'CLOSE-UP THPST REDHOT 110TK BYNC 36X145G', 36, 110, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(43, 67464688, 'WHEEL LAUNDRY SOAP 20TK 72X130G', 72, 27632, -380, '2019-07-25 02:22:38', '2019-07-25 02:22:38'),
(44, 67469858, 'VIM HDW NSD BAR FLOW_WRAP T_10 72X100G', 72, 25895, -120, '2019-07-25 02:22:42', '2019-07-25 02:22:42'),
(45, 67469864, 'VIM HDW NSD BAR FLOW_WRAP T_30 36X300G', 36, 19426, -120, '2019-07-25 02:22:43', '2019-07-25 02:22:43'),
(46, 67474208, 'CLEAR SHMPOO AHF OCEAN 3TK 864X5ML', 864, 495, 0, '2019-03-28 19:18:49', '2019-03-28 13:18:49'),
(47, 67474215, 'CLEAR SHMPOO CAC OCEAN 3TK 864X5ML', 864, 1001, -3360, '2019-07-25 02:22:49', '2019-07-25 02:22:49'),
(48, 67470860, 'DOVE SHAMPOO NOC DELUXE 330TK 24X340ML', 24, 110, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(49, 67470838, 'DOVE SHAMPOO IRP DELUXE 4TK 864X6ML', 864, 2475, -2880, '2019-07-25 02:22:51', '2019-07-25 02:22:51'),
(50, 67470855, 'DOVE SHAMPOO HFR DELUXE 4TK 864X6ML', 864, 2684, -6320, '2019-07-25 02:22:51', '2019-07-25 02:22:51'),
(51, 67470887, 'DOVE ENV DFNC SAMPO DETOX 350TK 24X340ML', 24, 99, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(52, 67470895, 'DOVE ENV DFNC SAMPO DETOX 200TK 24X170ML', 24, 176, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(53, 67476141, 'AXE MALE DEO AERO YOU POL 24X150ML', 24, 44, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(54, 67476090, 'AXE MALE DEO AERO DARK TEMPT 24X150ML', 24, 66, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(55, 67476083, 'AXE MALE DEO AERO GOLD TEMPT 24X150ML', 24, 22, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(56, 67476140, 'AXE MALE DEO AERO BLACK POL 24X150ML', 24, 66, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(57, 67461854, 'AYUSH FACE WASH N FAIR SAFFRON 24X80G', 24, 44, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(58, 67461846, 'AYUSH SHMP L&S GROWTH SHIKAKAI 12X175ML', 12, 11, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(59, 67461860, 'AYUSH SHAMPOO AHF BHRINGRAJ 12X175ML', 12, 22, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(60, 67498699, 'DOVE COND INTNS REPIR DAMG THPY 24X190ML', 24, 55, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(61, 67498700, 'DOVE CONDITNER HAIRFALL RESCUE 24X190ML', 24, 44, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(62, 67506713, 'VIM HDW STD LIQUID PRO EVO 95TK 12X500ML', 12, 24552, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(63, 67474245, 'CLEAR SHAMPOO CAC OCEAN 330TK 24X350ML', 24, 143, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(64, 67474237, 'CLEAR SHAMPO MEN OCEAN 340TK 24X330ML', 24, 209, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(65, 67474241, 'CLEAR SHAMPOO AHF OCEAN 330TK 24X350ML', 24, 132, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(66, 67499060, 'PONDS WHITE BEAUTY FWASH LOCAL 48X100G', 48, 451, 0, '2019-03-28 19:18:50', '2019-03-28 13:18:50'),
(67, 67499032, 'PONDS WHITE BEUTY FACEWASH 99TK 48X50G', 48, 627, -30, '2019-07-25 02:22:56', '2019-07-25 02:22:56'),
(68, 67487212, 'REXONA FEMAL AP ROLL ON PWDR DRY 24X25ML', 24, 110, 0, '2019-03-28 19:18:51', '2019-03-28 13:18:51'),
(69, 67487252, 'REXONA FML DEO ROLL ON PWDR DRY 24X50ML', 24, 143, 0, '2019-03-28 19:18:51', '2019-03-28 13:18:51'),
(70, 67488406, 'FAIR&LOVELY FC MENZ BIG NOSE 15TK 576X9G', 576, 187, 0, '2019-03-28 19:18:51', '2019-03-28 13:18:51'),
(71, 67488380, 'FAIR&LOVELY FW MENZ FALCON 150TK 48X100G', 48, 99, 0, '2019-03-28 19:18:51', '2019-03-28 13:18:51'),
(72, 67488374, 'FAIR&LOVELY FW MENZ FALCON 80TK 48X50G', 48, 363, 0, '2019-03-28 19:18:51', '2019-03-28 13:18:51'),
(73, 67488396, 'FAIR&LOVELY CM MEN RL ANT MK 70TK 72X25G', 72, 803, 0, '2019-03-28 19:18:51', '2019-03-28 13:18:51'),
(74, 67488385, 'FAIR&LOVELY CM MEN ANTMK 125TK 72X50G', 72, 242, 0, '2019-03-28 19:18:51', '2019-03-28 13:18:51'),
(75, 67495490, 'LIFEBUOY LQ HW LEMON FRESH 55TK 24X180ML', 24, 770, 0, '2019-03-28 19:18:51', '2019-03-28 13:18:51'),
(76, 67495534, 'LIFEBUOY LQ HW LEMON FRESH 90TK 24X200ML', 24, 330, 0, '2019-03-28 19:18:51', '2019-03-28 13:18:51'),
(77, 67535278, 'CLEAR SHAMPO MEN CSM OCEAN 4TK_864X5ML', 864, 4895, -2640, '2019-07-25 02:22:50', '2019-07-25 02:22:50'),
(78, 67540932, 'LUX SKN CLN BAR SNSUS SNDL 45TK 48X125G', 48, 1177, 0, '2019-03-28 19:18:51', '2019-03-28 13:18:51'),
(79, 67540994, 'LUX SKN CLN BAR SNSUS SNDL 30TK 90X75G', 90, 506, 0, '2019-03-28 19:18:51', '2019-03-28 13:18:51'),
(80, 67562353, 'VIM HDW STD LIQ SPOUT PACK 20TK 36X100ML', 36, 3421, -60, '2019-07-25 02:22:43', '2019-07-25 02:22:43'),
(81, 67564427, 'LUX SKIN BAR SFT TCH FW BLS 10TK 144X35G', 144, 4059, 0, '2019-03-28 19:18:52', '2019-03-28 13:18:52'),
(82, 67577931, 'FAIR & LOVELY FC MST MVIT CRM NTM 36X80G', 36, 924, 0, '2019-03-28 19:18:52', '2019-03-28 13:18:52'),
(83, 67575126, 'FAIR&LOVELY FW INSTA GLOW 140TK 48X100G', 48, 209, 0, '2019-03-28 19:18:52', '2019-03-28 13:18:52'),
(84, 67575131, 'FAIR&LOVELY FW ANTI PMPLE 150TK 48X100G', 48, 55, 0, '2019-03-28 19:18:52', '2019-03-28 13:18:52'),
(85, 67575137, 'FAIR & LOVELY FW AYURVEDA 150TK 48X100G', 48, 143, 0, '2019-03-28 19:18:52', '2019-03-28 13:18:52'),
(86, 67575091, 'FAIR & LOVELY FW INSTA GLOW 75TK 48X50G', 48, 792, -30, '2019-07-25 02:22:52', '2019-07-25 02:22:52'),
(87, 67603376, 'LUX SKN BAR SF TCH FL BLSS 34TK 108X100G', 108, 3102, 0, '2019-03-28 19:18:52', '2019-03-28 13:18:52'),
(88, 67603396, 'LUX BAR FRSH SPLSH FW BLSS 34TK 108X100G', 108, 704, 0, '2019-03-28 19:18:52', '2019-03-28 13:18:52'),
(89, 67603432, 'LUX SKN BAR VLV TCH FW BLS 34TK 108X100G', 108, 3982, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(90, 67603525, 'LUX BAR SFT TCH MP FW BLS 95TK 36X3X100G', 36, 363, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(91, 67603532, 'LUX BAR VLV TCH MP FW BLS 95TK 36X3X100G', 36, 517, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(92, 67605477, 'PONDS MOST VANISNG CRM 3YRSL 60TK 72X50G', 72, 264, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(93, 67623345, 'SURF EXCEL STD PWDR S+ UL 9T 5TK 864X20G', 864, 3916, -2760, '2019-07-25 02:22:39', '2019-07-25 02:22:39'),
(94, 67623493, 'SURF EXCEL STD PWD S+ BCP UL 210TK 6X1KG', 6, 1056, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(95, 67608979, 'FAIR&LOVELY FC MVIT FIZZA 115TK 72X50G', 72, 1628, -150, '2019-07-25 02:22:54', '2019-07-25 02:22:54'),
(96, 67608976, 'FAIR&LOVELY FC MULTVT FIZA 60TK 72X25G', 72, 2453, -240, '2019-07-25 02:22:55', '2019-07-25 02:22:55'),
(97, 67642196, 'FAIR&LOVELY BLEMISH BALM CREAM RL 24X40G', 24, 341, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(98, 67630172, 'DOVE SKIN CLNSNG BAR WHITE NPR 48X100G', 48, 715, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(99, 67630963, 'DOVE SKIN CLNSING BAR PINK NPR 48X135G', 48, 473, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(100, 67630962, 'DOVE SKIN CLNSING BAR WHTE NPR 48X135G', 48, 572, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(101, 67630193, 'DOVE SKIN CLNSING BAR PINK NPR 48X100G', 48, 440, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(102, 67630201, 'DOVE SKIN CLENSING BAR WHITE NPR 72X50G', 72, 220, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(103, 67632723, 'CLOSE-UP TH PST MINT KEY-RING CP 36X145G', 36, 220, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(104, 67632728, 'CLOSE-UP TPST REDHOT KEY-RING CP 36X145G', 36, 22, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(105, 67660995, 'LUX SKIN BAR SFT TCH FW BLS 47TK 72X150G', 72, 3135, -120, '2019-07-25 02:22:44', '2019-07-25 02:22:44'),
(106, 67608984, 'FAIR&LOVELY FC MV BIG NOSE 15TK 576X9G', 576, 176, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(107, 67660993, 'LUX SKN BAR VLV TCH FW BLSS 47TK 72X150G', 72, 3289, 0, '2019-03-28 19:18:53', '2019-03-28 13:18:53'),
(108, 67654896, 'BB TAAZA BLACK TEA RL 30TK 288X50G', 288, 319, -720, '2019-07-25 02:22:44', '2019-07-25 02:22:44'),
(109, 67654887, 'BB TAAZA BLACK TEA 110TK 48X200G', 48, 2442, -210, '2019-07-25 02:22:45', '2019-07-25 02:22:45'),
(110, 67657494, 'LIFEBUOY SKN BAR TOTAL RL 42TK 72X150G', 72, 385, -120, '2019-07-25 02:22:41', '2019-07-25 02:22:41'),
(111, 67654890, 'BB TAAZA BLACK TEA RL 58TK 144X100G', 144, 715, -120, '2019-07-25 02:22:45', '2019-07-25 02:22:45'),
(112, 67657470, 'LIFEBUOY SKN BAR TOTAL RL 30TK 72X100G', 72, 3872, -360, '2019-07-25 02:22:40', '2019-07-25 02:22:40'),
(113, 67662906, 'SURF EXCEL STD PWDR SKY+ 99TK 12X500G', 12, 31582, -90, '2019-07-25 02:22:39', '2019-07-25 02:22:39'),
(114, 67664397, 'SURF EXCEL STD PWDER SKY+ 40TK 144X200G', 144, 660, 0, '2019-03-28 19:18:54', '2019-03-28 13:18:54'),
(115, 67579217, 'PONDS FW ESS CARE DALY 3DPAK 85TK 48X50G', 48, 88, -30, '2019-07-25 02:22:56', '2019-07-25 02:22:56'),
(116, 67579212, 'PONDS FW ESS CARE RL 3DPAK 125TK 48X100G', 48, 55, 0, '2019-03-28 19:18:54', '2019-03-28 13:18:54'),
(117, 67650528, 'TRESEMME COND HAIR FALL DEF E 24X190ML', 24, 33, 0, '2019-03-28 19:18:54', '2019-03-28 13:18:54'),
(118, 67642194, 'TRESEMME SHMP NOUR & REP E 12X580ML', 12, 55, 0, '2019-03-28 19:18:54', '2019-03-28 13:18:54'),
(119, 67660989, 'WHEEL PWDR CLEAN&FRSH NI UL TK42 60X500G', 60, 9812, 0, '2019-03-28 19:18:54', '2019-03-28 13:18:54'),
(120, 67642189, 'TRESEMME SHAMPOO KERATIN SMTH E 12X580ML', 12, 44, 0, '2019-03-28 19:18:54', '2019-03-28 13:18:54'),
(121, 67661494, 'LUX SKN BAR SFT TCH FW BLSS 25TK 108X75G', 108, 1122, 0, '2019-03-28 19:18:54', '2019-03-28 13:18:54'),
(122, 67661497, 'LUX SKN BAR VLV TCH FW BLSS 25TK 108X75G', 108, 990, 0, '2019-03-28 19:18:54', '2019-03-28 13:18:54'),
(123, 67637988, 'RIN STD PWD PWR BRT SI NAW 60TK 60X500G', 60, 18360, -520, '2019-07-25 02:22:38', '2019-07-25 02:22:38'),
(124, 67638005, 'RIN STD PWD ANTI BAC NP 130TK 25X1KG', 25, 363, 0, '2019-03-28 19:18:54', '2019-03-28 13:18:54'),
(125, 67637991, 'RIN STD PWD PWR BRT SI NAW 25TK 144X200G', 144, 1650, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(126, 67638024, 'RIN STD PWD ANTI BAC NP 65TK 60X500G', 60, 253, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(127, 67658905, 'SUNSILK SHAMPO HFS SDPTY 165TK 24X180ML', 24, 374, -20, '2019-07-25 02:22:48', '2019-07-25 02:22:48'),
(128, 67658927, 'SUNSILK SHAMPO T&L SDPTY 165TK 24X180ML', 24, 473, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(129, 67658948, 'SUNSILK SMP HJAB RCRG ANB 300TK 24X375ML', 24, 55, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(130, 67658939, 'SUNSILK SMP HJAB RCRG REF 300TK 24X375ML', 24, 143, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(131, 67658944, 'SUNSILK SMP HJAB RCRG REF 180TK 24X180ML', 24, 121, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(132, 67658930, 'SUNSILK SMP PR-STRT SDPTY 300TK 24X375ML', 24, 99, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(133, 67658913, 'SUNSILK SHMP T&L SERNDPTY 290TK 24X375ML', 24, 418, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(134, 67658902, 'SUNSILK SMP HFS SERNDPTY 290TK 24X375ML', 24, 363, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(135, 67658933, 'SUNSILK SMP PR-STRT SDPTY 180TK 24X180ML', 24, 110, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(136, 67658898, 'SUNSILK SMP BLK SHN SDPTY 165TK 24X180ML', 24, 1111, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(137, 67658841, 'SUNSILK SHMP BLK SRNDPTY 290TK 24X375ML', 24, 440, 0, '2019-03-28 19:18:55', '2019-03-28 13:18:55'),
(138, 67677966, 'WHEEL CLEAN & ROSE FRS TH 45TK 60X500G', 60, 374, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(139, 67690623, 'DOVE SHMP OXGN MSTR SCRUFF FREE 24X340ML', 24, 22, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(140, 67690620, 'DOVE SHAMPOO NOC SCRUFF FREE 24X340ML', 24, 22, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(141, 67677860, 'SURF EXCEL STD PWD S+ UL 10TK 336X50G', 336, 2112, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(142, 67687602, 'WHEEL CLEAN & FRESH NI UL T_18 144X200G', 144, 1529, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(143, 67703554, 'LIFEBUOY SKN BAR TOTAL ARYA 30TK 72X100G', 72, 352, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(144, 67702456, 'BB TAAZA BLK TEA BG 10TK_OFF_CP 24X50X2G', 24, 506, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(145, 67605684, 'CLOSE-UP WH ACTN NTSMLE_THEA 80TK 72X80G', 72, 165, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(146, 67608964, 'FAIR&LOVELY FCL CRM AYRV 65TK 72X25G', 72, 121, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(147, 67608969, 'FAIR&LOVELY FCL CRM AYRV 120TK 72X50G', 72, 132, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(148, 67699927, 'BB TAAZA BLACK TEA 215TK 24X400G', 24, 12089, -100, '2019-07-25 02:22:45', '2019-07-25 02:22:45'),
(149, 67703557, 'LIFEBUOY SKN BAR CARE ARYA 32TK 72X100G', 72, 3773, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(150, 67634592, 'PONDS WTE BEAUTY CRM PDMPNK_100TK 48X25G', 48, 429, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(151, 67720291, 'VIM HDW STD LIQUID PRO 42TK 24X250ML', 24, 5599, 0, '2019-03-28 19:18:56', '2019-03-28 13:18:56'),
(152, 67722776, 'LIFEBUOY SKIN BAR CARE 44TK 72X150G', 72, 374, -120, '2019-07-25 02:22:40', '2019-07-25 02:22:40'),
(153, 67722724, 'LIFEBUOY SKN BAR TOTAL ARYA 42TK 72X150G', 72, 11, 0, '2019-03-28 19:18:57', '2019-03-28 13:18:57'),
(154, 67722714, 'LIFEBUOY BAR LMON-FRSH 32TK 72X100G', 72, 4444, -370, '2019-07-25 02:22:40', '2019-07-25 02:22:40'),
(155, 67722711, 'LIFEBUOY SKN BAR CARE 32TK 72X100G', 72, 990, 0, '2019-03-28 19:18:57', '2019-03-28 13:18:57'),
(156, 67741789, 'DOVE SHAMPOO IRP DOME 190TK 24X170ML', 24, 110, 0, '2019-03-28 19:18:57', '2019-03-28 13:18:57'),
(157, 67741795, 'DOVE SHAMPOO NOC DOME 190TK 24X170ML', 24, 176, 0, '2019-03-28 19:18:57', '2019-03-28 13:18:57'),
(158, 67741822, 'DOVE SHAMPOO IRP DOME 48X80ML', 48, 209, 0, '2019-03-28 19:18:57', '2019-03-28 13:18:57'),
(159, 67741775, 'DOVE SHAMPOO HFR DOME 330TK 24X340ML', 24, 165, 0, '2019-03-28 19:18:57', '2019-03-28 13:18:57'),
(160, 67741771, 'DOVE SHAMPOO IRP DOME 330TK 24X340ML', 24, 132, 0, '2019-03-28 19:18:57', '2019-03-28 13:18:57'),
(161, 67741792, 'DOVE SHAMPOO HFR DOME 190TK 24X170ML', 24, 308, 0, '2019-03-28 19:18:57', '2019-03-28 13:18:57'),
(162, 67741819, 'DOVE SHMP OXGN MOSTR DOME 200TK 24X170ML', 24, 143, 0, '2019-03-28 19:18:57', '2019-03-28 13:18:57'),
(163, 67764551, 'KNORR SOUP HOT & SOUR 40TK 72X31G', 72, 132, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(164, 67764554, 'KNORR SOUP CHCKN CRN CLASSIC 40TK 72X24G', 72, 176, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(165, 67765587, 'LIPTON GRN T BAG PUR LT 230TK 24X50X1.3G', 24, 88, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(166, 67765626, 'LIPTON GRN T BAG PUR LT 120TK 48X25X1.3G', 48, 99, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(167, 67767836, 'RIN STD PWD PWR BRT SI NSC 120TK 30X1KG', 30, 28985, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(168, 67767838, 'WHEEL PWDR CLEAN&FR NI NSC TK80 30X1KG', 30, 5445, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(169, 67620733, 'PEPSODENT TP CHRCL&LMN ALFRD 75TK 72X90G', 72, 121, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(170, 67620729, 'PEPSODENT TP CHRCL&LMN AFD 110TK 36X140G', 36, 55, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(171, 67758866, 'PEPSODENT TOTHPOWDR 40TK RLN_19 72X100G', 72, 418, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(172, 67758860, 'PEPSODENT TOTHPOWDR 24TK RLN_19 144X50G', 144, 605, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(173, 67705135, 'SUNSILK SMP HJB REF INT_SCRD_CP 24X375ML', 24, 33, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(174, 67758547, 'PEPSODENT SENTV EXP PRFSNL 135TK 36X140G', 36, 517, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(175, 67758553, 'PEPSODENT SENTV EXP PRFSNL 80TK 72X80G', 72, 517, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(176, 67758595, 'LIFEBUOY LQ HW LEMON 10TK_OFF 24X180ML', 24, 726, 0, '2019-03-28 19:18:58', '2019-03-28 13:18:58'),
(177, 67758592, 'LIFEBUOY LQ HW TOT KAL 10TK_OFF 24X180ML', 24, 1034, 0, '2019-03-28 19:18:59', '2019-03-28 13:18:59'),
(178, 67758589, 'LIFEBUOY LQ HW CRE KAL 10TK_OFF 24X180ML', 24, 968, 0, '2019-03-28 19:18:59', '2019-03-28 13:18:59'),
(179, 67789715, 'KNORR SOUP HOT&SOUR CP TFFN BOX 12X124G', 12, 66, 0, '2019-03-28 19:18:59', '2019-03-28 13:18:59'),
(180, 67789708, 'KNORR SOUP CLSS THAI CP TFFN BOX 12X112G', 12, 1177, 0, '2019-03-28 19:18:59', '2019-03-28 13:18:59'),
(181, 67789706, 'KNORR SUP CHK CRN CLS CP TFFN BOX 12X96G', 12, 1309, 0, '2019-03-28 19:18:59', '2019-03-28 13:18:59'),
(182, 67819901, 'SURF EXCEL HS PWD S+ BCKT CP 230TK 6X1KG', 6, 72017, 0, '2019-03-28 19:18:59', '2019-03-28 13:18:59'),
(183, 67833042, 'LUX SKIN BAR HYPNTC ROS 45TK TM 48X125G', 48, 231, 0, '2019-03-28 19:18:59', '2019-03-28 13:18:59'),
(184, 67833048, 'LUX SKIN BAR CRMNG MGNLA 45TK TM 48X125G', 48, 99, 0, '2019-03-28 19:18:59', '2019-03-28 13:18:59'),
(185, 21007328, 'VIM HDW ABR POWDER A03 60X500G', 60, 267, -60, '2019-07-25 02:22:42', '2019-07-25 02:22:42');

-- --------------------------------------------------------

--
-- Table structure for table `tadas`
--

CREATE TABLE `tadas` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `tada` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tadas`
--

INSERT INTO `tadas` (`id`, `employee_id`, `start_date`, `tada`, `created_at`, `updated_at`) VALUES
(1, 16, '2019-01-01', 6463, '2019-04-25 18:38:22', '2019-04-25 18:38:22'),
(2, 17, '2019-02-16', 0, '2019-04-25 18:51:48', '2019-04-25 18:51:48'),
(3, 16, '2019-01-16', 9765, '2019-04-26 20:52:20', '2019-04-26 20:52:20'),
(4, 17, '2019-01-16', 1776, '2019-05-11 08:34:34', '2019-05-11 08:34:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` varchar(30) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `gender` varchar(30) NOT NULL,
  `designation` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `remember_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `mobile`, `gender`, `designation`, `address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Zihad Hridoy', 'zihad@dms.com', '$2y$10$QVAvJgFMMG7prSpsnnMcq.g.P57YxccSHuFVCrjZ62AQrpLBwvoK.', 'Super Admin', '01906489959', 'm', 'Super Admin', '', 'yG5i6KdiSTlOCIwwsMZRoaxzEwGypJCM4JzWkNiht47TAjvAZ2nUlNwmQeSZ', '2019-06-29 10:37:13', '0000-00-00 00:00:00'),
(2, 'Mahmud Hasan', 'princerafid01@gmail.com', '$2y$10$QVAvJgFMMG7prSpsnnMcq.g.P57YxccSHuFVCrjZ62AQrpLBwvoK.', 'Super Admin', '01967890878', 'm', 'IT', 'Dhaka', NULL, '2019-01-02 15:23:15', '2019-01-02 09:23:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cash_flows`
--
ALTER TABLE `cash_flows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_expenses`
--
ALTER TABLE `company_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_expenses_claim`
--
ALTER TABLE `company_expenses_claim`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_expenses`
--
ALTER TABLE `employee_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense_types`
--
ALTER TABLE `expense_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investments`
--
ALTER TABLE `investments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investment_details`
--
ALTER TABLE `investment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_days`
--
ALTER TABLE `leave_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `office_expenses`
--
ALTER TABLE `office_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `off_days`
--
ALTER TABLE `off_days`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `other_infos`
--
ALTER TABLE `other_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_values`
--
ALTER TABLE `sale_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tadas`
--
ALTER TABLE `tadas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cash_flows`
--
ALTER TABLE `cash_flows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `company_expenses`
--
ALTER TABLE `company_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `company_expenses_claim`
--
ALTER TABLE `company_expenses_claim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `employee_expenses`
--
ALTER TABLE `employee_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `expense_types`
--
ALTER TABLE `expense_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `investments`
--
ALTER TABLE `investments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `investment_details`
--
ALTER TABLE `investment_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `leave_days`
--
ALTER TABLE `leave_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `office_expenses`
--
ALTER TABLE `office_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `off_days`
--
ALTER TABLE `off_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `other_infos`
--
ALTER TABLE `other_infos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=271;

--
-- AUTO_INCREMENT for table `sale_values`
--
ALTER TABLE `sale_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;

--
-- AUTO_INCREMENT for table `tadas`
--
ALTER TABLE `tadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
